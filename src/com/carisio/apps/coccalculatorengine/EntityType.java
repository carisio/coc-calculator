package com.carisio.apps.coccalculatorengine;

import java.util.ArrayList;
import java.util.List;

public enum EntityType {
	BARBARIAN,
	ARCHER,
	GIANT,
	GOBLIN,
	WALL_BREAKER,
	BALLOON,
	WIZARD,
	HEALER,
	DRAGON,
	PEKKA,
	BABY_DRAGON,
	MINER,
	MINION,
	HOG_RIDER,
	VALKYRIE,
	GOLEM,
	WITCH,
	LAVA_HOUND,
	BOWLER,
	LIGHTNING_SPELL,
	HEALING_SPELL,
	RAGE_SPELL,
	JUMP_SPELL,
	FREEZE_SPELL,
	CLONE_SPELL,
	POISON_SPELL,
	EARTHQUAKE_SPELL,
	HASTE_SPELL,
	SKELETON_SPELL,
	ARMY_CAMP,
	TOWN_HALL,
	ELIXIR_COLLECTOR,
	ELIXIR_STORAGE,
	GOLD_MINE,
	GOLD_STORAGE,
	BARRACK,
	DARK_BARRACK,
	LABORATORY,
	CANNON,
	ARCHER_TOWER,
	WALL,
	WIZARD_TOWER,
	AIR_DEFENSE,
	MORTAR,
	CLAN_CASTLE,
	BUILDER,
	HIDDEN_TESLA,
	SPELL_FACTORY,
	XBOW,
	BARBARIAN_KING,
	ARCHER_QUEEN,
	GRAND_WARDEN,
	DARK_ELIXIR_DRILL,
	DARK_ELIXIR_STORAGE,
	INFERNO_TOWER,
	AIR_SWEEPER,
	EAGLE_ARTILLERY,
	DARK_SPELL_FACTORY,
	BOMB,
	SPRING_TRAP,
	GIANT_BOMB,
	AIR_BOMB,
	SEEKING_AIR_BOMB,
	SKELETON_TRAP;

	public EvolutionTable getEvolutionTable() {
		EvolutionTable result = null;
		switch(this) {
			case BARBARIAN:
				result = EvolutionTable.getBarbarianEvolutionTable();
				break;
			case ARCHER:
				result = EvolutionTable.getArcherEvolutionTable();
				break;
			case GIANT:
				result = EvolutionTable.getGiantEvolutionTable();
				break;
			case GOBLIN:
				result = EvolutionTable.getGoblinEvolutionTable();
				break;
			case WALL_BREAKER:
				result = EvolutionTable.getWallBreakerEvolutionTable();
				break;
			case BALLOON:
				result = EvolutionTable.getBalloonEvolutionTable();
				break;
			case WIZARD:
				result = EvolutionTable.getWizardEvolutionTable();
				break;
			case HEALER:
				result = EvolutionTable.getHealerEvolutionTable();
				break;
			case DRAGON:
				result = EvolutionTable.getDragonEvolutionTable();
				break;
			case PEKKA:
				result = EvolutionTable.getPEKKAEvolutionTable();
				break;
			case BABY_DRAGON:
				result = EvolutionTable.getBabyDragonEvolutionTable();
				break;
			case MINER:
				result = EvolutionTable.getMinerEvolutionTable();
				break;
			case MINION:
				result = EvolutionTable.getMinionEvolutionTable();
				break;
			case HOG_RIDER:
				result = EvolutionTable.getHogRiderEvolutionTable();
				break;
			case VALKYRIE:
				result = EvolutionTable.getValkyrieEvolutionTable();
				break;
			case GOLEM:
				result = EvolutionTable.getGolemEvolutionTable();
				break;
			case WITCH:
				result = EvolutionTable.getWitchEvolutionTable();
				break;
			case LAVA_HOUND:
				result = EvolutionTable.getLavaHoundEvolutionTable();
				break;
			case BOWLER:
				result = EvolutionTable.getBowlerEvolutionTable();
				break;
			case LIGHTNING_SPELL:
				result = EvolutionTable.getLightningSpellEvolutionTable();
				break;
			case HEALING_SPELL:
				result = EvolutionTable.getHealingSpellEvolutionTable();
				break;
			case RAGE_SPELL:
				result = EvolutionTable.getRageSpellEvolutionTable();
				break;
			case JUMP_SPELL:
				result = EvolutionTable.getJumpSpellEvolutionTable();
				break;
			case FREEZE_SPELL:
				result = EvolutionTable.getFreezeSpellEvolutionTable();
				break;
			case CLONE_SPELL:
				result = EvolutionTable.getCloneSpellEvolutionTable();
				break;
			case POISON_SPELL:
				result = EvolutionTable.getPoisonSpellEvolutionTable();
				break;
			case EARTHQUAKE_SPELL:
				result = EvolutionTable.getEarthquakeSpellEvolutionTable();
				break;
			case HASTE_SPELL:
				result = EvolutionTable.getHasteSpellEvolutionTable();
				break;
			case SKELETON_SPELL:
				result = EvolutionTable.getSkeletonSpellEvolutionTable();
				break;
			case ARMY_CAMP:
				result = EvolutionTable.getArmyCampEvolutionTable();
				break;
			case TOWN_HALL:
				result = EvolutionTable.getTowhHallEvolutionTable();
				break;
			case ELIXIR_COLLECTOR:
				result = EvolutionTable.getElixirCollectorEvolutionTable();
				break;
			case ELIXIR_STORAGE:
				result = EvolutionTable.getElixirStorageEvolutionTable();
				break;
			case GOLD_MINE:
				result = EvolutionTable.getGoldMineEvolutionTable();
				break;
			case GOLD_STORAGE:
				result = EvolutionTable.getGoldStorageEvolutionTable();
				break;
			case BUILDER:
				result = new EvolutionTable(EntityType.BUILDER);
				break;
			case BARRACK:
				result = EvolutionTable.getBarracksEvolutionTable();
				break;
			case DARK_BARRACK:
				result = EvolutionTable.getDarkBarracksEvolutionTable();
				break;
			case LABORATORY:
				result = EvolutionTable.getLaboratoryEvolutionTable();
				break;
			case CANNON:
				result = EvolutionTable.getCannonEvolutionTable();
				break;
			case ARCHER_TOWER:
				result = EvolutionTable.getArcherTowerEvolutionTable();
				break;
			case WALL:
				result = EvolutionTable.getWallEvolutionTable();
				break;
			case WIZARD_TOWER:
				result = EvolutionTable.getWizardTowerEvolutionTable();
				break;
			case AIR_DEFENSE:
				result = EvolutionTable.getAirDefenseEvolutionTable();
				break;
			case MORTAR:
				result = EvolutionTable.getMortarEvolutionTable();
				break;
			case CLAN_CASTLE:
				result = EvolutionTable.getClanCastleEvolutionTable();
				break;
			case HIDDEN_TESLA:
				result = EvolutionTable.getHiddenTeslaEvolutionTable();
				break;
			case SPELL_FACTORY:
				result = EvolutionTable.getSpellFactoryEvolutionTable();
				break;
			case XBOW:
				result = EvolutionTable.getXBowEvolutionTable();
				break;
			case BARBARIAN_KING:
				result = EvolutionTable.getBarbarianKingEvolutionTable();
				break;
			case ARCHER_QUEEN:
				result = EvolutionTable.getArcherQueenEvolutionTable();
				break;
			case GRAND_WARDEN:
				result = EvolutionTable.getGrandWardenEvolutionTable();
				break;
			case DARK_ELIXIR_DRILL:
				result = EvolutionTable.getDarkElixiDrillEvolutionTable();
				break;
			case DARK_ELIXIR_STORAGE:
				result = EvolutionTable.getDarkElixiStorageEvolutionTable();
				break;
			case INFERNO_TOWER:
				result = EvolutionTable.getInfernoTowerEvolutionTable();
				break;
			case AIR_SWEEPER:
				result = EvolutionTable.getAirSweeperEvolutionTable();
				break;
			case EAGLE_ARTILLERY:
				result = EvolutionTable.getEagleArtilleryEvolutionTable();
				break;
			case DARK_SPELL_FACTORY:
				result = EvolutionTable.getDarkSpellFactoryEvolutionTable();
				break;
			case BOMB:
				result = EvolutionTable.getBombEvolutionTable();
				break;
			case SPRING_TRAP:
				result = EvolutionTable.getSpringTrapEvolutionTable();
				break;
			case GIANT_BOMB:
				result = EvolutionTable.getGiantBombEvolutionTable();
				break;
			case AIR_BOMB:
				result = EvolutionTable.getAirBombEvolutionTable();
				break;
			case SEEKING_AIR_BOMB:
				result = EvolutionTable.getSeekingAirMineEvolutionTable();
				break;
			case SKELETON_TRAP:
				result = EvolutionTable.getSkeletonTrapEvolutionTable();
				break;
		}
		
		return result;
	}
	public int getMaximumNumberOfEntityByTH(int thLevel) {
		int result = 0;
		switch(this) {
			case BUILDER:
				result = 5;
				break;
			case BARBARIAN:
				result = 1;
				break;
			case ARCHER:
				result = 1;
				break;
			case GIANT:
				if (thLevel >= 2)
					result = 1;
				else
					result = 0;
				break;
			case GOBLIN:
				if (thLevel >= 1)
					result = 1;
				break;
			case WALL_BREAKER:
				if (thLevel >= 3)
					result = 1;
				else
					result = 0;
				break;
			case BALLOON:
				if (thLevel >= 4)
					result = 1;
				else
					result = 0;
				break;
			case WIZARD:
				if (thLevel >= 5)
					result = 1;
				else
					result = 0;
				break;
			case HEALER:
				if (thLevel >= 6)
					result = 1;
				else
					result = 0;
				break;
			case DRAGON:
				if (thLevel >= 7)
					result = 1;
				else
					result = 0;
				break;
			case PEKKA:
				if (thLevel >= 8)
					result = 1;
				else
					result = 0;
				break;
			case BABY_DRAGON:
				if (thLevel >= 9)
					result = 1;
				else
					result = 0;
				break;
			case MINER:
				if (thLevel >= 10)
					result = 1;
				else
					result = 0;
				break;
			case MINION:
				if (thLevel >= 7)
					result = 1;
				else
					result = 0;
				break;
			case HOG_RIDER:
				if (thLevel >= 7)
					result = 1;
				else
					result = 0;
				break;
			case VALKYRIE:
				if (thLevel >= 8)
					result = 1;
				else
					result = 0;
				break;
			case GOLEM:
				if (thLevel >= 8)
					result = 1;
				else
					result = 0;
				break;
			case WITCH:
				if (thLevel >= 9)
					result = 1;
				else
					result = 0;
				break;
			case LAVA_HOUND:
				if (thLevel >= 9)
					result = 1;
				else
					result = 0;
				break;
			case BOWLER:
				if (thLevel >= 10) 
					result = 1;
				else
					result = 0;
				break;
			case LIGHTNING_SPELL:
				if (thLevel >= 5)
					result = 1;
				else
					result = 0;
				break;
			case HEALING_SPELL:
				if (thLevel >= 6)
					result = 1;
				else
					result = 0;
				break;
			case RAGE_SPELL:
				if (thLevel >= 7)
					result = 1;
				else
					result = 0;
				break;
			case JUMP_SPELL:
				if (thLevel >= 9)
					result = 1;
				else
					result = 0;
				break;
			case FREEZE_SPELL:
				if (thLevel >= 10)
					result = 1;
				else
					result = 0;
				break;
			case CLONE_SPELL:
				if (thLevel >= 10)
					result = 1;
				else
					result = 0;
				break;
			case POISON_SPELL:
				if (thLevel >= 8)
					result = 1;
				else
					result = 0;
				break;
			case EARTHQUAKE_SPELL:
				if (thLevel >= 8)
					result = 1;
				else
					result = 0;
				break;
			case HASTE_SPELL:
				if (thLevel >= 9)
					result = 1;
				else
					result = 0;
				break;
			case SKELETON_SPELL:
				if (thLevel >= 9)
					result = 1;
				else
					result = 0;
				break;
			case ARMY_CAMP:
				if (thLevel <= 2)
					result = 1;
				else if (thLevel <= 4)
					result = 2;
				else if (thLevel <= 6)
					result = 3;
				else
					result = 4;
				break;
			case TOWN_HALL:
				result = 1;
				break;
			case ELIXIR_COLLECTOR:
				if (thLevel <= 6)
					result = thLevel;
				else if (thLevel <= 9)
					result = 6;
				else if (thLevel >= 10)
					result = 7;
				break;
			case ELIXIR_STORAGE:
				if (thLevel <= 2)
					result = 1;
				else if (thLevel <= 7)
					result = 2;
				else if (thLevel <= 8)
					result = 3;
				else
					result = 4;
				break;
			case GOLD_MINE:
				if (thLevel <= 6)
					result = thLevel;
				else if (thLevel <= 9)
					result = 6;
				else if (thLevel >= 10)
					result = 7;
				break;
			case GOLD_STORAGE:
				if (thLevel <= 2)
					result = 1;
				else if (thLevel <= 7)
					result = 2;
				else if (thLevel <= 8)
					result = 3;
				else
					result = 4;
				break;
			case BARRACK:
				if (thLevel <= 1)
					result = 1;
				else if (thLevel <= 3)
					result = 2;
				else if (thLevel <= 6)
					result = 3;
				else
					result = 4;
				break;
			case DARK_BARRACK:
				if (thLevel <= 6)
					result = 0;
				else if (thLevel <= 7)
					result = 1;
				else
					result = 2;
				break;
			case LABORATORY:
				if (thLevel >= 3)
					result = 1;
				break;
			case CANNON:
				if (thLevel <= 4)
					result = 2;
				else if (thLevel <= 6)
					result = 3;
				else if (thLevel <= 9)
					result = 5;
				else if (thLevel == 10)
					result = 6;
				else if (thLevel >= 11)
					result = 7;
				break;
			case ARCHER_TOWER:
				if (thLevel <= 1)
					result = 0;
				else if (thLevel <= 3)
					result = 1;
				else if (thLevel <= 4)
					result = 2;
				else if (thLevel <= 6)
					result = 3;
				else if (thLevel <= 7)
					result = 4;
				else if (thLevel <= 8)
					result = 5;
				else if (thLevel <= 9)
					result = 6;
				else if (thLevel == 10)
					result = 7;
				else if (thLevel >= 11)
					result = 8;
				break;
			case WALL:
				if (thLevel == 1)
					result = 0;
				else if (thLevel == 2)
					result = 25;
				else if (thLevel == 3)
					result = 50;
				else if (thLevel == 4)
					result = 75;
				else if (thLevel == 5)
					result = 100;
				else if (thLevel == 6)
					result = 125;
				else if (thLevel == 7)
					result = 175;
				else if (thLevel == 8)
					result = 225;
				else if (thLevel == 9)
					result = 250;
				else if (thLevel >= 10)
					result = 275;
				break;
			case WIZARD_TOWER:
				if (thLevel <= 4)
					result = 0;
				else if (thLevel <= 5)
					result = 1;
				else if (thLevel <= 7)
					result = 2;
				else if (thLevel <= 8)
					result = 3;
				else if (thLevel <= 10)
					result = 4;
				else if (thLevel >= 11)
					result = 5;
				break;
			case AIR_DEFENSE:
				if (thLevel <= 3)
					result = 0;
				else if (thLevel <= 5)
					result = 1;
				else if (thLevel <= 6)
					result = 2;
				else if (thLevel <= 8)
					result = 3;
				else
					result = 4;
				break;
			case MORTAR:
				if (thLevel <= 2)
					result = 0;
				else if (thLevel <= 5)
					result = 1;
				else if (thLevel <= 6)
					result = 2;
				else if (thLevel <= 7)
					result = 3;
				else
					result = 4;
				break;
			case CLAN_CASTLE:
				if (thLevel >= 3)
					result = 1;
				break;
			case HIDDEN_TESLA:
				if (thLevel <= 6)
					result = 0;
				else if (thLevel == 7)
					result = 2;
				else if (thLevel == 8)
					result = 3;
				else if (thLevel >= 9)
					result = 4;
				break;
			case SPELL_FACTORY:
				if (thLevel >= 5)
					result = 1;
				break;
			case XBOW:
				if (thLevel == 9)
					result = 2;
				else if (thLevel == 10)
					result = 3;
				else if (thLevel >= 11)
					result = 4;
				break;
			case BARBARIAN_KING:
				if (thLevel >= 7)
					result = 1;
				break;
			case ARCHER_QUEEN:
				if (thLevel >= 9)
					result = 1;
				break;
			case GRAND_WARDEN:
				if (thLevel >= 11)
					result = 1;
				break;
			case DARK_ELIXIR_DRILL:
				if (thLevel <= 6)
					result = 0;
				else if (thLevel <= 7)
					result = 1;
				else if (thLevel <= 9)
					result = 2;
				else if (thLevel >= 10)
					result = 3;
				break;
			case DARK_ELIXIR_STORAGE:
				if (thLevel >= 7)
					result = 1;
				break;
			case INFERNO_TOWER:
				if (thLevel >= 10) 
					result = 2;
				break;
			case AIR_SWEEPER:
				if (thLevel <= 5)
					result = 0;
				else if (thLevel <= 8)
					result = 1;
				else
					result = 2;
				break;
			case EAGLE_ARTILLERY:
				if (thLevel >= 11)
					result = 1; 
				break;
			case DARK_SPELL_FACTORY:
				if (thLevel >= 8)
					result = 1;
				break;
			case BOMB:
				if (thLevel <= 2)
					result = 0;
				else if (thLevel <= 4)
					result = 2;
				else if (thLevel <= 6)
					result = 4;
				else if (thLevel >= 7)
					result = 6;
				break;
			case SPRING_TRAP:
				if (thLevel <= 3)
					result = 0;
				else if (thLevel <= 5)
					result = 2;
				else if (thLevel <= 7)
					result = 4;
				else if (thLevel >= 8)
					result = 6;
				break;
			case GIANT_BOMB:
				if (thLevel <= 5)
					result = 0;
				else if (thLevel <= 6)
					result = 1;
				else if (thLevel <= 7)
					result = 2;
				else if (thLevel <= 8)
					result = 3;
				else if (thLevel <= 9)
					result = 4;
				else if (thLevel >= 10)
					result = 5;
				break;
			case AIR_BOMB:
				if (thLevel <= 4)
					result = 0;
				else if (thLevel <= 7)
					result = 2;
				else if (thLevel <= 9)
					result = 4;
				else if (thLevel >= 10)
					result = 5;
				break;
			case SEEKING_AIR_BOMB:
				if (thLevel <= 6)
					result = 0;
				else if (thLevel == 7)
					result = 1;
				else if (thLevel == 8)
					result = 2;
				else if (thLevel == 9)
					result = 4;
				else if (thLevel >= 10)
					result = 5;
				break;
			case SKELETON_TRAP:
				if (thLevel <= 7)
					result = 0;
				else if (thLevel <= 9)
					result = 2;
				else if (thLevel >= 10)
					result = 3;
				break;
		}
		
		return result;
	}
	
	public static EntityType[] getAllTypes() {
		EntityType[] result = new EntityType[] {
			BARBARIAN,
			ARCHER,
			GIANT,
			GOBLIN,
			WALL_BREAKER,
			BALLOON,
			WIZARD,
			HEALER,
			DRAGON,
			PEKKA,
			BABY_DRAGON,
			MINER,
			MINION,
			HOG_RIDER,
			VALKYRIE,
			GOLEM,
			WITCH,
			LAVA_HOUND,
			BOWLER,
			LIGHTNING_SPELL,
			HEALING_SPELL,
			RAGE_SPELL,
			JUMP_SPELL,
			FREEZE_SPELL,
			CLONE_SPELL,
			POISON_SPELL,
			EARTHQUAKE_SPELL,
			HASTE_SPELL,
			SKELETON_SPELL,
			ARMY_CAMP,
			TOWN_HALL,
			ELIXIR_COLLECTOR,
			ELIXIR_STORAGE,
			GOLD_MINE,
			GOLD_STORAGE,
			BARRACK,
			DARK_BARRACK,
			LABORATORY,
			CANNON,
			ARCHER_TOWER,
			WALL,
			WIZARD_TOWER,
			AIR_DEFENSE,
			MORTAR,
			CLAN_CASTLE,
			BUILDER,
			HIDDEN_TESLA,
			SPELL_FACTORY,
			XBOW,
			BARBARIAN_KING,
			ARCHER_QUEEN,
			GRAND_WARDEN,
			DARK_ELIXIR_DRILL,
			DARK_ELIXIR_STORAGE,
			INFERNO_TOWER,
			AIR_SWEEPER,
			EAGLE_ARTILLERY,
			DARK_SPELL_FACTORY,
			BOMB,
			SPRING_TRAP,
			GIANT_BOMB,
			AIR_BOMB,
			SEEKING_AIR_BOMB,
			SKELETON_TRAP};
		return result;
	}
	
	public static List<EntityType> getAllExcept(EntityType[] exceptTypes) {
		EntityType[] allTypes = getAllTypes();
		List<EntityType> result = new ArrayList<EntityType>();
		for (EntityType all : allTypes) {
			boolean insert = true;
			for (EntityType except : exceptTypes) {
				if (all.equals(except)) {
					insert = false;
					break;
				}
			}
			if (insert)
				result.add(all);
		}
		return result;
	}
	
	public static List<EntityType> getElixirTroops() {
		List<EntityType> troops = new ArrayList<EntityType>();
		troops.add(EntityType.BARBARIAN);
		troops.add(EntityType.ARCHER);
		troops.add(EntityType.GIANT);
		troops.add(EntityType.GOBLIN);
		troops.add(EntityType.WALL_BREAKER);
		troops.add(EntityType.BALLOON);
		troops.add(EntityType.WIZARD);
		troops.add(EntityType.HEALER);
		troops.add(EntityType.DRAGON);
		troops.add(EntityType.PEKKA);
		troops.add(EntityType.BABY_DRAGON);
		troops.add(EntityType.MINER);
		return troops;
	}
	public static List<EntityType> getDarkElixirTroops() {
		List<EntityType> darkTroops = new ArrayList<EntityType>();
		darkTroops.add(EntityType.MINION);
		darkTroops.add(EntityType.HOG_RIDER);
		darkTroops.add(EntityType.VALKYRIE);
		darkTroops.add(EntityType.GOLEM);
		darkTroops.add(EntityType.WITCH);
		darkTroops.add(EntityType.LAVA_HOUND);
		darkTroops.add(EntityType.BOWLER);
		return darkTroops;
	}
	public static List<EntityType> getSpells() {
		List<EntityType> spells = new ArrayList<EntityType>();
		spells.add(EntityType.LIGHTNING_SPELL);
		spells.add(EntityType.HEALING_SPELL);
		spells.add(EntityType.RAGE_SPELL);
		spells.add(EntityType.JUMP_SPELL);
		spells.add(EntityType.FREEZE_SPELL);
		spells.add(EntityType.CLONE_SPELL);
		return spells;
	}
	public static List<EntityType> getDarkSpells() {
		List<EntityType> darkSpells = new ArrayList<EntityType>();
		darkSpells.add(EntityType.POISON_SPELL);
		darkSpells.add(EntityType.EARTHQUAKE_SPELL);
		darkSpells.add(EntityType.HASTE_SPELL);
		darkSpells.add(EntityType.SKELETON_SPELL);
		return darkSpells;
	}
	public static List<EntityType> getTroopsAndSpells() {
		List<EntityType> troopsAndSpells = new ArrayList<EntityType>();
		troopsAndSpells.addAll(getElixirTroops());
		troopsAndSpells.addAll(getDarkElixirTroops());
		troopsAndSpells.addAll(getSpells());
		troopsAndSpells.addAll(getDarkSpells());
		return troopsAndSpells;
	}
}