package com.carisio.apps.coccalculatorengine;

public class Resource {
	private long gold;
	private long elixir;
	private long darkElixir;
	
	public Resource(long gold, long elixir, long darkElixir) {
		this.gold = gold;
		this.elixir = elixir;
		this.darkElixir = darkElixir;
	}
	
	public long getGold() {
		return gold;
	}

	public void setGold(long gold) {
		this.gold = gold;
	}

	public long getElixir() {
		return elixir;
	}

	public void setElixir(long elixir) {
		this.elixir = elixir;
	}

	public long getDarkElixir() {
		return darkElixir;
	}

	public void setDarkElixir(long darkElixir) {
		this.darkElixir = darkElixir;
	}

	public Resource sum(Resource r) {
		return new Resource(gold + r.gold, elixir + r.elixir, darkElixir + r.darkElixir);
	}
	public Resource multiply(int n) {
		return new Resource(n*gold, n*elixir, n*darkElixir);
	}
	@Override
	public boolean equals(Object obj) {
		Resource r = (Resource)obj;
		return (gold == r.gold) && (elixir == r.elixir) && (darkElixir == r.darkElixir);
	}
	
	@Override
	public String toString() {
		return "gold: " + gold + ", elixir: " + elixir + ", dark elixir: " + darkElixir;
	}
	
	@Override
	protected Object clone() {
		Resource r = new Resource(gold, elixir, darkElixir);
		return r;
	}

	public boolean isZero() {
		return gold == 0 && elixir == 0 && darkElixir == 0;
	}
}

