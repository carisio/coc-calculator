package com.carisio.apps.coccalculatorengine;

import java.util.StringTokenizer;

/*
* Walls are implemented using an Entity class.
*/
public class WallEntity extends Entity {
	private int nWalls;
	public WallEntity(int seqId, int lvl, int nWalls) {
		super(seqId, lvl, EntityType.WALL);
		this.nWalls = nWalls;
	}
	public int getNumberOfWalls() {
		return nWalls;
	}
	public void setNumberOfWalls(int n) {
		nWalls = n;
	}
	public Resource getAccumulatedResourceToUpgradeOneWallTo(int toLevel) {
		return getAccumulatedResourceToUpgradeOneWallTo(getLevel(), toLevel);
	}
	public Resource getAccumulatedResourceToUpgradeOneWallTo(int fromLevel, int toLevel) {
			return super.getAccumulatedResourceToUpgradeTo(fromLevel, toLevel);
	}
	@Override
	public Resource getAccumulatedResourceToUpgradeTo(int toLevel) {
		return getAccumulatedResourceToUpgradeOneWallTo(getLevel(), toLevel).multiply(nWalls);
	}
	@Override
	public Resource getAccumulatedResourceToUpgradeTo(int from, int toLevel) {
		return getAccumulatedResourceToUpgradeOneWallTo(from, toLevel).multiply(nWalls);
	}
	@Override
	public CoCTime getAccumulatedTimeToUpgradeTo(int level) {
		CoCTime c = super.getAccumulatedTimeToUpgradeTo(level);
		return c.multiply(nWalls);
	}
	@Override
	public void setLevel(int level) {
		// Cannot change the level of the wall after it was created.
	}
	public String convertToString() {
		return super.convertToString() + "\t" + nWalls;
	}
	
	public void convertFromString(String s) {
		StringTokenizer st = new StringTokenizer(s);
		if (st.countTokens() >= 3) {
			super.convertFromString(s);
			int tokensTotal = st.countTokens();
			for (int i = 0; i < tokensTotal - 1; i++)
				st.nextToken();
			nWalls = Integer.valueOf(st.nextToken());
		}
	}
	
	public static int getInitialLevelToUpgradeWithGoldAndElixir() {
		EvolutionTable ev = EvolutionTable.getWallEvolutionTable();
		int maxLevel = ev.getMaxLevel();
		int minLevelElixir = 1;
		for (; minLevelElixir <= maxLevel; minLevelElixir++) {
			if (ev.getLevelRequirements(minLevelElixir).getCostToUpgrade().getElixir() > 0)
				break;
		}
		return minLevelElixir;
	}
}
