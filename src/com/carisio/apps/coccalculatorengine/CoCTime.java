package com.carisio.apps.coccalculatorengine;

public class CoCTime implements Comparable<CoCTime> {
	private int days;
	private int hours;
	private int minutes;
	private int seconds;
	
	public CoCTime(int days, int hours, int minutes, int seconds) {
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		
		while (this.seconds >= 60) {
			this.seconds -= 60;
			this.minutes++;
		}
		
		while (this.minutes >= 60) {
			this.minutes -= 60;
			this.hours++;
		}
		
		while (this.hours >= 24) {
			this.hours -= 24;
			this.days++;
		}
	}
	public int getDays() {
		return days;
	}
	public int getHours() {
		return hours;
	}
	public int getMinutes() {
		return minutes;
	}
	public int getSeconds() {
		return seconds;
	}
	public CoCTime sum(CoCTime c) {
		return new CoCTime(days + c.days, hours + c.hours, minutes + c.minutes, seconds + c.seconds);
	}
	public CoCTime multiply(int n) {
		return new CoCTime(n*days, n*hours, n*minutes, n*seconds);
	}	
	@Override
	public boolean equals(Object obj) {
		CoCTime c = (CoCTime)obj;
		return (days == c.days) && (hours == c.hours) && (minutes == c.minutes) && (seconds == c.seconds);
	}
	
	@Override
	public String toString() {
		return "days: " + days + ", hours: " + hours + ", minutes: " + minutes + ", seconds: " + seconds;
	}
	public String toString(String dSymbol, String hSymbol, String mSymbol, String sSymbol, String emptyString) {
		String result = "";
		if (days == 0 && hours == 0 && minutes == 0 && seconds == 0) {
			return emptyString;
		} else {
			if (days != 0) {
				result = days + dSymbol + " "; 
			}
			if (hours != 0) {
				result += hours + hSymbol + " ";
			}
			if (minutes != 0) {
				result += minutes + mSymbol + " ";
			}
			if (seconds != 0) {
				result += seconds + sSymbol + " ";
			}
		}
		return result;
	}
	public CoCTime multiply(double d) {
		double ddays = d * days;
		double dhours = d * hours;
		double dmin = d * minutes;
		double dsec = d * seconds;
		
		int rdays = (int)ddays;
		dhours += (ddays-rdays)*24;
		int rhour = (int)dhours;
		dmin += (dhours-rhour)*60;
		int rmin = (int)dmin;
		dsec += (dmin-rmin)*60;
		int rsec = (int)dsec;
		
		return new CoCTime(rdays, rhour, rmin, rsec);
	}
	@Override
	protected Object clone() {
		CoCTime c = new CoCTime(days, hours, minutes, seconds);
		return c;
	}
	public boolean isZero() {
		return (days == 0) && (hours == 0) && (minutes == 0) && (seconds == 0);
	}
	@Override
	public int compareTo(CoCTime another) {
		// returns positive if another is greater than this, positive if it is lower than this or 0 if both are equal
		
		long totalThisSeconds = convertToSeconds();
		long totalAnotherSeconds = another.convertToSeconds();

		return (int) (totalThisSeconds - totalAnotherSeconds);
	}
	
	public long convertToSeconds() {
		return days*24*60*60 + hours*60*60 + minutes*60 + seconds;
	}
}
