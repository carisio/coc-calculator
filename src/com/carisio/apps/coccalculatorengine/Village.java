package com.carisio.apps.coccalculatorengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class Village {
	/**
	 * The variable entities saves the progress of all entities of the game.
	 * If you have 3 entities of a type (i.e., 3 x-bow), there are
	 * 3 entities of type EntityType.XBOW inside the list entities
	 * 
	 * There are a lot of walls in the game. So each wall entity holds the number of wall
	 * at that level. Today are walls at level 0 to 11. So there are 11 walls inside 
	 * the walls list. 
	 */
	private String name;
	private List<Entity> entities;
	private List<WallEntity> walls;
	
	private short numBuilder = 2;
	
	public Village() {
		this(" ");
	}
	public Village(String name) {
		if (name == null)
			name = "";
		this.name = name;
		initializeEntities();
		
		// This is the status after the tutorial
		setNumBuilder(2);
		getEntity(EntityType.TOWN_HALL, 1).setLevel(1);
		getEntity(EntityType.GOLD_MINE, 1).setLevel(1);
		getEntity(EntityType.GOLD_STORAGE, 1).setLevel(1);
		getEntity(EntityType.ELIXIR_COLLECTOR, 1).setLevel(1);
		getEntity(EntityType.ELIXIR_STORAGE, 1).setLevel(1);
		getEntity(EntityType.ARMY_CAMP, 1).setLevel(1);
		getEntity(EntityType.BARRACK, 1).setLevel(1);
		getEntity(EntityType.CANNON, 1).setLevel(1);
	}
	public void buildFullVillage(int townHall) {
		EntityType[] types = EntityType.getAllTypes();
		
		for (int i = 0; i < types.length; i++) {
			EntityType t = types[i];
			List<Entity> entities = getAllEntities(t);
			
			int maxQty = t.getMaximumNumberOfEntityByTH(townHall);
			int maxLvl = t.getEvolutionTable().getMaxLevelAllowedAtTHLevel(townHall);

			if (t.equals(EntityType.TOWN_HALL)) {
				setTownHallLevel(townHall);
			} else if (t.equals(EntityType.BUILDER)) {
				numBuilder = (short) maxQty;
			} else if (t.equals(EntityType.WALL)) {
				for (int j = 0; j < walls.size(); j++) {
					WallEntity w = walls.get(j);
					if (w.getLevel() == (short)maxLvl)
						w.setNumberOfWalls(maxQty);
					else
						w.setNumberOfWalls(0);
				}
			} else {
				for (int j = 0; j < maxQty; j++) {
					getEntity(t, j+1).setLevel(maxLvl);
				}
				for (int j = maxQty; j < entities.size(); j++) {
					getEntity(t, j+1).setLevel(0);
				}
			}
		}
	}

	public String getName() {
		return name;
	}
	
	
	public List<Entity> getAllEntities() {
//		Collections.sort(entities);
		return entities;
	}
	/**
	 * Return all entities of the village with a given type.
	 * The entities are sorted by their id.
	 * @param type
	 * @return
	 */
	public List<Entity> getAllEntities(EntityType type) {
		List<Entity> result = new ArrayList<Entity>();
		for (Entity e : entities) {
			if (e.isType(type))
				result.add(e);
		}
		
//		Collections.sort(result);
		return result;
	}

	/**
	 * Return all entities of the type types
	 * @param types
	 * @return
	 */
	public List<Entity> getAllEntities(List<EntityType> types) {
		List<Entity> result = new ArrayList<Entity>();
		for (Entity e : entities) {
			if (types.contains(e.getType()))
				result.add(e);
		}
//		Collections.sort(result);
		return result;
	}
	/**
	 * Returns all entities of the village, except those whose type are
	 * given by 'types' parameter
	 * @param types
	 * @return
	 */
	public List<Entity> getAllEntitiesExcept(List<EntityType> types) {
		List<Entity> result = new ArrayList<Entity>();
		for (Entity e : entities) {
			if (!types.contains(e.getType()))
				result.add(e);
		}
//		Collections.sort(result);
		return result;
	}
	
	/**
	 * Return the n first entities of the village with a given type.
	 * @param n
	 * @param type
	 * @return
	 */
	public List<Entity> getNEntities(EntityType type, int n) {
		List<Entity> result = getAllEntities(type);
		while (result.size() > n)
			result.remove(result.size()-1);

		return result;
	}
	/**
	 * Return all entities by a TH level, except those of type "except"
	 * @param thLevel
	 * @return
	 */
	public List<Entity> getAllEntitiesForTHLevelExcept(int thLevel, List<EntityType> except) {
		if (except == null)
			except = new ArrayList<EntityType>();
		
		List<Entity> result = new ArrayList<Entity>();
		for (EntityType type : EntityType.values()) {
			if (!type.equals(EntityType.WALL) && !except.contains(type)) {
				int nEntitiesAtTHLevel = type.getMaximumNumberOfEntityByTH(thLevel);
				result.addAll(getNEntities(type, nEntitiesAtTHLevel));
			}
		}
		return result;
	}

	public List<Entity> getSomeEntitiesForATHLevel(int thLevel, List<EntityType> types) {
		List<Entity> result = new ArrayList<Entity>();
		for (EntityType type : EntityType.values()) {
			if (!type.equals(EntityType.WALL) && types.contains(type)) {
				int nEntitiesAtTHLevel = type.getMaximumNumberOfEntityByTH(thLevel);
				result.addAll(getNEntities(type, nEntitiesAtTHLevel));
			}
		}
		return result;
	}
	/**
	 * Return the n'th element of type type
	 * @param type the type of the element
	 * @param n the position of the element (it is 1-index: n = 1 is the first etc)
	 * @return
	 */
	public Entity getEntity(EntityType type, int n) {
		List<Entity> entitiesByType = getAllEntities(type);
		return entitiesByType.get(n-1);
	}
	
	public List<WallEntity> getWalls() {
		return walls;
	}
	public WallEntity getWall(int wallLevel) {
		WallEntity result = null;
		for (WallEntity w : walls) {
			if (w.getLevel() == wallLevel) {
				result = w;
				break;
			}
		}
		return result;
	}
	/**
	 * Check if the level of some entities of the EntityType type are possible in the 
	 * THLevel defined in the village
	 * @param type
	 * @return
	 */
	public boolean checkIfEntitiesRespectTHLevel(EntityType type) {
		boolean result = true;
		short thLevel = getEntity(EntityType.TOWN_HALL, 1).getLevel();
		
		if (type.equals(EntityType.WALL)) {
			for (WallEntity e : walls) {
				short minTHLevelRequired = e.getMinTHLevelRequired();
				if (e.getNumberOfWalls() > 0 && thLevel < minTHLevelRequired) {
					result = false;
					break;
				}
			}
		} else {
			List<Entity> entities = getAllEntities(type);
			for (Entity e : entities) {
				short minTHLevelRequired = e.getMinTHLevelRequired();
				if (thLevel < minTHLevelRequired) {
					result = false;
					break;
				}
			}			
		}

		return result;
	}
	public boolean checkIfNumberOfEntitiesRespectTHLevel(EntityType type) {
		boolean result = true;
		
		short thLevel = getEntity(EntityType.TOWN_HALL, 1).getLevel();
		int maxNumberOfEntities = type.getMaximumNumberOfEntityByTH(thLevel);

		List<Entity> entities = getAllEntities(type);
		int i = 0;
		for (i = maxNumberOfEntities; i < entities.size(); i++)
			if (entities.get(i).getLevel() != (short)0) {
				result = false;
				break;
			}
		return result;
	}
	
	/**
	 * Retorna todas as entidades que possuem o tipo contido na lista "types" e que ainda podem ser atualizadas
	 * at� o level thLevel
	 * @param thLevel
	 * @param types
	 * @return
	 */
	public List<Entity> getSomeEntitiesToUpgradeUntilTHLevel(int thLevel, List<EntityType> types) {
		List<Entity> toupgrade = new ArrayList<Entity>();
		List<Entity> allEntities = getSomeEntitiesForATHLevel(thLevel, types);
		
		for (Entity e : allEntities) {
			if (e.getMaxLevelAllowedAtTHLevel(thLevel) > e.getLevel())
				toupgrade.add(e);
		}
		return toupgrade;
	}
	/**
	 * Retorna todas as entidades que n�o possuem o tipo contido na lista "types" e que ainda podem ser atualizadas
	 * at� o level thLevel
	 * @param thLevel
	 * @param except
	 * @return
	 */
	public List<Entity> getAllEntitiesToUpgradeUntilTHLevelExcept(int thLevel, List<EntityType> except) {
		List<Entity> toupgrade = new ArrayList<Entity>();
		List<Entity> allEntities = getAllEntitiesForTHLevelExcept(thLevel, except);
		
		for (Entity e : allEntities) {
			if (e.getMaxLevelAllowedAtTHLevel(thLevel) > e.getLevel())
				toupgrade.add(e);
		}
		return toupgrade;
	}
	/**
	 * Calculate the cost to maximize the village at thLevel
	 * @param thLevel
	 * @return
	 */
	public Resource getCostToMaximizeUntilTHLevel(int thLevel) {
		List<Entity> allEntities = getAllEntitiesToUpgradeUntilTHLevelExcept(thLevel, null);
		Resource cost = new Resource(0, 0, 0);

		for (Entity e : allEntities) {
			Resource costToUpgrade = e.getAccumulatedResourceToUpgradeTo(e.getMaxLevelAllowedAtTHLevel(thLevel));
			cost = cost.sum(costToUpgrade);
		}

		return cost;
	}

	/**
	 * @return
	 */
	public Resource getCostSpentUntilHere() {
		List<Entity> allEntities = getAllEntities();
		Resource cost = new Resource(0, 0, 0);

		for (Entity e : allEntities) {
			Resource costToUpgrade = e.getAccumulatedResourceToUpgradeTo(0, e.getLevel());
			cost = cost.sum(costToUpgrade);
		}

		return cost;
	}
	
	
	public Resource getCostSpentWithWallsUntilHere() {
		Resource cost = new Resource(0, 0, 0);
		
		for (int i = 0; i < walls.size(); i++) {
			int level = walls.get(i).getLevel();
			
			Resource individual = walls.get(i).getAccumulatedResourceToUpgradeTo(0, level);
			cost = cost.sum(individual);
		}
		return cost;
	}
	/**
	 * The value stored in the gold represents only gold.
	 * The value stored in the elixir field informs that the 
	 * walls can be upgraded with gold or elixir
	 */
	public Resource getCostToMaximizeWallsUnitlTHLevel(int thLevel) {
		int qtdMax = EntityType.WALL.getMaximumNumberOfEntityByTH(thLevel);
		int maxWallLevel = EntityType.WALL.getEvolutionTable().getMaxLevelAllowedAtTHLevel(thLevel);
		
		Resource cost = getCostToSendWallsToLevel(qtdMax, maxWallLevel);
		return cost;
		
	}
	private Resource getCostToSendWallsToLevel(int qtdWalls, int wallLevel) {
		Resource cost = new Resource(0, 0, 0);
		int qtdAnalyzed = 0;
		/* Check the levels higher than the maxWallLevel */
		Collections.sort(walls);
		int i = walls.size() - 1;
		for (; i >= 0; i--) {
			int wallLvl = walls.get(i).getLevel();
			if (wallLvl < wallLevel)
				break;
			else 
				qtdAnalyzed += walls.get(i).getNumberOfWalls();
		}
		
		if (qtdAnalyzed >= qtdWalls) {
			return cost;
		} 
		
		// Analyse the cost to upgrade lower level walls
		int qtdToAnalyze = qtdWalls - qtdAnalyzed;
		for (; i >= 0; i--) {
			if (qtdToAnalyze == 0) 
				break;
			int nWalls = walls.get(i).getNumberOfWalls();
			if (nWalls < qtdToAnalyze) {
				qtdToAnalyze -= nWalls;
				cost = cost.sum(walls.get(i).getAccumulatedResourceToUpgradeTo(wallLevel));
			} else {
				cost = cost.sum(walls.get(i).getAccumulatedResourceToUpgradeOneWallTo(wallLevel).multiply(qtdToAnalyze));
				qtdToAnalyze = 0;
			}
		}
		/*
		 * If qtdToAnalyse > 0, the user doesn�t create all walls (there are more walls lvl 0 than in the variable)
		 */
		if (qtdToAnalyze > 0) {
			WallEntity wallLevel0 = new WallEntity(-1, 0, qtdToAnalyze);
			cost = cost.sum(wallLevel0.getAccumulatedResourceToUpgradeTo(wallLevel));
		}
		return cost;
	}
	/**
	 * Calculate the time to maximize the village at thLevel
	 * @param thLevel
	 * @return
	 */
	public CoCTime getTimeToMaximizeBuildingsUntilTHLevel(int thLevel) {
		List<EntityType> troopsAndSpells = EntityType.getTroopsAndSpells();
		List<Entity> allEntities = getAllEntitiesToUpgradeUntilTHLevelExcept(thLevel, troopsAndSpells);
		CoCTime time = new CoCTime(0, 0, 0, 0);
		
		for (Entity e : allEntities) {
			time = time.sum(e.getAccumulatedTimeToUpgradeTo(e.getMaxLevelAllowedAtTHLevel(thLevel)));
		}
		return time;
	}

	public CoCTime getTimeSpentWithBuildingsUntilHere() {
		List<EntityType> troopsAndSpells = EntityType.getTroopsAndSpells();
		List<Entity> allEntities = getAllEntitiesExcept(troopsAndSpells);
		CoCTime time = new CoCTime(0, 0, 0, 0);
		
		for (Entity e : allEntities) {
			time = time.sum(e.getAccumulatedTimeToUpgradeTo(0, e.getLevel()));
		}
		return time;
	}
	
	public CoCTime getTimeSpentWithTroopsAndSpellsUntilHere() {
		List<EntityType> troopsAndSpells = EntityType.getTroopsAndSpells();
		List<Entity> allEntities = getAllEntities(troopsAndSpells);
		CoCTime time = new CoCTime(0, 0, 0, 0);
		
		for (Entity e : allEntities) {
			time = time.sum(e.getAccumulatedTimeToUpgradeTo(0, e.getLevel()));
		}
		return time;
	}
	/**
	 * Calculate the time to maximize the village at thLevel
	 * @param thLevel
	 * @return
	 */
	public CoCTime getTimeToMaximizeTroopsAndSpellsUntilTHLevel(int thLevel) {
		List<EntityType> troopsAndSpells = EntityType.getTroopsAndSpells();
		List<Entity> allEntities = getSomeEntitiesToUpgradeUntilTHLevel(thLevel, troopsAndSpells);
		CoCTime time = new CoCTime(0, 0, 0, 0);
		
		for (Entity e : allEntities) {
			time = time.sum(e.getAccumulatedTimeToUpgradeTo(e.getMaxLevelAllowedAtTHLevel(thLevel)));
		}
		return time;
	}

	private void initializeEntities() {
		entities = new ArrayList<Entity>();
		walls = new ArrayList<WallEntity>();
		
		/**
		 *  Nota: Esse m�todo pode ser automatizado usando as fun��es getAllTypes() e getMaximumNumberOfEntityByTH(max_TH_level), 
		 *  ambos de EntityType. Entretanto, achei mais f�cil deixar assim pra ficar claro quais coisas est�o entrando na vila e quais
		 *  n�o est�o.
		 */
		
		Entity townHall = new Entity(1, 0, EntityType.TOWN_HALL);
		Entity clanCastle = new Entity(1, 0, EntityType.CLAN_CASTLE);
		
		Entity elixirCollector1 = new Entity(1, 0, EntityType.ELIXIR_COLLECTOR);
		Entity elixirCollector2 = new Entity(2, 0, EntityType.ELIXIR_COLLECTOR);
		Entity elixirCollector3 = new Entity(3, 0, EntityType.ELIXIR_COLLECTOR);
		Entity elixirCollector4 = new Entity(4, 0, EntityType.ELIXIR_COLLECTOR);
		Entity elixirCollector5 = new Entity(5, 0, EntityType.ELIXIR_COLLECTOR);
		Entity elixirCollector6 = new Entity(6, 0, EntityType.ELIXIR_COLLECTOR);
		Entity elixirCollector7 = new Entity(7, 0, EntityType.ELIXIR_COLLECTOR);
		Entity elixirStorage1 = new Entity(1, 0, EntityType.ELIXIR_STORAGE);
		Entity elixirStorage2 = new Entity(2, 0, EntityType.ELIXIR_STORAGE);
		Entity elixirStorage3 = new Entity(3, 0, EntityType.ELIXIR_STORAGE);
		Entity elixirStorage4 = new Entity(4, 0, EntityType.ELIXIR_STORAGE);
		Entity goldMine1 = new Entity(1, 0, EntityType.GOLD_MINE);
		Entity goldMine2 = new Entity(2, 0, EntityType.GOLD_MINE);
		Entity goldMine3 = new Entity(3, 0, EntityType.GOLD_MINE);
		Entity goldMine4 = new Entity(4, 0, EntityType.GOLD_MINE);
		Entity goldMine5 = new Entity(5, 0, EntityType.GOLD_MINE);
		Entity goldMine6 = new Entity(6, 0, EntityType.GOLD_MINE);
		Entity goldMine7 = new Entity(7, 0, EntityType.GOLD_MINE);
		Entity goldStorage1 = new Entity(1, 0, EntityType.GOLD_STORAGE);
		Entity goldStorage2 = new Entity(2, 0, EntityType.GOLD_STORAGE);
		Entity goldStorage3 = new Entity(3, 0, EntityType.GOLD_STORAGE);
		Entity goldStorage4 = new Entity(4, 0, EntityType.GOLD_STORAGE);
		Entity darkElixiDrill1 = new Entity(1, 0, EntityType.DARK_ELIXIR_DRILL);
		Entity darkElixiDrill2 = new Entity(2, 0, EntityType.DARK_ELIXIR_DRILL);
		Entity darkElixiDrill3 = new Entity(3, 0, EntityType.DARK_ELIXIR_DRILL);
		Entity darkElixirStorage = new Entity(1, 0, EntityType.DARK_ELIXIR_STORAGE);

		Entity laboratory = new Entity(1, 0, EntityType.LABORATORY);
		
		Entity cannon1 = new Entity(1, 0, EntityType.CANNON);
		Entity cannon2 = new Entity(2, 0, EntityType.CANNON);
		Entity cannon3 = new Entity(3, 0, EntityType.CANNON);
		Entity cannon4 = new Entity(4, 0, EntityType.CANNON);
		Entity cannon5 = new Entity(5, 0, EntityType.CANNON);
		Entity cannon6 = new Entity(6, 0, EntityType.CANNON);
		Entity cannon7 = new Entity(7, 0, EntityType.CANNON);
		Entity archerTower1 = new Entity(1, 0, EntityType.ARCHER_TOWER);
		Entity archerTower2 = new Entity(2, 0, EntityType.ARCHER_TOWER);
		Entity archerTower3 = new Entity(3, 0, EntityType.ARCHER_TOWER);
		Entity archerTower4 = new Entity(4, 0, EntityType.ARCHER_TOWER);
		Entity archerTower5 = new Entity(5, 0, EntityType.ARCHER_TOWER);
		Entity archerTower6 = new Entity(6, 0, EntityType.ARCHER_TOWER);
		Entity archerTower7 = new Entity(7, 0, EntityType.ARCHER_TOWER);
		Entity archerTower8 = new Entity(8, 0, EntityType.ARCHER_TOWER);
		Entity wizardTower1 = new Entity(1, 0, EntityType.WIZARD_TOWER);
		Entity wizardTower2 = new Entity(2, 0, EntityType.WIZARD_TOWER);
		Entity wizardTower3 = new Entity(3, 0, EntityType.WIZARD_TOWER);
		Entity wizardTower4 = new Entity(4, 0, EntityType.WIZARD_TOWER);
		Entity wizardTower5 = new Entity(5, 0, EntityType.WIZARD_TOWER);
		Entity airDefense1 = new Entity(1, 0, EntityType.AIR_DEFENSE);
		Entity airDefense2 = new Entity(2, 0, EntityType.AIR_DEFENSE);
		Entity airDefense3 = new Entity(3, 0, EntityType.AIR_DEFENSE);
		Entity airDefense4 = new Entity(4, 0, EntityType.AIR_DEFENSE);
		Entity mortar1 = new Entity(1, 0, EntityType.MORTAR);
		Entity mortar2 = new Entity(2, 0, EntityType.MORTAR);
		Entity mortar3 = new Entity(3, 0, EntityType.MORTAR);
		Entity mortar4 = new Entity(4, 0, EntityType.MORTAR);
		Entity hiddenTesla1 = new Entity(1, 0, EntityType.HIDDEN_TESLA);
		Entity hiddenTesla2 = new Entity(2, 0, EntityType.HIDDEN_TESLA);
		Entity hiddenTesla3 = new Entity(3, 0, EntityType.HIDDEN_TESLA);
		Entity hiddenTesla4 = new Entity(4, 0, EntityType.HIDDEN_TESLA);
		Entity xBow1 = new Entity(1, 0, EntityType.XBOW);
		Entity xBow2 = new Entity(2, 0, EntityType.XBOW);
		Entity xBow3 = new Entity(3, 0, EntityType.XBOW);
		Entity xBow4 = new Entity(4, 0, EntityType.XBOW);
		Entity infernoTower1 = new Entity(1, 0, EntityType.INFERNO_TOWER);
		Entity infernoTower2 = new Entity(2, 0, EntityType.INFERNO_TOWER);
		Entity airSweeper1 = new Entity(1, 0, EntityType.AIR_SWEEPER);
		Entity airSweeper2 = new Entity(2, 0, EntityType.AIR_SWEEPER);
		Entity eagleArtillery1 = new Entity(1, 0, EntityType.EAGLE_ARTILLERY);
		
		WallEntity numWall0 = new WallEntity(0, 0, 0);
		WallEntity numWall1 = new WallEntity(1, 1, 0);
		WallEntity numWall2 = new WallEntity(2, 2, 0);
		WallEntity numWall3 = new WallEntity(3, 3, 0);
		WallEntity numWall4 = new WallEntity(4, 4, 0);
		WallEntity numWall5 = new WallEntity(5, 5, 0);
		WallEntity numWall6 = new WallEntity(6, 6, 0);
		WallEntity numWall7 = new WallEntity(7, 7, 0);
		WallEntity numWall8 = new WallEntity(8, 8, 0);
		WallEntity numWall9 = new WallEntity(9, 9, 0);
		WallEntity numWall10 = new WallEntity(10, 10, 0);
		WallEntity numWall11 = new WallEntity(11, 11, 0);
		
		Entity spellFactory = new Entity(1, 0, EntityType.SPELL_FACTORY);
		Entity darkSpellFactory = new Entity(1, 0, EntityType.DARK_SPELL_FACTORY);

		Entity armyCamp1 = new Entity(1, 0, EntityType.ARMY_CAMP);
		Entity armyCamp2 = new Entity(2, 0, EntityType.ARMY_CAMP);
		Entity armyCamp3 = new Entity(3, 0, EntityType.ARMY_CAMP);
		Entity armyCamp4 = new Entity(4, 0, EntityType.ARMY_CAMP);
		Entity barrack1 = new Entity(1, 0, EntityType.BARRACK);
		Entity barrack2 = new Entity(2, 0, EntityType.BARRACK);
		Entity barrack3 = new Entity(3, 0, EntityType.BARRACK);
		Entity barrack4 = new Entity(4, 0, EntityType.BARRACK);
		Entity darkBarrack1 = new Entity(1, 0, EntityType.DARK_BARRACK);
		Entity darkBarrack2 = new Entity(2, 0, EntityType.DARK_BARRACK);
		Entity barbarian = new Entity(1, 0, EntityType.BARBARIAN);
		Entity archer = new Entity(1, 0, EntityType.ARCHER);
		Entity giant = new Entity(1, 0, EntityType.GIANT);
		Entity goblin = new Entity(1, 0, EntityType.GOBLIN);
		Entity wallBreaker = new Entity(1, 0, EntityType.WALL_BREAKER);
		Entity balloon = new Entity(1, 0, EntityType.BALLOON);
		Entity wizard = new Entity(1, 0, EntityType.WIZARD);
		Entity healer = new Entity(1, 0, EntityType.HEALER);
		Entity dragon = new Entity(1, 0, EntityType.DRAGON);
		Entity pekka = new Entity(1, 0, EntityType.PEKKA);
		Entity babyDragon = new Entity(1, 0, EntityType.BABY_DRAGON);
		Entity miner = new Entity(1, 0, EntityType.MINER);
		Entity minion = new Entity(1, 0, EntityType.MINION);
		Entity hogRider = new Entity(1, 0, EntityType.HOG_RIDER);
		Entity valkyrie = new Entity(1, 0, EntityType.VALKYRIE);
		Entity golem = new Entity(1, 0, EntityType.GOLEM);
		Entity witch = new Entity(1, 0, EntityType.WITCH);
		Entity lavaHound = new Entity(1, 0, EntityType.LAVA_HOUND);
		Entity bowler = new Entity(1, 0, EntityType.BOWLER);
		Entity lightningSpell = new Entity(1, 0, EntityType.LIGHTNING_SPELL);
		Entity healingSpell = new Entity(1, 0, EntityType.HEALING_SPELL);
		Entity rageSpell = new Entity(1, 0, EntityType.RAGE_SPELL);
		Entity jumpSpell = new Entity(1, 0, EntityType.JUMP_SPELL);
		Entity freezeSpell = new Entity(1, 0, EntityType.FREEZE_SPELL);
		Entity cloneSpell = new Entity(1, 0, EntityType.CLONE_SPELL);
		Entity poisonSpell = new Entity(1, 0, EntityType.POISON_SPELL);
		Entity earthquakeSpell = new Entity(1, 0, EntityType.EARTHQUAKE_SPELL);
		Entity hasteSpell = new Entity(1, 0, EntityType.HASTE_SPELL);
		Entity skeletonSpell = new Entity(1, 0, EntityType.SKELETON_SPELL);

		Entity barbarianKing = new Entity(1, 0, EntityType.BARBARIAN_KING);
		Entity archerQueen = new Entity(1, 0, EntityType.ARCHER_QUEEN);
		Entity grandWarden = new Entity(1, 0, EntityType.GRAND_WARDEN);
		
		Entity bomb1 = new Entity(1, 0, EntityType.BOMB);
		Entity bomb2 = new Entity(2, 0, EntityType.BOMB);
		Entity bomb3 = new Entity(3, 0, EntityType.BOMB);
		Entity bomb4 = new Entity(4, 0, EntityType.BOMB);
		Entity bomb5 = new Entity(5, 0, EntityType.BOMB);
		Entity bomb6 = new Entity(6, 0, EntityType.BOMB);
		Entity springTrap1 = new Entity(1, 0, EntityType.SPRING_TRAP);
		Entity springTrap2 = new Entity(2, 0, EntityType.SPRING_TRAP);
		Entity springTrap3 = new Entity(3, 0, EntityType.SPRING_TRAP);
		Entity springTrap4 = new Entity(4, 0, EntityType.SPRING_TRAP);
		Entity springTrap5 = new Entity(5, 0, EntityType.SPRING_TRAP);
		Entity springTrap6 = new Entity(6, 0, EntityType.SPRING_TRAP);
		Entity giantBomb1 = new Entity(1, 0, EntityType.GIANT_BOMB);
		Entity giantBomb2 = new Entity(2, 0, EntityType.GIANT_BOMB);
		Entity giantBomb3 = new Entity(3, 0, EntityType.GIANT_BOMB);
		Entity giantBomb4 = new Entity(4, 0, EntityType.GIANT_BOMB);
		Entity giantBomb5 = new Entity(5, 0, EntityType.GIANT_BOMB);
		Entity airBomb1 = new Entity(1, 0, EntityType.AIR_BOMB);
		Entity airBomb2 = new Entity(2, 0, EntityType.AIR_BOMB);
		Entity airBomb3 = new Entity(3, 0, EntityType.AIR_BOMB);
		Entity airBomb4 = new Entity(4, 0, EntityType.AIR_BOMB);
		Entity airBomb5 = new Entity(5, 0, EntityType.AIR_BOMB);
		Entity seekingAirBomb1 = new Entity(1, 0, EntityType.SEEKING_AIR_BOMB);
		Entity seekingAirBomb2 = new Entity(2, 0, EntityType.SEEKING_AIR_BOMB);
		Entity seekingAirBomb3 = new Entity(3, 0, EntityType.SEEKING_AIR_BOMB);
		Entity seekingAirBomb4 = new Entity(4, 0, EntityType.SEEKING_AIR_BOMB);
		Entity seekingAirBomb5 = new Entity(5, 0, EntityType.SEEKING_AIR_BOMB);
		Entity skeletonTrap1 = new Entity(1, 0, EntityType.SKELETON_TRAP);
		Entity skeletonTrap2 = new Entity(2, 0, EntityType.SKELETON_TRAP);
		Entity skeletonTrap3 = new Entity(3, 0, EntityType.SKELETON_TRAP);
		
		entities.add(townHall);
		entities.add(clanCastle);
		
		entities.add(elixirCollector1);
		entities.add(elixirCollector2);
		entities.add(elixirCollector3);
		entities.add(elixirCollector4);
		entities.add(elixirCollector5);
		entities.add(elixirCollector6);
		entities.add(elixirCollector7);
		entities.add(elixirStorage1);
		entities.add(elixirStorage2);
		entities.add(elixirStorage3);
		entities.add(elixirStorage4);
		entities.add(goldMine1);
		entities.add(goldMine2);
		entities.add(goldMine3);
		entities.add(goldMine4);
		entities.add(goldMine5);
		entities.add(goldMine6);
		entities.add(goldMine7);
		entities.add(goldStorage1);
		entities.add(goldStorage2);
		entities.add(goldStorage3);
		entities.add(goldStorage4);
		entities.add(darkElixiDrill1);
		entities.add(darkElixiDrill2);
		entities.add(darkElixiDrill3);
		entities.add(darkElixirStorage);

		entities.add(laboratory);
		
		entities.add(cannon1);
		entities.add(cannon2);
		entities.add(cannon3);
		entities.add(cannon4);
		entities.add(cannon5);
		entities.add(cannon6);
		entities.add(cannon7);
		entities.add(archerTower1);
		entities.add(archerTower2);
		entities.add(archerTower3);
		entities.add(archerTower4);
		entities.add(archerTower5);
		entities.add(archerTower6);
		entities.add(archerTower7);
		entities.add(archerTower8);
		entities.add(wizardTower1);
		entities.add(wizardTower2);
		entities.add(wizardTower3);
		entities.add(wizardTower4);
		entities.add(wizardTower5);
		entities.add(airDefense1);
		entities.add(airDefense2);
		entities.add(airDefense3);
		entities.add(airDefense4);
		entities.add(mortar1);
		entities.add(mortar2);
		entities.add(mortar3);
		entities.add(mortar4);
		entities.add(hiddenTesla1);
		entities.add(hiddenTesla2);
		entities.add(hiddenTesla3);
		entities.add(hiddenTesla4);
		entities.add(xBow1);
		entities.add(xBow2);
		entities.add(xBow3);
		entities.add(xBow4);
		entities.add(infernoTower1);
		entities.add(infernoTower2);
		entities.add(airSweeper1);
		entities.add(airSweeper2);
		entities.add(eagleArtillery1);
		
		walls.add(numWall0);
		walls.add(numWall1);
		walls.add(numWall2);
		walls.add(numWall3);
		walls.add(numWall4);
		walls.add(numWall5);
		walls.add(numWall6);
		walls.add(numWall7);
		walls.add(numWall8);
		walls.add(numWall9);
		walls.add(numWall10);
		walls.add(numWall11);
		
		entities.add(spellFactory);
		entities.add(darkSpellFactory);

		entities.add(armyCamp1);
		entities.add(armyCamp2);
		entities.add(armyCamp3);
		entities.add(armyCamp4);
		entities.add(barrack1);
		entities.add(barrack2);
		entities.add(barrack3);
		entities.add(barrack4);
		entities.add(darkBarrack1);
		entities.add(darkBarrack2);
		entities.add(barbarian);
		entities.add(archer);
		entities.add(giant);
		entities.add(goblin);
		entities.add(wallBreaker);
		entities.add(balloon);
		entities.add(wizard);
		entities.add(healer);
		entities.add(dragon);
		entities.add(pekka);
		entities.add(babyDragon);
		entities.add(miner);
		entities.add(minion);
		entities.add(hogRider);
		entities.add(valkyrie);
		entities.add(golem);
		entities.add(witch);
		entities.add(lavaHound);
		entities.add(bowler);
		entities.add(lightningSpell);
		entities.add(healingSpell);
		entities.add(rageSpell);
		entities.add(jumpSpell);
		entities.add(freezeSpell);
		entities.add(cloneSpell);
		entities.add(poisonSpell);
		entities.add(earthquakeSpell);
		entities.add(hasteSpell);
		entities.add(skeletonSpell);

		entities.add(barbarianKing);
		entities.add(archerQueen);
		entities.add(grandWarden);
		
		entities.add(bomb1);
		entities.add(bomb2);
		entities.add(bomb3);
		entities.add(bomb4);
		entities.add(bomb5);
		entities.add(bomb6);
		entities.add(springTrap1);
		entities.add(springTrap2);
		entities.add(springTrap3);
		entities.add(springTrap4);
		entities.add(springTrap5);
		entities.add(springTrap6);
		entities.add(giantBomb1);
		entities.add(giantBomb2);
		entities.add(giantBomb3);
		entities.add(giantBomb4);
		entities.add(giantBomb5);
		entities.add(airBomb1);
		entities.add(airBomb2);
		entities.add(airBomb3);
		entities.add(airBomb4);
		entities.add(airBomb5);
		entities.add(seekingAirBomb1);
		entities.add(seekingAirBomb2);
		entities.add(seekingAirBomb3);
		entities.add(seekingAirBomb4);
		entities.add(seekingAirBomb5);
		entities.add(skeletonTrap1);
		entities.add(skeletonTrap2);
		entities.add(skeletonTrap3);
	}
	
	@Override
	public String toString() {
		String result = "";
		for (Entity e : entities) {
			result += e.toString() + "\n";
		}
		return result;
	}
	public String convertToString() {
		String result = "";
		result += name + "\n";
		for (Entity e : entities) {
			result += e.convertToString() + "\n";
		}
		for (WallEntity w : walls) {
			result += w.convertToString() + "\n";
		}
		// Fake a new Entity
		Entity builder = new Entity(1, numBuilder, EntityType.BUILDER);
		result += builder.convertToString() + "\n";
		
		return result;
	}
	public void convertFromString(String s) {
		StringTokenizer st = new StringTokenizer(s, "\n");
		
		/*
		 * Tem que garantir a consist�ncia do programa. Talvez a melhor forma de fazer isso seja n�o apagando as entidades, e sim
		 * lendo do arquivo e gravando nas j� existentes. E no final usar o adaptVillageToTownHallLevel
		 * Quando a Supercell insere um novo tipo (uma nova defesa, por exemplo), o programa dever� ser atualizado.
		 * Nesse caso, quando o usu�rio baixar o novo programa e for carregar o arquivo da vila nele, o programa vai dar pau, 
		 * pois a nova defesa n�o estar� no arquivo antigo. Ent�o � necess�rio criar essa nova defesa aqui.
		 */
		
		if (st.countTokens() > 1) {
			name = st.nextToken();
			initializeEntities();
		}
		while (st.hasMoreTokens()) {
			String line = st.nextToken();
			Entity entityRead = new Entity(0,0,EntityType.AIR_BOMB); // ANYTHING
			entityRead.convertFromString(line);
			
			if (entityRead.isType(EntityType.WALL)) {
				WallEntity wallRead = new WallEntity(0, 0, 0);
				wallRead.convertFromString(line);

				WallEntity w2 = walls.get(wallRead.getLevel());
				w2.setLevel(wallRead.getLevel()); // N�O DEVE ALTERAR NADA AQUI, J� QUE O LEVEL DEVE SER O MESMO
				w2.setNumberOfWalls(wallRead.getNumberOfWalls());
			} else if (entityRead.isType(EntityType.BUILDER)) {
				numBuilder = entityRead.getLevel();
			} else {
				getNEntities(entityRead.getType(), entityRead.getSequencialId()).
						get(entityRead.getSequencialId()-1).
						setLevel(entityRead.getLevel());
			}
		}
	}
	
	private void adaptVillageToTownHallLevel(int newLevel) {
		for (EntityType type : EntityType.getAllTypes()) {
			int maxLevelAllowed = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(newLevel);
			if (!type.equals(EntityType.WALL) && !type.equals(EntityType.BUILDER)) {
				// Zera tudo que n�o � poss�vel ter no TH, pra n�o manter lixo na vila
				int maxNumberEntity = type.getMaximumNumberOfEntityByTH(newLevel);
				List<Entity> entities = getAllEntities(type);
				for (int i = 0; i < maxNumberEntity; i++) {
					int newEntityLevel = entities.get(i).getLevel() > maxLevelAllowed ? maxLevelAllowed : entities.get(i).getLevel();
					entities.get(i).setLevel(newEntityLevel);
				}
				for (int i = maxNumberEntity; i < entities.size(); i++) {
					entities.get(i).setLevel(0);
				}
			}
		}
		adaptWallsToTHLevel(newLevel);
	}
	public void adaptWallsToTHLevel(int newLevel) {
		int maxQuantity = EntityType.WALL.getMaximumNumberOfEntityByTH(newLevel);
		int maxWallLevelAtTH = EntityType.WALL.getEvolutionTable().getMaxLevelAllowedAtTHLevel(newLevel);
		
		// Delete the walls where the level are higher than the allowed level
		int nDeletedWalls = 0;
		for (WallEntity w : walls) {
			if (w.getLevel() > maxWallLevelAtTH) {
				nDeletedWalls += w.getNumberOfWalls();
				w.setNumberOfWalls(0);
			}
		}
		// Add the deleted walls at higher level to the most level allowed. This is necessary to "move" the higher levels walls to lower level walls
		int totalQtd = getTotalNumberOfWalls();
		walls.get(maxWallLevelAtTH).setNumberOfWalls(walls.get(maxWallLevelAtTH).getNumberOfWalls() + nDeletedWalls);
		totalQtd = getTotalNumberOfWalls();
		
		// Reduce the number of walls to fit the maxQuantity at the town hall level
		// Delete from wall level 1 to level max
		int needSubtract = totalQtd - maxQuantity;
		if (needSubtract > 0) {
			for (int wLvl = 1; wLvl <= maxWallLevelAtTH; wLvl++) {
				int qtd = getWall(wLvl).getNumberOfWalls();
				if (qtd > 0 && qtd > needSubtract) {
					getWall(wLvl).setNumberOfWalls(qtd-needSubtract);
					break;
				} else if (qtd > 0 && qtd < needSubtract) {
					getWall(wLvl).setNumberOfWalls(0);
					needSubtract -= qtd;
				}
			}
		}
		
		getWall(0).setNumberOfWalls(maxQuantity - getTotalNumberOfWalls());
	}
	private int getTotalNumberOfWalls() {
		int qtd = 0;
		for (WallEntity w : walls) {
			if (w.getLevel() != 0)
				qtd += w.getNumberOfWalls();
		}
		return qtd;
	}
	public void setTownHallLevel(int newLevel) {
		getEntity(EntityType.TOWN_HALL, 1).setLevel(newLevel);
		adaptVillageToTownHallLevel(newLevel);
	}
	public short getTownHallLevel() {
		return getEntity(EntityType.TOWN_HALL, 1).getLevel();
	}
	public int getMaxTownHallLevel() {
		return getEntity(EntityType.TOWN_HALL, 1).getMaxLevel();
	}
	public short getNumBuilder() {
		return numBuilder;
	}
	public void setNumBuilder(int numBuilder) {
		this.numBuilder = (short)numBuilder;
	}
	public int getAvailabeSpaceInArmyCamps() {
		return ArmyCampInformationTable.getAvailableSpace(this);
	}
	public int getAvailableSpaceInSpellFactorys() {
		return SpellFactoryInformationTable.getAvailableSpace(this);
	}
	public int countItem(EntityType type) {
		List<Entity> entities = getAllEntities(type);
		int count = 0;
		for (Entity e : entities) {
			if (e.getLevel() > 0)
				count++;
		}
		return count;
	}
}
