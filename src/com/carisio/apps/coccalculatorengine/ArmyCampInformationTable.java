package com.carisio.apps.coccalculatorengine;

import java.util.List;

public class ArmyCampInformationTable {
	public static int getAvailableSpace(int armyCampLevel) {
		int result = 0;
		switch (armyCampLevel) {
			case 1:
				result = 20;
				break;
			case 2:
				result = 30;
				break;
			case 3:
				result = 35;
				break;
			case 4:
				result = 40;
				break;
			case 5:
				result = 45;
				break;
			case 6:
				result = 50;
				break;
			case 7:
				result = 55;
				break;
			case 8:
				result = 60;
				break;
		}
		return result;
	}
	
	public static int getAvailableSpace(Village v) {
		List<Entity> camps = v.getAllEntities(EntityType.ARMY_CAMP);
		int result = 0;
		for (Entity e : camps) {
			result += getAvailableSpace(e.getLevel());
		}
		return result;
	}
}
