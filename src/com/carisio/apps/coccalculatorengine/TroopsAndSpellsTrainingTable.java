package com.carisio.apps.coccalculatorengine;

import java.util.HashMap;
import java.util.Map;

public class TroopsAndSpellsTrainingTable {
	// Key is the level and LevelInfor is the training info for that level
	private Map<Short, TroopsAndSpellsTrainingTable.LevelInfo> table;
	private EntityType type;
	// Ocupied space in army camp
	private short space;
	
	public class LevelInfo {
		Resource costToTrain;
		CoCTime timeToTrain;
		
		public LevelInfo() {
			costToTrain = new Resource(0, 0, 0);
			timeToTrain = new CoCTime(0, 0, 0, 0);
		}
		public LevelInfo(Resource costToTrain, CoCTime timeToTrain) {
			this.costToTrain = costToTrain;
			this.timeToTrain = timeToTrain;
		}
		public Resource getCostToTrain() {
			return costToTrain;
		}
		public CoCTime getTimeToTrain() {
			return timeToTrain;
		}
	}
	public TroopsAndSpellsTrainingTable(EntityType type, int space) {
		this.type = type;
		this.space = (short) space;
		table = new HashMap<Short, TroopsAndSpellsTrainingTable.LevelInfo>();
		table.put(new Short((short)0), new LevelInfo());
	}
	public void addLevel(int level, Resource resourceReq, CoCTime cocTimeReq) {
		table.put(new Short((short)level), new LevelInfo(resourceReq, cocTimeReq));
	}
	public Resource getCostToTrain(int level) {
		LevelInfo info = table.get((short)level);
		return info.getCostToTrain();
	}
	public CoCTime getTimeToTrain(int level) {
		LevelInfo info = table.get((short)level);
		return info.getTimeToTrain();
	}
	public int getSpace() {
		return space;
	}
	public EntityType getType() {
		return type;
	}
	public boolean isType(EntityType t) {
		return type.equals(t);
	}

	/*****************************************/
	/* ALL TRAINING TABLE FOR CLASH OF CLANS */
	/*****************************************/
	public static Map<EntityType, TroopsAndSpellsTrainingTable> getAllTroopsAndSpellsTrainingTable() {
		Map<EntityType, TroopsAndSpellsTrainingTable> map = new HashMap<EntityType, TroopsAndSpellsTrainingTable>();
		map.put(EntityType.BARBARIAN, getBarbarianTrainingTable());
		map.put(EntityType.ARCHER, getArcherTrainingTable());
		map.put(EntityType.GIANT, getGiantTrainingTable());
		map.put(EntityType.GOBLIN, getGoblinTrainingTable());
		map.put(EntityType.WALL_BREAKER, getWallBreakerTrainingTable());
		map.put(EntityType.BALLOON, getBalloonTrainingTable());
		map.put(EntityType.WIZARD, getWizardTrainingTable());
		map.put(EntityType.HEALER, getHealerTrainingTable());
		map.put(EntityType.DRAGON, getDragonTrainingTable());
		map.put(EntityType.PEKKA, getPEKKATrainingTable());
		map.put(EntityType.BABY_DRAGON, getBabyDragonTrainingTable());
		map.put(EntityType.MINER, getMinerTrainingTable());
		map.put(EntityType.MINION, getMinionTrainingTable());
		map.put(EntityType.HOG_RIDER, getHogRiderTrainingTable());
		map.put(EntityType.VALKYRIE, getValkyrieTrainingTable());
		map.put(EntityType.GOLEM, getGolemTrainingTable());
		map.put(EntityType.WITCH, getWitchTrainingTable());
		map.put(EntityType.LAVA_HOUND, getLavaHoundTrainingTable());
		map.put(EntityType.BOWLER, getBowlerTrainingTable());
		map.put(EntityType.LIGHTNING_SPELL, getLightningSpellTrainingTable());
		map.put(EntityType.HEALING_SPELL, getHealingSpellTrainingTable());
		map.put(EntityType.RAGE_SPELL, getRageSpellTrainingTable());
		map.put(EntityType.JUMP_SPELL, getJumpSpellTrainingTable());
		map.put(EntityType.FREEZE_SPELL, getFreezeSpellTrainingTable());
		map.put(EntityType.CLONE_SPELL, getCloneSpellTrainingTable());
		map.put(EntityType.POISON_SPELL, getPoisonSpellTrainingTable());
		map.put(EntityType.EARTHQUAKE_SPELL, getEarthquakeSpellTrainingTable());
		map.put(EntityType.HASTE_SPELL, getHasteSpellTrainingTable());
		map.put(EntityType.SKELETON_SPELL, getSkeletonSpellTrainingTable());
		return map;
	}
	public static TroopsAndSpellsTrainingTable getBarbarianTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.BARBARIAN, 1);
		table.addLevel(1, new Resource(0, 25, 0), new CoCTime(0, 0, 0, 20));
		table.addLevel(2, new Resource(0, 40, 0), new CoCTime(0, 0, 0, 20));
		table.addLevel(3, new Resource(0, 60, 0), new CoCTime(0, 0, 0, 20));
		table.addLevel(4, new Resource(0, 100, 0), new CoCTime(0, 0, 0, 20));
		table.addLevel(5, new Resource(0, 150, 0), new CoCTime(0, 0, 0, 20));
		table.addLevel(6, new Resource(0, 200, 0), new CoCTime(0, 0, 0, 20));
		table.addLevel(7, new Resource(0, 250, 0), new CoCTime(0, 0, 0, 20));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getArcherTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.ARCHER, 1);
		table.addLevel(1, new Resource(0, 50, 0), new CoCTime(0, 0, 0, 25));
		table.addLevel(2, new Resource(0, 80, 0), new CoCTime(0, 0, 0, 25));
		table.addLevel(3, new Resource(0, 120, 0), new CoCTime(0, 0, 0, 25));
		table.addLevel(4, new Resource(0, 200, 0), new CoCTime(0, 0, 0, 25));
		table.addLevel(5, new Resource(0, 300, 0), new CoCTime(0, 0, 0, 25));
		table.addLevel(6, new Resource(0, 400, 0), new CoCTime(0, 0, 0, 25));
		table.addLevel(7, new Resource(0, 500, 0), new CoCTime(0, 0, 0, 25));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getGiantTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.GIANT, 5);
		table.addLevel(1, new Resource(0, 250, 0), new CoCTime(0, 0, 2, 0));
		table.addLevel(2, new Resource(0, 750, 0), new CoCTime(0, 0, 2, 0));
		table.addLevel(3, new Resource(0, 1250, 0), new CoCTime(0, 0, 2, 0));
		table.addLevel(4, new Resource(0, 1750, 0), new CoCTime(0, 0, 2, 0));
		table.addLevel(5, new Resource(0, 2250, 0), new CoCTime(0, 0, 2, 0));
		table.addLevel(6, new Resource(0, 3000, 0), new CoCTime(0, 0, 2, 0));
		table.addLevel(7, new Resource(0, 3500, 0), new CoCTime(0, 0, 2, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getGoblinTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.GOBLIN, 1);
		table.addLevel(1, new Resource(0, 25, 0), new CoCTime(0, 0, 0, 30));
		table.addLevel(2, new Resource(0, 40, 0), new CoCTime(0, 0, 0, 30));
		table.addLevel(3, new Resource(0, 60, 0), new CoCTime(0, 0, 0, 30));
		table.addLevel(4, new Resource(0, 80, 0), new CoCTime(0, 0, 0, 30));
		table.addLevel(5, new Resource(0, 100, 0), new CoCTime(0, 0, 0, 30));
		table.addLevel(6, new Resource(0, 150, 0), new CoCTime(0, 0, 0, 30));
		table.addLevel(7, new Resource(0, 200, 0), new CoCTime(0, 0, 0, 30));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getWallBreakerTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.WALL_BREAKER, 2);
		table.addLevel(1, new Resource(0, 1000, 0), new CoCTime(0, 0, 1, 0));
		table.addLevel(2, new Resource(0, 1500, 0), new CoCTime(0, 0, 1, 0));
		table.addLevel(3, new Resource(0, 2000, 0), new CoCTime(0, 0, 1, 0));
		table.addLevel(4, new Resource(0, 2500, 0), new CoCTime(0, 0, 1, 0));
		table.addLevel(5, new Resource(0, 3000, 0), new CoCTime(0, 0, 1, 0));
		table.addLevel(6, new Resource(0, 3500, 0), new CoCTime(0, 0, 1, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getBalloonTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.BALLOON, 5);
		table.addLevel(1, new Resource(0, 2000, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 2500, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 3000, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 3500, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(5, new Resource(0, 4000, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(6, new Resource(0, 4500, 0), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getWizardTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.WIZARD, 4);
		table.addLevel(1, new Resource(0, 1500, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 2000, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 2500, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 3000, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(5, new Resource(0, 3500, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(6, new Resource(0, 4000, 0), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getHealerTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.HEALER, 14);
		table.addLevel(1, new Resource(0, 5000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 6000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 8000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(4, new Resource(0, 10000, 0), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getDragonTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.DRAGON, 20);
		table.addLevel(1, new Resource(0, 25000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(2, new Resource(0, 29000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(3, new Resource(0, 33000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(4, new Resource(0, 37000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(5, new Resource(0, 42000, 0), new CoCTime(0, 0, 15, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getPEKKATrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.PEKKA, 25);
		table.addLevel(1, new Resource(0, 28000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(2, new Resource(0, 32000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(3, new Resource(0, 36000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(4, new Resource(0, 40000, 0), new CoCTime(0, 0, 15, 0));
		table.addLevel(5, new Resource(0, 45000, 0), new CoCTime(0, 0, 15, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getBabyDragonTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.BABY_DRAGON, 10);
		table.addLevel(1, new Resource(0, 15000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 16000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 17000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(4, new Resource(0, 18000, 0), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getMinerTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.MINER, 5);
		table.addLevel(1, new Resource(0, 4200, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 4800, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 5400, 0), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 6000, 0), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getMinionTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.MINION, 2);
		table.addLevel(1, new Resource(0, 0, 6), new CoCTime(0, 0, 0, 45));
		table.addLevel(2, new Resource(0, 0, 7), new CoCTime(0, 0, 0, 45));
		table.addLevel(3, new Resource(0, 0, 8), new CoCTime(0, 0, 0, 45));
		table.addLevel(4, new Resource(0, 0, 9), new CoCTime(0, 0, 0, 45));
		table.addLevel(5, new Resource(0, 0, 10), new CoCTime(0, 0, 0, 45));
		table.addLevel(6, new Resource(0, 0, 11), new CoCTime(0, 0, 0, 45));
		table.addLevel(7, new Resource(0, 0, 12), new CoCTime(0, 0, 0, 45));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getHogRiderTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.HOG_RIDER, 5);
		table.addLevel(1, new Resource(0, 0, 40), new CoCTime(0, 0, 2, 0));
		table.addLevel(2, new Resource(0, 0, 45), new CoCTime(0, 0, 2, 0));
		table.addLevel(3, new Resource(0, 0, 52), new CoCTime(0, 0, 2, 0));
		table.addLevel(4, new Resource(0, 0, 58), new CoCTime(0, 0, 2, 0));
		table.addLevel(5, new Resource(0, 0, 65), new CoCTime(0, 0, 2, 0));
		table.addLevel(6, new Resource(0, 0, 90), new CoCTime(0, 0, 2, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getValkyrieTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.VALKYRIE, 8);
		table.addLevel(1, new Resource(0, 0, 70), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 0, 100), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 0, 130), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 0, 160), new CoCTime(0, 0, 5, 0));
		table.addLevel(5, new Resource(0, 0, 190), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getGolemTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.GOLEM, 30);
		table.addLevel(1, new Resource(0, 0, 450), new CoCTime(0, 0, 15, 0));
		table.addLevel(2, new Resource(0, 0, 525), new CoCTime(0, 0, 15, 0));
		table.addLevel(3, new Resource(0, 0, 600), new CoCTime(0, 0, 15, 0));
		table.addLevel(4, new Resource(0, 0, 675), new CoCTime(0, 0, 15, 0));
		table.addLevel(5, new Resource(0, 0, 750), new CoCTime(0, 0, 15, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getLavaHoundTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.LAVA_HOUND, 30);
		table.addLevel(1, new Resource(0, 0, 390), new CoCTime(0, 0, 15, 0));
		table.addLevel(2, new Resource(0, 0, 450), new CoCTime(0, 0, 15, 0));
		table.addLevel(3, new Resource(0, 0, 510), new CoCTime(0, 0, 15, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getBowlerTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.BOWLER, 6);
		table.addLevel(1, new Resource(0, 0, 130), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 0, 150), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 0, 170), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getWitchTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.WITCH, 12);
		table.addLevel(1, new Resource(0, 0, 250), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 0, 350), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 0, 450), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getLightningSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.LIGHTNING_SPELL, 2);
		table.addLevel(1, new Resource(0, 15000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 16500, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 18000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(4, new Resource(0, 20000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(5, new Resource(0, 22000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(6, new Resource(0, 24000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(7, new Resource(0, 26000, 0), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getHealingSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.HEALING_SPELL, 2);
		table.addLevel(1, new Resource(0, 15000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 16500, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 18000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(4, new Resource(0, 20000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(5, new Resource(0, 22000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(6, new Resource(0, 24000, 0), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getRageSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.RAGE_SPELL, 2);
		table.addLevel(1, new Resource(0, 23000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 25000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 27000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(4, new Resource(0, 30000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(5, new Resource(0, 33000, 0), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getJumpSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.JUMP_SPELL, 2);
		table.addLevel(1, new Resource(0, 23000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 27000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 31000, 0), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getFreezeSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.FREEZE_SPELL, 2);
		table.addLevel(1, new Resource(0, 26000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(2, new Resource(0, 29000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(3, new Resource(0, 31000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(4, new Resource(0, 33000, 0), new CoCTime(0, 0, 10, 0));
		table.addLevel(5, new Resource(0, 35000, 0), new CoCTime(0, 0, 10, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getCloneSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.CLONE_SPELL, 2);
		table.addLevel(1, new Resource(0, 38000, 0), new CoCTime(0, 0, 20, 0));
		table.addLevel(2, new Resource(0, 40000, 0), new CoCTime(0, 0, 20, 0));
		table.addLevel(3, new Resource(0, 42000, 0), new CoCTime(0, 0, 20, 0));
		table.addLevel(4, new Resource(0, 44000, 0), new CoCTime(0, 0, 20, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getPoisonSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.POISON_SPELL, 1);
		table.addLevel(1, new Resource(0, 0, 95), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 0, 110), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 0, 125), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 0, 140), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getEarthquakeSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.EARTHQUAKE_SPELL, 1);
		table.addLevel(1, new Resource(0, 0, 125), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 0, 140), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 0, 160), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 0, 180), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getHasteSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.HASTE_SPELL, 1);
		table.addLevel(1, new Resource(0, 0, 80), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 0, 85), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 0, 90), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 0, 95), new CoCTime(0, 0, 5, 0));
		return table;
	}
	public static TroopsAndSpellsTrainingTable getSkeletonSpellTrainingTable() {
		TroopsAndSpellsTrainingTable table = new TroopsAndSpellsTrainingTable(EntityType.SKELETON_SPELL, 1);
		table.addLevel(1, new Resource(0, 0, 110), new CoCTime(0, 0, 5, 0));
		table.addLevel(2, new Resource(0, 0, 120), new CoCTime(0, 0, 5, 0));
		table.addLevel(3, new Resource(0, 0, 130), new CoCTime(0, 0, 5, 0));
		table.addLevel(4, new Resource(0, 0, 140), new CoCTime(0, 0, 5, 0));
		return table;
	}
}
