package com.carisio.apps.coccalculatorengine;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class EvolutionTable {
	// Key is the level and Requirements is the requeriments for that level
	private Map<Short, EvolutionTable.LevelRequirements> table;
	private EntityType type;
	
	public class LevelRequirements {
		Resource costToUpgrade;
		CoCTime timeToUpgrade;
		short minTH;
		
		public LevelRequirements() {
			costToUpgrade = new Resource(0, 0, 0);
			timeToUpgrade = new CoCTime(0, 0, 0, 0);
			minTH = 0;
		}
		public LevelRequirements(Resource costToUpgrade, CoCTime timeToUpgrade, int minTH) {
			this.costToUpgrade = costToUpgrade;
			this.timeToUpgrade = timeToUpgrade;
			this.minTH = (short) minTH;
		}
		public Resource getCostToUpgrade() {
			return costToUpgrade;
		}
		public CoCTime getTimeToUpgrade() {
			return timeToUpgrade;
		}
		public short getMinTH() {
			return minTH;
		}
	}

	public EvolutionTable(EntityType type) {
		this.type = type;
		table = new HashMap<Short, EvolutionTable.LevelRequirements>();
		table.put(new Short((short)0), new LevelRequirements(new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 0));
	}
	public EntityType getType() {
		return type;
	}
	public boolean isType(EntityType t) {
		return type.equals(t);
	}
	public void addLevel(int level, Resource resourceReq, CoCTime cocTimeReq, int minTH) {
		table.put(new Short((short)level), new LevelRequirements(resourceReq, cocTimeReq, minTH));
	}
	public CoCTime getAccumulatedTimeToUpgrade(int lvlFrom, int lvlTo) {
		CoCTime acc = new CoCTime(0, 0, 0, 0);
		for (int lvl = lvlFrom + 1; lvl <= lvlTo; lvl++) {
			acc = acc.sum(table.get((short)lvl).timeToUpgrade);
		}
		return acc;
	}
	public Resource getAccumulatedResourceToUpgrade(int lvlFrom, int lvlTo) {
		Resource acc = new Resource(0, 0, 0);
		for (int lvl = lvlFrom + 1; lvl <= lvlTo; lvl++) {
			acc = acc.sum(table.get((short)lvl).costToUpgrade);
		}
		return acc;
	}
	public EvolutionTable.LevelRequirements getLevelRequirements(int lvl) {
		return table.get((short)lvl);
	}
	public short getMaxLevelAllowedAtTHLevel(int thLevel) {
		Short[] allLevels = table.keySet().toArray(new Short[0]);
		Arrays.sort(allLevels);
		short maxLevel = 0;
		for (int i = 0; i < allLevels.length; i++) {
			LevelRequirements lr = table.get(allLevels[i]);
			if (lr.getMinTH() <= thLevel) {
				maxLevel = allLevels[i];
			}
		}
		return maxLevel;
	}
	
	public int getMaxLevel() {
		Short[] allLevels = table.keySet().toArray(new Short[0]);
		Arrays.sort(allLevels);
		return allLevels[allLevels.length-1];
	}
	
	
	/**********************************************/
	/* ALL EVOLUTION TABLE OF CLASH OF CLANS GAME */
	/**********************************************/
	public static EvolutionTable getBarbarianEvolutionTable() {
		EvolutionTable bev = new EvolutionTable(EntityType.BARBARIAN);
		bev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 1);
		bev.addLevel(2, new Resource(0, 50000, 0), new CoCTime(0, 6, 0, 0), 3);
		bev.addLevel(3, new Resource(0, 150000, 0), new CoCTime(1, 0, 0, 0), 5);
		bev.addLevel(4, new Resource(0, 500000, 0), new CoCTime(3, 0, 0, 0), 7);
		bev.addLevel(5, new Resource(0, 1500000, 0), new CoCTime(5, 0, 0, 0), 8);
		bev.addLevel(6, new Resource(0, 4500000, 0), new CoCTime(10, 0, 0, 0), 9);
		bev.addLevel(7, new Resource(0, 6000000, 0), new CoCTime(14, 0, 0, 0), 10);
		return bev;
	}
	public static EvolutionTable getArcherEvolutionTable() {
		EvolutionTable aev = new EvolutionTable(EntityType.ARCHER);
		aev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 1);
		aev.addLevel(2, new Resource(0, 50000, 0), new CoCTime(0, 12, 0, 0), 3);
		aev.addLevel(3, new Resource(0, 250000, 0), new CoCTime(2, 0, 0, 0), 5);
		aev.addLevel(4, new Resource(0, 750000, 0), new CoCTime(3, 0, 0, 0), 7);
		aev.addLevel(5, new Resource(0, 2250000, 0), new CoCTime(5, 0, 0, 0), 8);
		aev.addLevel(6, new Resource(0, 6000000, 0), new CoCTime(10, 0, 0, 0), 9);
		aev.addLevel(7, new Resource(0, 7500000, 0), new CoCTime(14, 0, 0, 0), 10);
		return aev;
	}
	public static EvolutionTable getGoblinEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.GOBLIN);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 2);
		ev.addLevel(2, new Resource(0, 50000, 0), new CoCTime(0, 12, 0, 0), 3);
		ev.addLevel(3, new Resource(0, 250000, 0), new CoCTime(2, 0, 0, 0), 5);
		ev.addLevel(4, new Resource(0, 750000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 2250000, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(0, 4500000, 0), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(7, new Resource(0, 6750000, 0), new CoCTime(10, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getGiantEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.GIANT);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 1);
		ev.addLevel(2, new Resource(0, 100000, 0), new CoCTime(1, 0, 0, 0), 4);
		ev.addLevel(3, new Resource(0, 250000, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(4, new Resource(0, 750000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 2250000, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(0, 5000000, 0), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(7, new Resource(0, 6000000, 0), new CoCTime(14, 0, 0, 0), 10);
		ev.addLevel(8, new Resource(0, 9500000, 0), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getWallBreakerEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.WALL_BREAKER);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 3);
		ev.addLevel(2, new Resource(0, 100000, 0), new CoCTime(1, 0, 0, 0), 4);
		ev.addLevel(3, new Resource(0, 250000, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(4, new Resource(0, 750000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 2250000, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(0, 6750000, 0), new CoCTime(10, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getBalloonEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.BALLOON);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 4);
		ev.addLevel(2, new Resource(0, 150000, 0), new CoCTime(1, 0, 0, 0), 4);
		ev.addLevel(3, new Resource(0, 450000, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(4, new Resource(0, 1350000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 2500000, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(0, 6000000, 0), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(7, new Resource(0, 9500000, 0), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getWizardEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.WIZARD);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 5);
		ev.addLevel(2, new Resource(0, 150000, 0), new CoCTime(1, 0, 0, 0), 5);
		ev.addLevel(3, new Resource(0, 450000, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(4, new Resource(0, 1350000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 2500000, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(0, 7500000, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getHealerEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.HEALER);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 6);
		ev.addLevel(2, new Resource(0, 750000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 1500000, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(0, 3000000, 0), new CoCTime(7, 0, 0, 0), 9);
		return ev;
	}
	public static EvolutionTable getDragonEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.DRAGON);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 2000000, 0), new CoCTime(7, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 3000000, 0), new CoCTime(10, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(0, 7000000, 0), new CoCTime(14, 0, 0, 0), 9);
		ev.addLevel(5, new Resource(0, 8000000, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getPEKKAEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.PEKKA);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 8);
		ev.addLevel(2, new Resource(0, 3000000, 0), new CoCTime(10, 0, 0, 0), 8);
		ev.addLevel(3, new Resource(0, 6000000, 0), new CoCTime(12, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(0, 7000000, 0), new CoCTime(14, 0, 0, 0), 10);
		ev.addLevel(5, new Resource(0, 8000000, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getBabyDragonEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.BABY_DRAGON);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 6000000, 0), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(3, new Resource(0, 7000000, 0), new CoCTime(12, 0, 0, 0), 10);
		ev.addLevel(4, new Resource(0, 8000000, 0), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getMinerEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.MINER);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 10);
		ev.addLevel(2, new Resource(0, 7500000, 0), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(3, new Resource(0, 8500000, 0), new CoCTime(12, 0, 0, 0), 11);
		ev.addLevel(4, new Resource(0, 9500000, 0), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getMinionEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.MINION);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 0, 10000), new CoCTime(5, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 0, 20000), new CoCTime(6, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(0, 0, 30000), new CoCTime(7, 0, 0, 0), 8);
		ev.addLevel(5, new Resource(0, 0, 40000), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(0, 0, 100000), new CoCTime(12, 0, 0, 0), 10);
		ev.addLevel(7, new Resource(0, 0, 140000), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getHogRiderEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.HOG_RIDER);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 0, 20000), new CoCTime(8, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 0, 30000), new CoCTime(10, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(0, 0, 40000), new CoCTime(12, 0, 0, 0), 8);
		ev.addLevel(5, new Resource(0, 0, 50000), new CoCTime(14, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(0, 0, 100000), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getValkyrieEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.VALKYRIE);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 8);
		ev.addLevel(2, new Resource(0, 0, 50000), new CoCTime(10, 0, 0, 0), 8);
		ev.addLevel(3, new Resource(0, 0, 60000), new CoCTime(12, 0, 0, 0), 9);
		ev.addLevel(4, new Resource(0, 0, 70000), new CoCTime(14, 0, 0, 0), 9);
		ev.addLevel(5, new Resource(0, 0, 110000), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getGolemEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.GOLEM);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 8);
		ev.addLevel(2, new Resource(0, 0, 60000), new CoCTime(10, 0, 0, 0), 8);
		ev.addLevel(3, new Resource(0, 0, 70000), new CoCTime(12, 0, 0, 0), 9);
		ev.addLevel(4, new Resource(0, 0, 80000), new CoCTime(14, 0, 0, 0), 9);
		ev.addLevel(5, new Resource(0, 0, 90000), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getWitchEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.WITCH);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 0, 75000), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(3, new Resource(0, 0, 160000), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getLavaHoundEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.LAVA_HOUND);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 0, 60000), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(3, new Resource(0, 0, 70000), new CoCTime(12, 0, 0, 0), 10);
		ev.addLevel(4, new Resource(0, 0, 150000), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getBowlerEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.BOWLER);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 10);
		ev.addLevel(2, new Resource(0, 0, 120000), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(3, new Resource(0, 0, 200000), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getBarbarianKingEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.BARBARIAN_KING);
		ev.addLevel(1, new Resource(0, 0, 10000), new CoCTime(0, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 0, 12500), new CoCTime(0, 12, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 0, 15000), new CoCTime(1, 0, 0, 0), 7);
		ev.addLevel(4, new Resource(0, 0, 17500), new CoCTime(1, 12, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 0, 20000), new CoCTime(2, 0, 0, 0), 7);
		ev.addLevel(6, new Resource(0, 0, 22500), new CoCTime(2, 12, 0, 0), 8);
		ev.addLevel(7, new Resource(0, 0, 25000), new CoCTime(3, 0, 0, 0), 8);
		ev.addLevel(8, new Resource(0, 0, 30000), new CoCTime(3, 12, 0, 0), 8);
		ev.addLevel(9, new Resource(0, 0, 35000), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(10, new Resource(0, 0, 40000), new CoCTime(4, 12, 0, 0), 8);
		ev.addLevel(11, new Resource(0, 0, 45000), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(12, new Resource(0, 0, 50000), new CoCTime(5, 12, 0, 0), 9);
		ev.addLevel(13, new Resource(0, 0, 55000), new CoCTime(6, 0, 0, 0), 9);
		ev.addLevel(14, new Resource(0, 0, 60000), new CoCTime(6, 12, 0, 0), 9);
		ev.addLevel(15, new Resource(0, 0, 65000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(16, new Resource(0, 0, 70000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(17, new Resource(0, 0, 75000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(18, new Resource(0, 0, 80000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(19, new Resource(0, 0, 85000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(20, new Resource(0, 0, 90000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(21, new Resource(0, 0, 95000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(22, new Resource(0, 0, 100000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(23, new Resource(0, 0, 105000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(24, new Resource(0, 0, 110000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(25, new Resource(0, 0, 115000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(26, new Resource(0, 0, 120000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(27, new Resource(0, 0, 125000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(28, new Resource(0, 0, 130000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(29, new Resource(0, 0, 135000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(30, new Resource(0, 0, 140000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(31, new Resource(0, 0, 145000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(32, new Resource(0, 0, 150000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(33, new Resource(0, 0, 155000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(34, new Resource(0, 0, 160000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(35, new Resource(0, 0, 165000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(36, new Resource(0, 0, 170000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(37, new Resource(0, 0, 175000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(38, new Resource(0, 0, 180000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(39, new Resource(0, 0, 185000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(40, new Resource(0, 0, 190000), new CoCTime(7, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getArcherQueenEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.ARCHER_QUEEN);
		ev.addLevel(1, new Resource(0, 0, 40000), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 0, 22500), new CoCTime(0, 12, 0, 0), 9);
		ev.addLevel(3, new Resource(0, 0, 25000), new CoCTime(1, 0, 0, 0), 9);
		ev.addLevel(4, new Resource(0, 0, 27500), new CoCTime(1, 12, 0, 0), 9);
		ev.addLevel(5, new Resource(0, 0, 30000), new CoCTime(2, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(0, 0, 32500), new CoCTime(2, 12, 0, 0), 9);
		ev.addLevel(7, new Resource(0, 0, 35000), new CoCTime(3, 0, 0, 0), 9);
		ev.addLevel(8, new Resource(0, 0, 40000), new CoCTime(3, 12, 0, 0), 9);
		ev.addLevel(9, new Resource(0, 0, 45000), new CoCTime(4, 0, 0, 0), 9);
		ev.addLevel(10, new Resource(0, 0, 50000), new CoCTime(4, 12, 0, 0), 9);
		ev.addLevel(11, new Resource(0, 0, 55000), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(12, new Resource(0, 0, 60000), new CoCTime(5, 12, 0, 0), 9);
		ev.addLevel(13, new Resource(0, 0, 65000), new CoCTime(6, 0, 0, 0), 9);
		ev.addLevel(14, new Resource(0, 0, 70000), new CoCTime(6, 12, 0, 0), 9);
		ev.addLevel(15, new Resource(0, 0, 75000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(16, new Resource(0, 0, 80000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(17, new Resource(0, 0, 85000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(18, new Resource(0, 0, 90000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(19, new Resource(0, 0, 95000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(20, new Resource(0, 0, 100000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(21, new Resource(0, 0, 105000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(22, new Resource(0, 0, 110000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(23, new Resource(0, 0, 115000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(24, new Resource(0, 0, 120000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(25, new Resource(0, 0, 125000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(26, new Resource(0, 0, 130000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(27, new Resource(0, 0, 135000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(28, new Resource(0, 0, 140000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(29, new Resource(0, 0, 145000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(30, new Resource(0, 0, 150000), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(31, new Resource(0, 0, 155000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(32, new Resource(0, 0, 160000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(33, new Resource(0, 0, 165000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(34, new Resource(0, 0, 170000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(35, new Resource(0, 0, 175000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(36, new Resource(0, 0, 180000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(37, new Resource(0, 0, 185000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(38, new Resource(0, 0, 190000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(39, new Resource(0, 0, 195000), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(40, new Resource(0, 0, 200000), new CoCTime(7, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getGrandWardenEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.GRAND_WARDEN);
		ev.addLevel(1, new Resource(0, 6000000, 0), new CoCTime(0, 0, 0, 0), 11);
		ev.addLevel(2, new Resource(0, 2500000, 0), new CoCTime(0, 12, 0, 0), 11);
		ev.addLevel(3, new Resource(0, 3000000, 0), new CoCTime(1, 0, 0, 0), 11);
		ev.addLevel(4, new Resource(0, 3500000, 0), new CoCTime(1, 12, 0, 0), 11);
		ev.addLevel(5, new Resource(0, 4000000, 0), new CoCTime(2, 0, 0, 0), 11);
		ev.addLevel(6, new Resource(0, 4500000, 0), new CoCTime(2, 12, 0, 0), 11);
		ev.addLevel(7, new Resource(0, 5000000, 0), new CoCTime(3, 0, 0, 0), 11);
		ev.addLevel(8, new Resource(0, 5500000, 0), new CoCTime(3, 12, 0, 0), 11);
		ev.addLevel(9, new Resource(0, 6000000, 0), new CoCTime(4, 0, 0, 0), 11);
		ev.addLevel(10, new Resource(0, 6500000, 0), new CoCTime(4, 12, 0, 0), 11);
		ev.addLevel(11, new Resource(0, 7000000, 0), new CoCTime(5, 0, 0, 0), 11);
		ev.addLevel(12, new Resource(0, 7500000, 0), new CoCTime(5, 12, 0, 0), 11);
		ev.addLevel(13, new Resource(0, 8000000, 0), new CoCTime(6, 0, 0, 0), 11);
		ev.addLevel(14, new Resource(0, 8400000, 0), new CoCTime(6, 12, 0, 0), 11);
		ev.addLevel(15, new Resource(0, 8800000, 0), new CoCTime(7, 0, 0, 0), 11);
		ev.addLevel(16, new Resource(0, 9100000, 0), new CoCTime(7, 0, 0, 0), 11);
		ev.addLevel(17, new Resource(0, 9400000, 0), new CoCTime(7, 0, 0, 0), 11);
		ev.addLevel(18, new Resource(0, 9600000, 0), new CoCTime(7, 0, 0, 0), 11);
		ev.addLevel(19, new Resource(0, 9800000, 0), new CoCTime(7, 0, 0, 0), 11);
		ev.addLevel(20, new Resource(0, 10000000, 0), new CoCTime(7, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getLightningSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.LIGHTNING_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 5);
		ev.addLevel(2, new Resource(0, 200000, 0), new CoCTime(1, 0, 0, 0), 5); 
		ev.addLevel(3, new Resource(0, 500000, 0), new CoCTime(2, 0, 0, 0), 5); 
		ev.addLevel(4, new Resource(0, 1000000, 0), new CoCTime(3, 0, 0, 0), 5);
		ev.addLevel(5, new Resource(0, 2000000, 0), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(0, 6000000, 0), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(7, new Resource(0, 8000000, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getHealingSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.HEALING_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 6);
		ev.addLevel(2, new Resource(0, 300000, 0), new CoCTime(1, 0, 0, 0), 6); 
		ev.addLevel(3, new Resource(0, 600000, 0), new CoCTime(2, 0, 0, 0), 6); 
		ev.addLevel(4, new Resource(0, 1200000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 2400000, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(0, 4800000, 0), new CoCTime(7, 0, 0, 0), 9);
		return ev;
	}
	public static EvolutionTable getRageSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.RAGE_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 450000, 0), new CoCTime(2, 0, 0, 0), 7);  
		ev.addLevel(3, new Resource(0, 900000, 0), new CoCTime(3, 0, 0, 0), 7); 
		ev.addLevel(4, new Resource(0, 1800000, 0), new CoCTime(5, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(0, 3000000, 0), new CoCTime(7, 0, 0, 0), 8);
		return ev;
	}
	public static EvolutionTable getJumpSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.JUMP_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 3000000, 0), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(3, new Resource(0, 6000000, 0), new CoCTime(7, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getFreezeSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.FREEZE_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 4000000, 0), new CoCTime(5, 0, 0, 0), 10);
		ev.addLevel(3, new Resource(0, 5000000, 0), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(4, new Resource(0, 6500000, 0), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(5, new Resource(0, 8000000, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getCloneSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.CLONE_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 10);
		ev.addLevel(2, new Resource(0, 5000000, 0), new CoCTime(8, 0, 0, 0), 10);
		ev.addLevel(3, new Resource(0, 7000000, 0), new CoCTime(10, 0, 0, 0), 11);
		ev.addLevel(4, new Resource(0, 9000000, 0), new CoCTime(12, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getPoisonSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.POISON_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 8);
		ev.addLevel(2, new Resource(0, 0, 25000), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(3, new Resource(0, 0, 50000), new CoCTime(6, 0, 0, 0), 9);
		ev.addLevel(4, new Resource(0, 0, 75000), new CoCTime(10, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getEarthquakeSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.EARTHQUAKE_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 8);
		ev.addLevel(2, new Resource(0, 0, 30000), new CoCTime(6, 0, 0, 0), 8);
		ev.addLevel(3, new Resource(0, 0, 60000), new CoCTime(8, 0, 0, 0), 9);
		ev.addLevel(4, new Resource(0, 0, 90000), new CoCTime(12, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getHasteSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.HASTE_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 0, 40000), new CoCTime(8, 0, 0, 0), 9);
		ev.addLevel(3, new Resource(0, 0, 80000), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(4, new Resource(0, 0, 100000), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getSkeletonSpellEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.SKELETON_SPELL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(0, 0, 50000), new CoCTime(8, 0, 0, 0), 10);
		ev.addLevel(3, new Resource(0, 0, 75000), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(4, new Resource(0, 0, 100000), new CoCTime(12, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getArmyCampEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.ARMY_CAMP);
		ev.addLevel(1, new Resource(0, 250, 0), new CoCTime(0, 0, 5, 0), 1);
		ev.addLevel(2, new Resource(0, 2500, 0), new CoCTime(0, 1, 0, 0), 2);
		ev.addLevel(3, new Resource(0, 10000, 0), new CoCTime(0, 3, 0, 0), 3);
		ev.addLevel(4, new Resource(0, 100000, 0), new CoCTime(0, 8, 0, 0), 4);
		ev.addLevel(5, new Resource(0, 250000, 0), new CoCTime(1, 0, 0, 0), 5);
		ev.addLevel(6, new Resource(0, 750000, 0), new CoCTime(3, 0, 0, 0), 6);
		ev.addLevel(7, new Resource(0, 2250000, 0), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(8, new Resource(0, 6750000, 0), new CoCTime(10, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getTowhHallEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.TOWN_HALL);
		ev.addLevel(1, new Resource(0, 0, 0), new CoCTime(0, 0, 0, 0), 0);
		ev.addLevel(2, new Resource(1000, 0, 0), new CoCTime(0, 0, 0, 10), 1);
		ev.addLevel(3, new Resource(4000, 0, 0), new CoCTime(0, 3, 0, 0), 2);
		ev.addLevel(4, new Resource(25000, 0, 0), new CoCTime(1, 0, 0, 0), 3);
		ev.addLevel(5, new Resource(150000, 0, 0), new CoCTime(2, 0, 0, 0), 4);
		ev.addLevel(6, new Resource(750000, 0, 0), new CoCTime(4, 0, 0, 0), 5);
		ev.addLevel(7, new Resource(1200000, 0, 0), new CoCTime(6, 0, 0, 0), 6);
		ev.addLevel(8, new Resource(2000000, 0, 0), new CoCTime(8, 0, 0, 0), 7);
		ev.addLevel(9, new Resource(3000000, 0, 0), new CoCTime(10, 0, 0, 0), 8);
		ev.addLevel(10, new Resource(5000000, 0, 0), new CoCTime(12, 0, 0, 0), 9);
		ev.addLevel(11, new Resource(7000000, 0, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getElixirCollectorEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.ELIXIR_COLLECTOR);
		ev.addLevel(1, new Resource(150, 0, 0), new CoCTime(0, 0, 0, 10), 1);
		ev.addLevel(2, new Resource(300, 0, 0), new CoCTime(0, 0, 1, 0), 1);
		ev.addLevel(3, new Resource(700, 0, 0), new CoCTime(0, 0, 15, 0), 2);
		ev.addLevel(4, new Resource(1400, 0, 0), new CoCTime(0, 1, 0, 0), 2);
		ev.addLevel(5, new Resource(3500, 0, 0), new CoCTime(0, 2, 0, 0), 3);
		ev.addLevel(6, new Resource(7000, 0, 0), new CoCTime(0, 6, 0, 0), 3);
		ev.addLevel(7, new Resource(14000, 0, 0), new CoCTime(0, 12, 0, 0), 4);
		ev.addLevel(8, new Resource(28000, 0, 0), new CoCTime(1, 0, 0, 0), 4);
		ev.addLevel(9, new Resource(56000, 0, 0), new CoCTime(2, 0, 0, 0), 5);
		ev.addLevel(10, new Resource(84000, 0, 0), new CoCTime(3, 0, 0, 0), 5);
		ev.addLevel(11, new Resource(168000, 0, 0), new CoCTime(4, 0, 0, 0), 7);
		ev.addLevel(12, new Resource(336000, 0, 0), new CoCTime(5, 0, 0, 0), 8);
		return ev;
	}
	public static EvolutionTable getElixirStorageEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.ELIXIR_STORAGE);
		ev.addLevel(1, new Resource(300, 0, 0), new CoCTime(0, 0, 0, 10), 1);
		ev.addLevel(2, new Resource(750, 0, 0), new CoCTime(0, 0, 30, 0), 2);
		ev.addLevel(3, new Resource(1500, 0, 0), new CoCTime(0, 1, 0, 0), 2);
		ev.addLevel(4, new Resource(3000, 0, 0), new CoCTime(0, 2, 0, 0), 3);
		ev.addLevel(5, new Resource(6000, 0, 0), new CoCTime(0, 3, 0, 0), 3);
		ev.addLevel(6, new Resource(12000, 0, 0), new CoCTime(0, 4, 0, 0), 3);
		ev.addLevel(7, new Resource(25000, 0, 0), new CoCTime(0, 6, 0, 0), 4);
		ev.addLevel(8, new Resource(50000, 0, 0), new CoCTime(0, 8, 0, 0), 4);
		ev.addLevel(9, new Resource(100000, 0, 0), new CoCTime(0, 12, 0, 0), 5);
		ev.addLevel(10, new Resource(250000, 0, 0), new CoCTime(1, 0, 0, 0), 6);
		ev.addLevel(11, new Resource(500000, 0, 0), new CoCTime(2, 0, 0, 0), 7);
		ev.addLevel(12, new Resource(2500000, 0, 0), new CoCTime(7, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getGoldMineEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.GOLD_MINE);
		ev.addLevel(1, new Resource(0, 150, 0), new CoCTime(0, 0, 0, 10), 1);
		ev.addLevel(2, new Resource(0, 300, 0), new CoCTime(0, 0, 1, 0), 1);
		ev.addLevel(3, new Resource(0, 700, 0), new CoCTime(0, 0, 15, 0), 2);
		ev.addLevel(4, new Resource(0, 1400, 0), new CoCTime(0, 1, 0, 0), 2);
		ev.addLevel(5, new Resource(0, 3000, 0), new CoCTime(0, 2, 0, 0), 3);
		ev.addLevel(6, new Resource(0, 7000, 0), new CoCTime(0, 6, 0, 0), 3);
		ev.addLevel(7, new Resource(0, 14000, 0), new CoCTime(0, 12, 0, 0), 4);
		ev.addLevel(8, new Resource(0, 28000, 0), new CoCTime(1, 0, 0, 0), 4);
		ev.addLevel(9, new Resource(0, 56000, 0), new CoCTime(2, 0, 0, 0), 5);
		ev.addLevel(10, new Resource(0, 84000, 0), new CoCTime(3, 0, 0, 0), 5);
		ev.addLevel(11, new Resource(0, 168000, 0), new CoCTime(4, 0, 0, 0), 7);
		ev.addLevel(12, new Resource(0, 336000, 0), new CoCTime(5, 0, 0, 0), 8);
		return ev;
	}
	public static EvolutionTable getGoldStorageEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.GOLD_STORAGE);
		ev.addLevel(1, new Resource(0, 300, 0), new CoCTime(0, 0, 0, 10), 1);
		ev.addLevel(2, new Resource(0, 750, 0), new CoCTime(0, 0, 30, 0), 2);
		ev.addLevel(3, new Resource(0, 1500, 0), new CoCTime(0, 1, 0, 0), 2);
		ev.addLevel(4, new Resource(0, 3000, 0), new CoCTime(0, 2, 0, 0), 3);
		ev.addLevel(5, new Resource(0, 6000, 0), new CoCTime(0, 3, 0, 0), 3);
		ev.addLevel(6, new Resource(0, 12000, 0), new CoCTime(0, 4, 0, 0), 3);
		ev.addLevel(7, new Resource(0, 25000, 0), new CoCTime(0, 6, 0, 0), 4);
		ev.addLevel(8, new Resource(0, 50000, 0), new CoCTime(0, 8, 0, 0), 4);
		ev.addLevel(9, new Resource(0, 100000, 0), new CoCTime(0, 12, 0, 0), 5);
		ev.addLevel(10, new Resource(0, 250000, 0), new CoCTime(1, 0, 0, 0), 6);
		ev.addLevel(11, new Resource(0, 500000, 0), new CoCTime(2, 0, 0, 0), 7);
		ev.addLevel(12, new Resource(0, 2500000, 0), new CoCTime(7, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getBarracksEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.BARRACK);
		ev.addLevel(1, new Resource(0, 200, 0), new CoCTime(0, 0, 0, 10), 1);
		ev.addLevel(2, new Resource(0, 1000, 0), new CoCTime(0, 0, 15, 0), 1);
		ev.addLevel(3, new Resource(0, 2500, 0), new CoCTime(0, 2, 0, 0), 1);
		ev.addLevel(4, new Resource(0, 5000, 0), new CoCTime(0, 4, 0, 0), 2);
		ev.addLevel(5, new Resource(0, 10000, 0), new CoCTime(0, 10, 0, 0), 3);
		ev.addLevel(6, new Resource(0, 80000, 0), new CoCTime(0, 16, 0, 0), 4);
		ev.addLevel(7, new Resource(0, 240000, 0), new CoCTime(1, 0, 0, 0), 5);
		ev.addLevel(8, new Resource(0, 700000, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(9, new Resource(0, 1500000, 0), new CoCTime(4, 0, 0, 0), 7);
		ev.addLevel(10, new Resource(0, 2000000, 0), new CoCTime(6, 0, 0, 0), 8);
		ev.addLevel(11, new Resource(0, 3000000, 0), new CoCTime(8, 0, 0, 0), 9);
		ev.addLevel(12, new Resource(0, 4000000, 0), new CoCTime(10, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getLaboratoryEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.LABORATORY);
		ev.addLevel(1, new Resource(0, 25000, 0), new CoCTime(0, 0, 30, 0), 3);
		ev.addLevel(2, new Resource(0, 50000, 0), new CoCTime(0, 5, 0, 0), 4);
		ev.addLevel(3, new Resource(0, 90000, 0), new CoCTime(0, 12, 0, 0), 5);
		ev.addLevel(4, new Resource(0, 270000, 0), new CoCTime(1, 0, 0, 0), 6);
		ev.addLevel(5, new Resource(0, 500000, 0), new CoCTime(2, 0, 0, 0), 7);
		ev.addLevel(6, new Resource(0, 1000000, 0), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(7, new Resource(0, 2500000, 0), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(8, new Resource(0, 4000000, 0), new CoCTime(6, 0, 0, 0), 10);
		ev.addLevel(9, new Resource(0, 6000000, 0), new CoCTime(7, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getCannonEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.CANNON);
		ev.addLevel(1, new Resource(250, 0, 0), new CoCTime(0, 0, 0, 10), 1);
		ev.addLevel(2, new Resource(1000, 0, 0), new CoCTime(0, 0, 15, 0), 1);
		ev.addLevel(3, new Resource(4000, 0, 0), new CoCTime(0, 0, 45, 0), 2);
		ev.addLevel(4, new Resource(16000, 0, 0), new CoCTime(0, 2, 0, 0), 3);
		ev.addLevel(5, new Resource(50000, 0, 0), new CoCTime(0, 6, 0, 0), 4);
		ev.addLevel(6, new Resource(100000, 0, 0), new CoCTime(0, 12, 0, 0), 5);
		ev.addLevel(7, new Resource(200000, 0, 0), new CoCTime(1, 0, 0, 0), 6);
		ev.addLevel(8, new Resource(400000, 0, 0), new CoCTime(2, 0, 0, 0), 7);
		ev.addLevel(9, new Resource(800000, 0, 0), new CoCTime(3, 0, 0, 0), 8);
		ev.addLevel(10, new Resource(1600000, 0, 0), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(11, new Resource(3200000, 0, 0), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(12, new Resource(6400000, 0, 0), new CoCTime(6, 0, 0, 0), 10);
		ev.addLevel(13, new Resource(7500000, 0, 0), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(14, new Resource(8500000, 0, 0), new CoCTime(8, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getArcherTowerEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.ARCHER_TOWER);
		ev.addLevel(1, new Resource(1000, 0, 0), new CoCTime(0, 0, 1, 0), 2);
		ev.addLevel(2, new Resource(2000, 0, 0), new CoCTime(0, 0, 30, 0), 2);
		ev.addLevel(3, new Resource(5000, 0, 0), new CoCTime(0, 0, 45, 0), 3);
		ev.addLevel(4, new Resource(20000, 0, 0), new CoCTime(0, 4, 0, 0), 4);
		ev.addLevel(5, new Resource(80000, 0, 0), new CoCTime(0, 12, 0, 0), 5);
		ev.addLevel(6, new Resource(180000, 0, 0), new CoCTime(1, 0, 0, 0), 5);
		ev.addLevel(7, new Resource(360000, 0, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(8, new Resource(720000, 0, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(9, new Resource(1500000, 0, 0), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(10, new Resource(2500000, 0, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(11, new Resource(4500000, 0, 0), new CoCTime(6, 0, 0, 0), 9);
		ev.addLevel(12, new Resource(6500000, 0, 0), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(13, new Resource(7500000, 0, 0), new CoCTime(8, 0, 0, 0), 10);
		ev.addLevel(14, new Resource(9000000, 0, 0), new CoCTime(9, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getWallEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.WALL);
		ev.addLevel(1, new Resource(50, 0, 0), new CoCTime(0, 0, 0, 0), 2);
		ev.addLevel(2, new Resource(1000, 0, 0), new CoCTime(0, 0, 0, 0), 2);
		ev.addLevel(3, new Resource(5000, 0, 0), new CoCTime(0, 0, 0, 0), 3);
		ev.addLevel(4, new Resource(10000, 0, 0), new CoCTime(0, 0, 0, 0), 4);
		ev.addLevel(5, new Resource(30000, 0, 0), new CoCTime(0, 0, 0, 0), 5);
		ev.addLevel(6, new Resource(75000, 0, 0), new CoCTime(0, 0, 0, 0), 6);
		ev.addLevel(7, new Resource(200000, 0, 0), new CoCTime(0, 0, 0, 0), 7);
		ev.addLevel(8, new Resource(500000, 0, 0), new CoCTime(0, 0, 0, 0), 8);
		ev.addLevel(9, new Resource(0, 1000000, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(10, new Resource(0, 3000000, 0), new CoCTime(0, 0, 0, 0), 9);
		ev.addLevel(11, new Resource(0, 4000000, 0), new CoCTime(0, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getWizardTowerEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.WIZARD);
		ev.addLevel(1, new Resource(180000, 0, 0), new CoCTime(0, 12, 0, 0), 5);
		ev.addLevel(2, new Resource(360000, 0, 0), new CoCTime(1, 0, 0, 0), 5);
		ev.addLevel(3, new Resource(720000, 0, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(4, new Resource(1280000, 0, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(5, new Resource(1960000, 0, 0), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(2680000, 0, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(7, new Resource(5360000, 0, 0), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(8, new Resource(6480000, 0, 0), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(9, new Resource(8560000, 0, 0), new CoCTime(12, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getAirDefenseEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.AIR_DEFENSE);
		ev.addLevel(1, new Resource(22500, 0, 0), new CoCTime(0, 5, 0, 0), 4);
		ev.addLevel(2, new Resource(90000, 0, 0), new CoCTime(1, 0, 0, 0), 4);
		ev.addLevel(3, new Resource(270000, 0, 0), new CoCTime(3, 0, 0, 0), 5);
		ev.addLevel(4, new Resource(540000, 0, 0), new CoCTime(5, 0, 0, 0), 6);
		ev.addLevel(5, new Resource(1080000, 0, 0), new CoCTime(6, 0, 0, 0), 7);
		ev.addLevel(6, new Resource(2160000, 0, 0), new CoCTime(8, 0, 0, 0), 8);
		ev.addLevel(7, new Resource(4320000, 0, 0), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(8, new Resource(7560000, 0, 0), new CoCTime(12, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getMortarEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.MORTAR);
		ev.addLevel(1, new Resource(8000, 0, 0), new CoCTime(0, 8, 0, 0), 3);
		ev.addLevel(2, new Resource(32000, 0, 0), new CoCTime(0, 12, 0, 0), 4);
		ev.addLevel(3, new Resource(120000, 0, 0), new CoCTime(1, 0, 0, 0), 5);
		ev.addLevel(4, new Resource(400000, 0, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(5, new Resource(800000, 0, 0), new CoCTime(4, 0, 0, 0), 7);
		ev.addLevel(6, new Resource(1600000, 0, 0), new CoCTime(5, 0, 0, 0), 8);
		ev.addLevel(7, new Resource(3200000, 0, 0), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(8, new Resource(6400000, 0, 0), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(9, new Resource(9000000, 0, 0), new CoCTime(12, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getClanCastleEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.CLAN_CASTLE);
		ev.addLevel(1, new Resource(10000, 0, 0), new CoCTime(0, 0, 0, 0), 3);
		ev.addLevel(2, new Resource(100000, 0, 0), new CoCTime(0, 6, 0, 0), 4);
		ev.addLevel(3, new Resource(800000, 0, 0), new CoCTime(1, 0, 0, 0), 6);
		ev.addLevel(4, new Resource(1800000, 0, 0), new CoCTime(2, 0, 0, 0), 8);
		ev.addLevel(5, new Resource(5000000, 0, 0), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(7000000, 0, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getHiddenTeslaEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.HIDDEN_TESLA);
		ev.addLevel(1, new Resource(1000000, 0, 0), new CoCTime(1, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(1250000, 0, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(1500000, 0, 0), new CoCTime(5, 0, 0, 0), 7);
		ev.addLevel(4, new Resource(2000000, 0, 0), new CoCTime(6, 0, 0, 0), 8);
		ev.addLevel(5, new Resource(2500000, 0, 0), new CoCTime(8, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(3000000, 0, 0), new CoCTime(10, 0, 0, 0), 8);
		ev.addLevel(7, new Resource(3500000, 0, 0), new CoCTime(12, 0, 0, 0), 9);
		ev.addLevel(8, new Resource(5000000, 0, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getSpellFactoryEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.SPELL_FACTORY);
		ev.addLevel(1, new Resource(0, 200000, 0), new CoCTime(1, 0, 0, 0), 5);
		ev.addLevel(2, new Resource(0, 400000, 0), new CoCTime(2, 0, 0, 0), 6);
		ev.addLevel(3, new Resource(0, 800000, 0), new CoCTime(4, 0, 0, 0), 7);
		ev.addLevel(4, new Resource(0, 1600000, 0), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(5, new Resource(0, 3200000, 0), new CoCTime(6, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getXBowEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.XBOW);
		ev.addLevel(1, new Resource(3000000, 0, 0), new CoCTime(7, 0, 0, 0), 9);
		ev.addLevel(2, new Resource(5000000, 0, 0), new CoCTime(10, 0, 0, 0), 9);
		ev.addLevel(3, new Resource(7000000, 0, 0), new CoCTime(14, 0, 0, 0), 9);
		ev.addLevel(4, new Resource(8000000, 0, 0), new CoCTime(14, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getDarkElixiDrillEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.DARK_ELIXIR_DRILL);
		ev.addLevel(1, new Resource(0, 1000000, 0), new CoCTime(1, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 1500000, 0), new CoCTime(2, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 2000000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(4, new Resource(0, 3000000, 0), new CoCTime(4, 0, 0, 0), 9);
		ev.addLevel(5, new Resource(0, 4000000, 0), new CoCTime(6, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(0, 5000000, 0), new CoCTime(8, 0, 0, 0), 9);
		return ev;
	}
	public static EvolutionTable getDarkElixiStorageEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.DARK_ELIXIR_STORAGE);
		ev.addLevel(1, new Resource(0, 600000, 0), new CoCTime(1, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 1200000, 0), new CoCTime(2, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 1800000, 0), new CoCTime(3, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(0, 2400000, 0), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(5, new Resource(0, 3000000, 0), new CoCTime(5, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(0, 3600000, 0), new CoCTime(6, 0, 0, 0), 9);
		return ev;
	}
	public static EvolutionTable getDarkBarracksEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.DARK_BARRACK);
		ev.addLevel(1, new Resource(0, 750000, 0), new CoCTime(3, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(0, 1250000, 0), new CoCTime(5, 0, 0, 0), 7);
		ev.addLevel(3, new Resource(0, 1750000, 0), new CoCTime(6, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(0, 2250000, 0), new CoCTime(7, 0, 0, 0), 8);
		ev.addLevel(5, new Resource(0, 2750000, 0), new CoCTime(8, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(0, 3500000, 0), new CoCTime(9, 0, 0, 0), 9);
		ev.addLevel(7, new Resource(0, 6000000, 0), new CoCTime(12, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getInfernoTowerEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.INFERNO_TOWER);
		ev.addLevel(1, new Resource(5000000, 0, 0), new CoCTime(7, 0, 0, 0), 10);
		ev.addLevel(2, new Resource(6500000, 0, 0), new CoCTime(10, 0, 0, 0), 10);
		ev.addLevel(3, new Resource(8000000, 0, 0), new CoCTime(14, 0, 0, 0), 10);
		ev.addLevel(4, new Resource(10000000, 0, 0), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}
	public static EvolutionTable getAirSweeperEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.AIR_SWEEPER);
		ev.addLevel(1, new Resource(500000, 0, 0), new CoCTime(1, 0, 0, 0), 6);
		ev.addLevel(2, new Resource(750000, 0, 0), new CoCTime(3, 0, 0, 0), 6);
		ev.addLevel(3, new Resource(1250000, 0, 0), new CoCTime(5, 0, 0, 0), 7);
		ev.addLevel(4, new Resource(2400000, 0, 0), new CoCTime(7, 0, 0, 0), 8);
		ev.addLevel(5, new Resource(4800000, 0, 0), new CoCTime(8, 0, 0, 0), 9);
		ev.addLevel(6, new Resource(7200000, 0, 0), new CoCTime(9, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getEagleArtilleryEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.EAGLE_ARTILLERY);
		ev.addLevel(1, new Resource(8000000, 0, 0), new CoCTime(10, 0, 0, 0), 11);
		ev.addLevel(2, new Resource(10000000, 0, 0), new CoCTime(14, 0, 0, 0), 11);
		return ev;
	}	
	public static EvolutionTable getDarkSpellFactoryEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.DARK_SPELL_FACTORY);
		ev.addLevel(1, new Resource(0, 1500000, 0), new CoCTime(4, 0, 0, 0), 8);
		ev.addLevel(2, new Resource(0, 2500000, 0), new CoCTime(6, 0, 0, 0), 8);
		ev.addLevel(3, new Resource(0, 3500000, 0), new CoCTime(8, 0, 0, 0), 9);
		ev.addLevel(4, new Resource(0, 4500000, 0), new CoCTime(10, 0, 0, 0), 9);
		return ev;
	}
	public static EvolutionTable getBombEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.BOMB);
		ev.addLevel(1, new Resource(400, 0, 0), new CoCTime(0, 0, 0, 0), 3);
		ev.addLevel(2, new Resource(1000, 0, 0), new CoCTime(0, 0, 15, 0), 3);
		ev.addLevel(3, new Resource(10000, 0, 0), new CoCTime(0, 2, 0, 0), 5);
		ev.addLevel(4, new Resource(100000, 0, 0), new CoCTime(0, 8, 0, 0), 7);
		ev.addLevel(5, new Resource(1000000, 0, 0), new CoCTime(1, 0, 0, 0), 8);
		ev.addLevel(6, new Resource(1500000, 0, 0), new CoCTime(2, 0, 0, 0), 9);
		return ev;
	}
	public static EvolutionTable getSpringTrapEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.SPRING_TRAP);
		ev.addLevel(1, new Resource(2000, 0, 0), new CoCTime(0, 0, 0, 0), 4);
		ev.addLevel(2, new Resource(500000, 0, 0), new CoCTime(0, 16, 0, 0), 7);
		ev.addLevel(3, new Resource(1000000, 0, 0), new CoCTime(1, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(1500000, 0, 0), new CoCTime(2, 0, 0, 0), 9);
		ev.addLevel(5, new Resource(2000000, 0, 0), new CoCTime(3, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getGiantBombEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.GIANT_BOMB);
		ev.addLevel(1, new Resource(12500, 0, 0), new CoCTime(0, 0, 0, 0), 6);
		ev.addLevel(2, new Resource(75000, 0, 0), new CoCTime(0, 6, 0, 0), 6);
		ev.addLevel(3, new Resource(750000, 0, 0), new CoCTime(1, 0, 0, 0), 8);
		ev.addLevel(4, new Resource(2500000, 0, 0), new CoCTime(3, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getAirBombEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.AIR_BOMB);
		ev.addLevel(1, new Resource(4000, 0, 0), new CoCTime(0, 0, 0, 0), 5);
		ev.addLevel(2, new Resource(20000, 0, 0), new CoCTime(0, 4, 0, 0), 5);
		ev.addLevel(3, new Resource(200000, 0, 0), new CoCTime(0, 12, 0, 0), 7);
		ev.addLevel(4, new Resource(1500000, 0, 0), new CoCTime(1, 0, 0, 0), 9);
		return ev;
	}
	public static EvolutionTable getSeekingAirMineEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.SEEKING_AIR_BOMB);
		ev.addLevel(1, new Resource(15000, 0, 0), new CoCTime(0, 0, 0, 0), 7);
		ev.addLevel(2, new Resource(2000000, 0, 0), new CoCTime(1, 0, 0, 0), 9);
		ev.addLevel(3, new Resource(4000000, 0, 0), new CoCTime(3, 0, 0, 0), 10);
		return ev;
	}
	public static EvolutionTable getSkeletonTrapEvolutionTable() {
		EvolutionTable ev = new EvolutionTable(EntityType.SKELETON_TRAP);
		ev.addLevel(1, new Resource(6000, 0, 0), new CoCTime(0, 0, 0, 0), 8);
		ev.addLevel(2, new Resource(600000, 0, 0), new CoCTime(0, 6, 0, 0), 8);
		ev.addLevel(3, new Resource(1300000, 0, 0), new CoCTime(1, 0, 0, 0), 9);
		return ev;
	}
	public static void main(String[] args) {
		System.out.println(getArcherQueenEvolutionTable().getAccumulatedResourceToUpgrade(0, 19));
	}
	
}
