package com.carisio.apps.coccalculatorengine;

import java.util.StringTokenizer;

public class Entity implements Comparable<Entity> {
	private short sequencialId;
	private short level;
	private EntityType type;
	
	/**
	 * Create an Entity
	 * @param sequencialId Represents the id for a given EntityType. All entities within an EntityType must have a unique id. The id must start in 1 and be increment by 1. 
	 * @param lvl
	 * @param type
	 */
	public Entity(int seqId, int lvl, EntityType type) {
		sequencialId = (short)seqId;
		level = (short)lvl;
		this.type = type;
	}
	public short getSequencialId() {
		return sequencialId;
	}
	public EntityType getType() {
		return type;
	}
	public EvolutionTable getEvolutionTable() {
		return type.getEvolutionTable();
	}
	public boolean isType(EntityType t) {
		return type.equals(t);
	}
	
	public short getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = (short)level;
	}
	@Override
	public String toString() {
		return type.toString() + ", ID: " + sequencialId + ", level: " + level;
	}

	@Override
	public int compareTo(Entity o) {
		return sequencialId - o.sequencialId;
	}
	public CoCTime getAccumulatedTimeToUpgradeTo(int from, int level) {
		return type.getEvolutionTable().getAccumulatedTimeToUpgrade(from, level);
	}
	public CoCTime getAccumulatedTimeToUpgradeTo(int level) {
		return type.getEvolutionTable().getAccumulatedTimeToUpgrade(this.level, level);
	}
	public Resource getAccumulatedResourceToUpgradeTo(int from, int level) {
		return type.getEvolutionTable().getAccumulatedResourceToUpgrade(from, level);
	}
	public Resource getAccumulatedResourceToUpgradeTo(int level) {
		return type.getEvolutionTable().getAccumulatedResourceToUpgrade(this.level, level);
	}
	
	/**
	 * This method checks the level of this entity and returns the minimun th level requerid
	 * for this level.
	 * @return
	 */
	public short getMinTHLevelRequired() {
		EvolutionTable.LevelRequirements lvlRequirements = type.getEvolutionTable().getLevelRequirements(level);
		return lvlRequirements.getMinTH();
	}
	
	public short getMaxLevelAllowedAtTHLevel(int thLevel) {
		return getEvolutionTable().getMaxLevelAllowedAtTHLevel(thLevel);
	}
	public int getMaxLevel() {
		return getEvolutionTable().getMaxLevel();
	}	
	public String convertToString() {
		String result = type.toString() + "\t" + sequencialId + "\t" + level;
		return result;
	}
	
	public void convertFromString(String s) {
		StringTokenizer st = new StringTokenizer(s);
		if (st.countTokens() >= 3) {
			String strType = st.nextToken();
			String strSeqId = st.nextToken();
			String strLevel = st.nextToken();
			type = EntityType.valueOf(strType);
			sequencialId = Short.valueOf(strSeqId);
			level = Short.valueOf(strLevel);
		}
	}
	
	public int getPercentageResourcesCompleted(int toLevel) {
		if (getLevel() >= toLevel)
			return 100;
		
		Resource total = getAccumulatedResourceToUpgradeTo(0, toLevel);
		Resource completed = getAccumulatedResourceToUpgradeTo(0, getLevel());
		
		if (total.getElixir() > 0) {
			return (int) ((completed.getElixir()*100)/total.getElixir());
		} else if (total.getGold() > 0) {
			return (int) ((completed.getGold()*100)/total.getGold());
		} else if (total.getDarkElixir() > 0) {
			return (int) ((completed.getDarkElixir()*100)/total.getDarkElixir());
		}
		
		return 0;
	}
	public int getPercentageTimeCompleted(int toLevel) {
		if (getLevel() >= toLevel)
			return 100;
		
		CoCTime total = getAccumulatedTimeToUpgradeTo(0, toLevel);
		CoCTime completed = getAccumulatedTimeToUpgradeTo(0, getLevel());
		
		return (int) (completed.convertToSeconds()*100/total.convertToSeconds());
	}
}