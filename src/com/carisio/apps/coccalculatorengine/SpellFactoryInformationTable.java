package com.carisio.apps.coccalculatorengine;


public class SpellFactoryInformationTable {
	public static int getAvailableSpaceInSpellFactory(int factoryLevel) {
		int result = 0;
		switch (factoryLevel) {
			case 1:
				result = 2;
				break;
			case 2:
				result = 4;
				break;
			case 3:
				result = 6;
				break;
			case 4:
				result = 8;
				break;
			case 5:
				result = 10;
				break;
		}
		return result;
	}
	public static int getAvailableSpaceInDarkSpellFactory(int darkFactoryLevel) {
		if (darkFactoryLevel >= 1)
			return 1;
		else return 0;
	}
	
	public static int getAvailableSpace(Village v) {
		int spellFactoryCapacity = v.getEntity(EntityType.SPELL_FACTORY, 1).getLevel();
		int darkSpellFactoryCapacity = v.getEntity(EntityType.DARK_SPELL_FACTORY, 1).getLevel();
		return getAvailableSpaceInSpellFactory(spellFactoryCapacity) + getAvailableSpaceInDarkSpellFactory(darkSpellFactoryCapacity);
	}
}
