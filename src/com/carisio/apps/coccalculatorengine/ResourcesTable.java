package com.carisio.apps.coccalculatorengine;

import java.util.HashMap;
import java.util.Map;

public class ResourcesTable {
	EntityType type;
	// Key is the level and value is the rate of production per hour
	private Map<Short, Long> table;
	
	public ResourcesTable(EntityType type) {
		this.type = type;
		table = new HashMap<Short, Long>();
		table.put(new Short((short)0), Long.valueOf(0));
	}
	public EntityType getType() {
		return type;
	}
	public boolean isType(EntityType t) {
		return type.equals(t);
	}
	public long getProductionPerHour(int level) {
		return table.get(new Short((short)level));
	}
	public void addLevel(int level, long productionPerHour) {
		table.put(new Short((short)level), Long.valueOf(productionPerHour));
	}
	
	/**********************************************/
	/* ALL RESOURCE TABLE OF CLASH OF CLANS GAME  */
	/**********************************************/
	public static ResourcesTable getElixirCollectorResourceTable() {
		ResourcesTable rt = new ResourcesTable(EntityType.ELIXIR_COLLECTOR);
		rt.addLevel(1, 200);
		rt.addLevel(2, 400);
		rt.addLevel(3, 600);
		rt.addLevel(4, 800);
		rt.addLevel(5, 1000);
		rt.addLevel(6, 1300);
		rt.addLevel(7, 1600);
		rt.addLevel(8, 1900);
		rt.addLevel(9, 2200);
		rt.addLevel(10, 2500);
		rt.addLevel(11, 3000);
		rt.addLevel(12, 3500);
		return rt;
	}
	public static ResourcesTable getGoldMineResourceTable() {
		ResourcesTable rt = new ResourcesTable(EntityType.GOLD_MINE);
		rt.addLevel(1, 200);
		rt.addLevel(2, 400);
		rt.addLevel(3, 600);
		rt.addLevel(4, 800);
		rt.addLevel(5, 1000);
		rt.addLevel(6, 1300);
		rt.addLevel(7, 1600);
		rt.addLevel(8, 1900);
		rt.addLevel(9, 2200);
		rt.addLevel(10, 2500);
		rt.addLevel(11, 3000);
		rt.addLevel(12, 3500);
		return rt;
	}
	public static ResourcesTable getDarkElixirDrillResourceTable() {
		ResourcesTable rt = new ResourcesTable(EntityType.DARK_ELIXIR_DRILL);
		rt.addLevel(1, 20);
		rt.addLevel(2, 30);
		rt.addLevel(3, 45);
		rt.addLevel(4, 60);
		rt.addLevel(5, 80);
		rt.addLevel(6, 100);
		return rt;
	}

}
