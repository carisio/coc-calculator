package com.carisio.apps.coccalculator.utils;

import java.util.ArrayList;
import java.util.List;

import com.carisio.apps.calculatorforclash.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

public class ViewUtils {

	public static void configureTextView(Activity activity, TextView textView) {
		final Typeface typeFace = Typeface.createFromAsset(activity.getAssets(),"fonts/Supercell-magic-webfont.ttf");
		textView.setTypeface(typeFace);	
	}
	public static void configureButton(Activity activity, Button button) {
		final Typeface typeFace = Typeface.createFromAsset(activity.getAssets(),"fonts/Supercell-magic-webfont.ttf");
		button.setTypeface(typeFace);	
	}
	public static List<String> getList(int from, int to, int step) {
		List<String> list = new ArrayList<String>();
		for (int i = from; i <= to; i = i + step)
			list.add("" + i);
		return list;
	}
	public static void configureIntegerSpinner(Spinner spinner, Activity activity, int from, int to) {
		configureIntegerSpinner(spinner, activity, from, to, 1);
	}
	
	public static void configureIntegerSpinner(Spinner spinner, Activity activity, int from, int to, int step) {
		List<String> list = getList(from, to, step);
		configureSpinner(spinner, activity, list);
	}
	
	public static void configureSpinner(Spinner spinner, Activity activity, List<String> list) {
		final Typeface typeFace = Typeface.createFromAsset(activity.getAssets(),"fonts/Supercell-magic-webfont.ttf");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, R.layout.spinner_layout, list) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
		        View v = super.getView(position, convertView, parent);
		        ((TextView) v).setTypeface(typeFace);
		        return v;
			}
			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {
				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(typeFace);
				return v;
			}
		};
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
	}
}
