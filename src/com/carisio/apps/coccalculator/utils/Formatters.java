package com.carisio.apps.coccalculator.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import com.carisio.apps.coccalculatorengine.CoCTime;

public class Formatters {
	public static String format(long resource) {
		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator(' ');
		df.setDecimalFormatSymbols(dfs);
		return df.format(resource);
	}
	public static String format(CoCTime time, String dSymbol, String hSymbol, String mSymbol, String sSymbol, String emptyString) {
		return time.toString(dSymbol, hSymbol, mSymbol, sSymbol, emptyString);
	}

}
