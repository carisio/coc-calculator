package com.carisio.apps.coccalculator.utils;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.carisio.apps.calculatorforclash.R;

public class ImageUtils {
	private static Map<String, int[]> mapEntityNameImage;
	
	public static int[] getImages(Context ctx, String entityName) {
		if (mapEntityNameImage == null) {
			createMapEntityNameImage(ctx);
		}
		return mapEntityNameImage.get(entityName);
	}
	private static void createMapEntityNameImage(Context ctx) {
		mapEntityNameImage = new HashMap<String, int[]>();
		mapEntityNameImage.put(ctx.getString(R.string.cannon), getCannonsImages());
		mapEntityNameImage.put(ctx.getString(R.string.archer_tower), getArcherTowerImages());
		mapEntityNameImage.put(ctx.getString(R.string.wizard_tower), getWizardTowerImages());
		mapEntityNameImage.put(ctx.getString(R.string.air_defense), getAirDefenseImages());
		mapEntityNameImage.put(ctx.getString(R.string.mortar), getMortarImages());
		mapEntityNameImage.put(ctx.getString(R.string.hidden_tesla), getHiddenTeslaImages());
		mapEntityNameImage.put(ctx.getString(R.string.xbow), getXBowImages());
		mapEntityNameImage.put(ctx.getString(R.string.inferno_tower), getInfernoTowerImages());
		mapEntityNameImage.put(ctx.getString(R.string.air_sweeper), getAirSweeperImages());
		mapEntityNameImage.put(ctx.getString(R.string.eagle_artillery), getEagleArtilleryImages());
		mapEntityNameImage.put(ctx.getString(R.string.gold_storage), getGoldStorageImages());
		mapEntityNameImage.put(ctx.getString(R.string.elixir_storage), getElixirStorageImages());
		mapEntityNameImage.put(ctx.getString(R.string.dark_elixir_storage), getDarkElixirStorageImages());
		mapEntityNameImage.put(ctx.getString(R.string.gold_mine), getGoldMineImages());
		mapEntityNameImage.put(ctx.getString(R.string.elixir_colector), getElixirCollectorImages());
		mapEntityNameImage.put(ctx.getString(R.string.dark_elixir_drill), getDarkElixirDrillImages());
		mapEntityNameImage.put(ctx.getString(R.string.army_camp), getArmyCampImages());
		mapEntityNameImage.put(ctx.getString(R.string.clan_castle), getClanCastleImages());
		mapEntityNameImage.put(ctx.getString(R.string.laboratory), getLaboratoryImages());
		mapEntityNameImage.put(ctx.getString(R.string.barrack), getBarrackImages());
		mapEntityNameImage.put(ctx.getString(R.string.dark_barrack), getDarkBarrackImages());
		mapEntityNameImage.put(ctx.getString(R.string.spell_factory), getSpellFactoryImages());
		mapEntityNameImage.put(ctx.getString(R.string.dark_spell_factory), getDarkSpellFactoryImages());
		mapEntityNameImage.put(ctx.getString(R.string.bomb), getBombImages());
		mapEntityNameImage.put(ctx.getString(R.string.spring_trap), getSpringTrapImages());
		mapEntityNameImage.put(ctx.getString(R.string.giant_bomb), getGiantBombImages());
		mapEntityNameImage.put(ctx.getString(R.string.air_bomb), getAirBombImages());
		mapEntityNameImage.put(ctx.getString(R.string.seeking_air_bomb), getSeekingAirBombImages());
		mapEntityNameImage.put(ctx.getString(R.string.skeleton_trap), getSkeletonTrapImages());
		mapEntityNameImage.put(ctx.getString(R.string.lightning_spell), getLightningSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.healing_spell), getHealingSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.rage_spell), getRageSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.jump_spell), getJumpSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.freeze_spell), getFreezeSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.clone_spell), getCloneSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.poison_spell), getPoisonSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.earthquake_spell), getEarthquakeSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.haste_spell), getHasteSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.skeleton_spell), getSkeletonSpellImages());
		mapEntityNameImage.put(ctx.getString(R.string.barbarian), getBarbarianImages());
		mapEntityNameImage.put(ctx.getString(R.string.archer), getArcherImages());
		mapEntityNameImage.put(ctx.getString(R.string.goblin), getGoblinImages());
		mapEntityNameImage.put(ctx.getString(R.string.giant), getGiantImages());
		mapEntityNameImage.put(ctx.getString(R.string.wall_breaker), getWallBreakerImages());
		mapEntityNameImage.put(ctx.getString(R.string.balloon), getBalloonImages());
		mapEntityNameImage.put(ctx.getString(R.string.wizard), getWizardImages());
		mapEntityNameImage.put(ctx.getString(R.string.healer), getHealerImages());
		mapEntityNameImage.put(ctx.getString(R.string.dragon), getDragonImages());
		mapEntityNameImage.put(ctx.getString(R.string.pekka), getPEKKAImages());
		mapEntityNameImage.put(ctx.getString(R.string.baby_dragon), getBabyDragonImages());
		mapEntityNameImage.put(ctx.getString(R.string.miner), getMinerImages());
		mapEntityNameImage.put(ctx.getString(R.string.minion), getMinionImages());
		mapEntityNameImage.put(ctx.getString(R.string.hog_rider), getHogRiderImages());
		mapEntityNameImage.put(ctx.getString(R.string.valkyrie), getValkyrieImages());
		mapEntityNameImage.put(ctx.getString(R.string.golem), getGolemImages());
		mapEntityNameImage.put(ctx.getString(R.string.witch), getWitchImages());
		mapEntityNameImage.put(ctx.getString(R.string.lava_hound), getLavaHoundImages());
		mapEntityNameImage.put(ctx.getString(R.string.bowler), getBowlerImages());
		mapEntityNameImage.put(ctx.getString(R.string.archer_queen), getArcherQueenImages());
		mapEntityNameImage.put(ctx.getString(R.string.grand_warden), getGrandWardenImages());
		mapEntityNameImage.put(ctx.getString(R.string.barbarian_king), getBarbarianKingImages());
		mapEntityNameImage.put(ctx.getString(R.string.wall), getWallsImages());
	}
	
	public static int[] getBarbarianKingImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king1to9,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king10to19,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus,
			R.drawable.barbarian_king20plus
		};
	}

	public static int[] getArcherQueenImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen1to9,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen10to19,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus,
			R.drawable.archer_queen20plus
		};
	}
	public static int[] getGrandWardenImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden1to9,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20,
				R.drawable.grand_warden10to20
		};
	}
	public static int[] getLavaHoundImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.lava_hound1and2,
			R.drawable.lava_hound1and2,
			R.drawable.lava_hound3,
			R.drawable.lava_hound3 // TODO: ALTERAR QUANDO A IMAGEM DELE SAIR
		};
	}
	public static int[] getBowlerImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.bowler1and2,
			R.drawable.bowler1and2,
			R.drawable.bowler3
		};
	}
	public static int[] getWitchImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.witch1and2,
			R.drawable.witch1and2,
			R.drawable.witch3
		};
	}

	public static int[] getGolemImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.golem1and2,
			R.drawable.golem1and2,
			R.drawable.golem3and4,
			R.drawable.golem3and4,
			R.drawable.golem5
		};
	}

	public static int[] getValkyrieImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.valkyrie1and2,
			R.drawable.valkyrie1and2,
			R.drawable.valkyrie3and4,
			R.drawable.valkyrie3and4,
			R.drawable.valkyrie5
		};
	}

	public static int[] getHogRiderImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.hog_rider1and2,
			R.drawable.hog_rider1and2,
			R.drawable.hog_rider3and4,
			R.drawable.hog_rider3and4,
			R.drawable.hog_rider5,
			R.drawable.hog_rider6
		};
	}

	public static int[] getMinionImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.minion1and2,
			R.drawable.minion1and2,
			R.drawable.minion3and4,
			R.drawable.minion3and4,
			R.drawable.minion5,
			R.drawable.minion6,
			R.drawable.minion7
		};
	}

	public static int[] getPEKKAImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.pekka1,
			R.drawable.pekka2,
			R.drawable.pekka3,
			R.drawable.pekka4,
			R.drawable.pekka5
		};
	}

	public static int[] getBabyDragonImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.baby_dragon1to4,
			R.drawable.baby_dragon1to4,
			R.drawable.baby_dragon1to4,
			R.drawable.baby_dragon1to4
		};
	}
	
	public static int[] getMinerImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.miner1and2,
			R.drawable.miner1and2,
			R.drawable.miner3and4,
			R.drawable.miner3and4
		};
	}

	public static int[] getDragonImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.dragon1,
			R.drawable.dragon2,
			R.drawable.dragon3,
			R.drawable.dragon4,
			R.drawable.dragon5
		};
	}

	public static int[] getHealerImages() {
		return new int[] {
			R.drawable.grass,
			R.drawable.healer1and2,
			R.drawable.healer1and2,
			R.drawable.healer3and4,
			R.drawable.healer3and4
		};
	}

	public static int[] getWizardImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.wizard1and2,
				R.drawable.wizard1and2,
				R.drawable.wizard3and4,
				R.drawable.wizard3and4,
				R.drawable.wizard5,
				R.drawable.wizard6
		};
	}

	public static int[] getBalloonImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.balloon1and2,
				R.drawable.balloon1and2,
				R.drawable.balloon3and4,
				R.drawable.balloon3and4,
				R.drawable.balloon5,
				R.drawable.balloon6,
				R.drawable.balloon7
		};
	}

	public static int[] getWallBreakerImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.wall_breaker1and2,
				R.drawable.wall_breaker1and2,
				R.drawable.wall_breaker3and4,
				R.drawable.wall_breaker3and4,
				R.drawable.wall_breaker5,
				R.drawable.wall_breaker6
		};
	}

	public static int[] getGiantImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.giant1and2,
				R.drawable.giant1and2,
				R.drawable.giant3and4,
				R.drawable.giant3and4,
				R.drawable.giant5,
				R.drawable.giant6,
				R.drawable.giant7,
				R.drawable.giant7 // TODO: trocar a imagem quando sair
		};
	}

	public static int[] getGoblinImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.goblin1and2,
				R.drawable.goblin1and2,
				R.drawable.goblin3and4,
				R.drawable.goblin3and4,
				R.drawable.goblin5,
				R.drawable.goblin6,
				R.drawable.goblin7
		};
	}

	public static int[] getBarbarianImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.barbarian1and2,
				R.drawable.barbarian1and2,
				R.drawable.barbarian3and4,
				R.drawable.barbarian3and4,
				R.drawable.barbarian5,
				R.drawable.barbarian6,
				R.drawable.barbarian7
		};
	}

	public static int[] getArcherImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.archer1and2,
				R.drawable.archer1and2,
				R.drawable.archer3and4,
				R.drawable.archer3and4,
				R.drawable.archer5,
				R.drawable.archer6,
				R.drawable.archer7
		};
	}

	public static int[] getHasteSpellImages() {
		return new int[] {
				R.drawable.haste_spell,
				R.drawable.haste_spell,
				R.drawable.haste_spell,
				R.drawable.haste_spell,
				R.drawable.haste_spell
		};
	}
	
	public static int[] getSkeletonSpellImages() {
		return new int[] {
				R.drawable.skeleton_spell,
				R.drawable.skeleton_spell,
				R.drawable.skeleton_spell,
				R.drawable.skeleton_spell,
				R.drawable.skeleton_spell
		};
	}

	public static int[] getEarthquakeSpellImages() {
		return new int[] {
				R.drawable.earthquake_spell,
				R.drawable.earthquake_spell,
				R.drawable.earthquake_spell,
				R.drawable.earthquake_spell,
				R.drawable.earthquake_spell
		};
	}

	public static int[] getPoisonSpellImages() {
		return new int[] {
				R.drawable.poison_spell,
				R.drawable.poison_spell,
				R.drawable.poison_spell,
				R.drawable.poison_spell,
				R.drawable.poison_spell
		};
	}

	public static int[] getFreezeSpellImages() {
		return new int[] {
				R.drawable.freeze_spell,
				R.drawable.freeze_spell,
				R.drawable.freeze_spell,
				R.drawable.freeze_spell,
				R.drawable.freeze_spell,
				R.drawable.freeze_spell
		};
	}

	public static int[] getCloneSpellImages() {
		return new int[] {
				R.drawable.clone_spell,
				R.drawable.clone_spell,
				R.drawable.clone_spell,
				R.drawable.clone_spell,
				R.drawable.clone_spell
		};
	}

	public static int[] getJumpSpellImages() {
		return new int[] {
				R.drawable.jump_spell,
				R.drawable.jump_spell,
				R.drawable.jump_spell,
				R.drawable.jump_spell
		};
	}

	public static int[] getRageSpellImages() {
		return new int[] {
				R.drawable.rage_spell,
				R.drawable.rage_spell,
				R.drawable.rage_spell,
				R.drawable.rage_spell,
				R.drawable.rage_spell,
				R.drawable.rage_spell
		};
	}

	public static int[] getHealingSpellImages() {
		return new int[] {
				R.drawable.healing_spell,
				R.drawable.healing_spell,
				R.drawable.healing_spell,
				R.drawable.healing_spell,
				R.drawable.healing_spell,
				R.drawable.healing_spell,
				R.drawable.healing_spell
		};
	}

	public static int[] getLightningSpellImages() {
		return new int[] {
				R.drawable.lightning_spell,
				R.drawable.lightning_spell,
				R.drawable.lightning_spell,
				R.drawable.lightning_spell,
				R.drawable.lightning_spell,
				R.drawable.lightning_spell,
				R.drawable.lightning_spell,
				R.drawable.lightning_spell
		};
	}

	public static int[] getTownHownImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.th1,
				R.drawable.th2,
				R.drawable.th3,
				R.drawable.th4,
				R.drawable.th5,
				R.drawable.th6,
				R.drawable.th7,
				R.drawable.th8,
				R.drawable.th9,
				R.drawable.th10,
				R.drawable.th11
		};
	}

	public static int[] getSkeletonTrapImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.skeleton_trap1and2,
				R.drawable.skeleton_trap1and2,
				R.drawable.skeleton_trap3
		};
	}

	public static int[] getSeekingAirBombImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.seeking_air_trap1and2,
				R.drawable.seeking_air_trap1and2,
				R.drawable.seeking_air_trap3
		};
	}
	public static int[] getAirBombImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.air_bomb1and2,
				R.drawable.air_bomb1and2,
				R.drawable.air_bomb3and4,
				R.drawable.air_bomb3and4
		};
	}

	public static int[] getGiantBombImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.giant_bomb1and2,
				R.drawable.giant_bomb1and2,
				R.drawable.giant_bomb3and4,
				R.drawable.giant_bomb3and4
		};
	}

	public static int[] getSpringTrapImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.spring_trap1and2,
				R.drawable.spring_trap1and2,
				R.drawable.spring_trap3and4,
				R.drawable.spring_trap3and4,
				R.drawable.spring_trap5
		};
	}

	public static int[] getBombImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.bomb1and2,
				R.drawable.bomb1and2,
				R.drawable.bomb3and4,
				R.drawable.bomb3and4,
				R.drawable.bomb5and6,
				R.drawable.bomb5and6
		};
	}

	public static int[] getDarkSpellFactoryImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.dark_spell_factory1,
				R.drawable.dark_spell_factory2,
				R.drawable.dark_spell_factory3,
				R.drawable.dark_spell_factory4
		};
	}

	public static int[] getSpellFactoryImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.spell_factory1,
				R.drawable.spell_factory2,
				R.drawable.spell_factory3,
				R.drawable.spell_factory4,
				R.drawable.spell_factory5
		};
	}

	public static int[] getDarkBarrackImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.dark_barracks1,
				R.drawable.dark_barracks2,
				R.drawable.dark_barracks3,
				R.drawable.dark_barracks4,
				R.drawable.dark_barracks5,
				R.drawable.dark_barracks6,
				R.drawable.dark_barracks7
		};
	}

	public static int[] getBarrackImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.barracks1,
				R.drawable.barracks2,
				R.drawable.barracks3,
				R.drawable.barracks4,
				R.drawable.barracks5,
				R.drawable.barracks6,
				R.drawable.barracks7,
				R.drawable.barracks8,
				R.drawable.barracks9,
				R.drawable.barracks10,
				R.drawable.barracks11,
				R.drawable.barracks12
		};
	}

	public static int[] getLaboratoryImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.laboratory1,
				R.drawable.laboratory2,
				R.drawable.laboratory3,
				R.drawable.laboratory4,
				R.drawable.laboratory5,
				R.drawable.laboratory6,
				R.drawable.laboratory7,
				R.drawable.laboratory8,
				R.drawable.laboratory9
		};
	}

	public static int[] getClanCastleImages() {
		return new int[] {
				R.drawable.clan_castle0,
				R.drawable.clan_castle1,
				R.drawable.clan_castle2,
				R.drawable.clan_castle3,
				R.drawable.clan_castle4,
				R.drawable.clan_castle5,
				R.drawable.clan_castle6
		};
	}

	public static int[] getArmyCampImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.army_camp1,
				R.drawable.army_camp2,
				R.drawable.army_camp3,
				R.drawable.army_camp4,
				R.drawable.army_camp5,
				R.drawable.army_camp6,
				R.drawable.army_camp7,
				R.drawable.army_camp8
		};
	}

	public static int[] getDarkElixirDrillImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.dark_elixir_drill1,
				R.drawable.dark_elixir_drill2,
				R.drawable.dark_elixir_drill3,
				R.drawable.dark_elixir_drill4,
				R.drawable.dark_elixir_drill5,
				R.drawable.dark_elixir_drill6
		};
	}

	public static int[] getElixirCollectorImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.elixir_collector1,
				R.drawable.elixir_collector2,
				R.drawable.elixir_collector3,
				R.drawable.elixir_collector4,
				R.drawable.elixir_collector5,
				R.drawable.elixir_collector6,
				R.drawable.elixir_collector7,
				R.drawable.elixir_collector8,
				R.drawable.elixir_collector9,
				R.drawable.elixir_collector10,
				R.drawable.elixir_collector11,
				R.drawable.elixir_collector12
		};

	}

	public static int[] getGoldMineImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.gold_mine1,
				R.drawable.gold_mine2,
				R.drawable.gold_mine3,
				R.drawable.gold_mine4,
				R.drawable.gold_mine5,
				R.drawable.gold_mine6,
				R.drawable.gold_mine7,
				R.drawable.gold_mine8,
				R.drawable.gold_mine9,
				R.drawable.gold_mine10,
				R.drawable.gold_mine11,
				R.drawable.gold_mine12
		};

	}

	public static int[] getDarkElixirStorageImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.dark_elixir_storage1,
				R.drawable.dark_elixir_storage2,
				R.drawable.dark_elixir_storage3,
				R.drawable.dark_elixir_storage4,
				R.drawable.dark_elixir_storage5,
				R.drawable.dark_elixir_storage6
		};
	}

	public static int[] getElixirStorageImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.elixir_storage1,
				R.drawable.elixir_storage2,
				R.drawable.elixir_storage3,
				R.drawable.elixir_storage4,
				R.drawable.elixir_storage5,
				R.drawable.elixir_storage6,
				R.drawable.elixir_storage7,
				R.drawable.elixir_storage8,
				R.drawable.elixir_storage9,
				R.drawable.elixir_storage10,
				R.drawable.elixir_storage11,
				R.drawable.elixir_storage12
		};
	}

	public static int[] getGoldStorageImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.gold_storage1,
				R.drawable.gold_storage2,
				R.drawable.gold_storage3,
				R.drawable.gold_storage4,
				R.drawable.gold_storage5,
				R.drawable.gold_storage6,
				R.drawable.gold_storage7,
				R.drawable.gold_storage8,
				R.drawable.gold_storage9,
				R.drawable.gold_storage10,
				R.drawable.gold_storage11,
				R.drawable.gold_storage12
		};
	}
	
	public static int[] getWallsImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.wall1,
				R.drawable.wall2,
				R.drawable.wall3,
				R.drawable.wall4,
				R.drawable.wall5,
				R.drawable.wall6,
				R.drawable.wall7,
				R.drawable.wall8,
				R.drawable.wall9,
				R.drawable.wall10,
				R.drawable.wall11
		};
	}
	
	public static int[] getCannonsImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.cannon1,
				R.drawable.cannon2,
				R.drawable.cannon3,
				R.drawable.cannon4,
				R.drawable.cannon5,
				R.drawable.cannon6,
				R.drawable.cannon7,
				R.drawable.cannon8,
				R.drawable.cannon9,
				R.drawable.cannon10,
				R.drawable.cannon11,
				R.drawable.cannon12,
				R.drawable.cannon13,
				R.drawable.cannon14
		};
	}
	
	public static int[] getArcherTowerImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.archer_tower1,
				R.drawable.archer_tower2,
				R.drawable.archer_tower3,
				R.drawable.archer_tower4,
				R.drawable.archer_tower5,
				R.drawable.archer_tower6,
				R.drawable.archer_tower7,
				R.drawable.archer_tower8,
				R.drawable.archer_tower9,
				R.drawable.archer_tower10,
				R.drawable.archer_tower11,
				R.drawable.archer_tower12,
				R.drawable.archer_tower13,
				R.drawable.archer_tower14
		};
	}
	
	public static int[] getWizardTowerImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.wizard_tower1,
				R.drawable.wizard_tower2,
				R.drawable.wizard_tower3,
				R.drawable.wizard_tower4,
				R.drawable.wizard_tower5,
				R.drawable.wizard_tower6,
				R.drawable.wizard_tower7,
				R.drawable.wizard_tower8,
				R.drawable.wizard_tower9
		};
	}
	
	public static int[] getAirDefenseImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.air_defense1,
				R.drawable.air_defense2,
				R.drawable.air_defense3,
				R.drawable.air_defense4,
				R.drawable.air_defense5,
				R.drawable.air_defense6,
				R.drawable.air_defense7,
				R.drawable.air_defense8
		};
	}
	
	public static int[] getMortarImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.mortar1,
				R.drawable.mortar2,
				R.drawable.mortar3,
				R.drawable.mortar4,
				R.drawable.mortar5,
				R.drawable.mortar6,
				R.drawable.mortar7,
				R.drawable.mortar8,
				R.drawable.mortar9
		};
	}
	
	public static int[] getHiddenTeslaImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.hidden_tesla1,
				R.drawable.hidden_tesla2,
				R.drawable.hidden_tesla3,
				R.drawable.hidden_tesla4,
				R.drawable.hidden_tesla5,
				R.drawable.hidden_tesla6,
				R.drawable.hidden_tesla7,
				R.drawable.hidden_tesla8
		};
	}
	
	public static int[] getXBowImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.xbow1,
				R.drawable.xbow2,
				R.drawable.xbow3,
				R.drawable.xbow4
		};
	}
	
	public static int[] getInfernoTowerImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.inferno_tower1,
				R.drawable.inferno_tower2,
				R.drawable.inferno_tower3,
				R.drawable.inferno_tower4
		};
	}
	
	public static int[] getAirSweeperImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.air_sweeper1,
				R.drawable.air_sweeper2,
				R.drawable.air_sweeper3,
				R.drawable.air_sweeper4,
				R.drawable.air_sweeper5,
				R.drawable.air_sweeper6
		};
	}
	public static int[] getEagleArtilleryImages() {
		return new int[] {
				R.drawable.grass,
				R.drawable.eagle_artillery1,
				R.drawable.eagle_artillary2
		};
	}
	public static int getBuilderHutImage() {
		return R.drawable.builder_hut;
	}
	
	public static int[] getResourcesImages() {
		return new int[] {
			R.drawable.gold,
			R.drawable.elixir,
			R.drawable.dark_elixir
		};
	}
}
