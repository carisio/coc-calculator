
package com.carisio.apps.coccalculator.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculatorengine.EntityType;
import com.carisio.apps.coccalculatorengine.Village;

public class Global {
	private static Village village;
	private static String ARQUIVO = "village.txt";
	private Global() {
		
	}
	
	public static Village getVillage() {
		if (village == null) {
			village = new Village("Village");
			
//			village.getEntity(EntityType.TOWN_HALL, 1).setLevel(10);
//			village.getEntity(EntityType.CLAN_CASTLE, 1).setLevel(6);
//			
//			village.getEntity(EntityType.ELIXIR_COLLECTOR, 1).setLevel(12);
//			village.getEntity(EntityType.ELIXIR_COLLECTOR, 2).setLevel(12);
//			village.getEntity(EntityType.ELIXIR_COLLECTOR, 3).setLevel(12);
//			village.getEntity(EntityType.ELIXIR_COLLECTOR, 4).setLevel(12);
//			village.getEntity(EntityType.ELIXIR_COLLECTOR, 5).setLevel(12);
//			village.getEntity(EntityType.ELIXIR_COLLECTOR, 6).setLevel(12);
//			village.getEntity(EntityType.ELIXIR_COLLECTOR, 7).setLevel(12);
//			village.getEntity(EntityType.ELIXIR_STORAGE, 1).setLevel(11);
//			village.getEntity(EntityType.ELIXIR_STORAGE, 2).setLevel(11);
//			village.getEntity(EntityType.ELIXIR_STORAGE, 3).setLevel(11);
//			village.getEntity(EntityType.ELIXIR_STORAGE, 4).setLevel(11);
//			village.getEntity(EntityType.GOLD_MINE, 1).setLevel(12);
//			village.getEntity(EntityType.GOLD_MINE, 2).setLevel(12);
//			village.getEntity(EntityType.GOLD_MINE, 3).setLevel(12);
//			village.getEntity(EntityType.GOLD_MINE, 4).setLevel(12);
//			village.getEntity(EntityType.GOLD_MINE, 5).setLevel(12);
//			village.getEntity(EntityType.GOLD_MINE, 6).setLevel(12);
//			village.getEntity(EntityType.GOLD_MINE, 7).setLevel(12);
//			village.getEntity(EntityType.GOLD_STORAGE, 1).setLevel(11);
//			village.getEntity(EntityType.GOLD_STORAGE, 2).setLevel(11);
//			village.getEntity(EntityType.GOLD_STORAGE, 3).setLevel(11);
//			village.getEntity(EntityType.GOLD_STORAGE, 4).setLevel(11);
//			village.getEntity(EntityType.DARK_ELIXIR_DRILL, 1).setLevel(6);
//			village.getEntity(EntityType.DARK_ELIXIR_DRILL, 2).setLevel(6);
//			village.getEntity(EntityType.DARK_ELIXIR_DRILL, 3).setLevel(6);
//			village.getEntity(EntityType.DARK_ELIXIR_STORAGE, 1).setLevel(6);
//
//			village.getEntity(EntityType.LABORATORY, 1).setLevel(8);
//			
//			village.getEntity(EntityType.CANNON, 1).setLevel(13);
//			village.getEntity(EntityType.CANNON, 2).setLevel(13);
//			village.getEntity(EntityType.CANNON, 3).setLevel(13);
//			village.getEntity(EntityType.CANNON, 4).setLevel(13);
//			village.getEntity(EntityType.CANNON, 5).setLevel(13);
//			village.getEntity(EntityType.CANNON, 6).setLevel(13);
//			village.getEntity(EntityType.ARCHER_TOWER, 1).setLevel(13);
//			village.getEntity(EntityType.ARCHER_TOWER, 2).setLevel(13);
//			village.getEntity(EntityType.ARCHER_TOWER, 3).setLevel(13);
//			village.getEntity(EntityType.ARCHER_TOWER, 4).setLevel(13);
//			village.getEntity(EntityType.ARCHER_TOWER, 5).setLevel(13);
//			village.getEntity(EntityType.ARCHER_TOWER, 6).setLevel(13);
//			village.getEntity(EntityType.ARCHER_TOWER, 7).setLevel(13);
//			village.getEntity(EntityType.WIZARD_TOWER, 1).setLevel(8);
//			village.getEntity(EntityType.WIZARD_TOWER, 2).setLevel(8);
//			village.getEntity(EntityType.WIZARD_TOWER, 3).setLevel(8);
//			village.getEntity(EntityType.WIZARD_TOWER, 4).setLevel(8);
//			village.getEntity(EntityType.AIR_DEFENSE, 1).setLevel(8);
//			village.getEntity(EntityType.AIR_DEFENSE, 2).setLevel(8);
//			village.getEntity(EntityType.AIR_DEFENSE, 3).setLevel(8);
//			village.getEntity(EntityType.AIR_DEFENSE, 4).setLevel(8);
//			village.getEntity(EntityType.MORTAR, 1).setLevel(8);
//			village.getEntity(EntityType.MORTAR, 2).setLevel(8);
//			village.getEntity(EntityType.MORTAR, 3).setLevel(8);
//			village.getEntity(EntityType.MORTAR, 4).setLevel(8);
//			village.getEntity(EntityType.HIDDEN_TESLA, 1).setLevel(8);
//			village.getEntity(EntityType.HIDDEN_TESLA, 2).setLevel(8);
//			village.getEntity(EntityType.HIDDEN_TESLA, 3).setLevel(8);
//			village.getEntity(EntityType.HIDDEN_TESLA, 4).setLevel(8);
//			village.getEntity(EntityType.XBOW, 1).setLevel(4);
//			village.getEntity(EntityType.XBOW, 2).setLevel(4);
//			village.getEntity(EntityType.XBOW, 3).setLevel(4);
//			village.getEntity(EntityType.INFERNO_TOWER, 1).setLevel(3);
//			village.getEntity(EntityType.INFERNO_TOWER, 2).setLevel(3);
//			village.getEntity(EntityType.AIR_SWEEPER, 1).setLevel(6);
//			village.getEntity(EntityType.AIR_SWEEPER, 2).setLevel(6);
//			
//			village.getEntity(EntityType.SPELL_FACTORY, 1).setLevel(5);
//			village.getEntity(EntityType.DARK_SPELL_FACTORY, 1).setLevel(3);
//
//			village.getEntity(EntityType.ARMY_CAMP, 1).setLevel(8);
//			village.getEntity(EntityType.ARMY_CAMP, 2).setLevel(8);
//			village.getEntity(EntityType.ARMY_CAMP, 3).setLevel(8);
//			village.getEntity(EntityType.ARMY_CAMP, 4).setLevel(8);
//			village.getEntity(EntityType.BARRACK, 1).setLevel(10);
//			village.getEntity(EntityType.BARRACK, 2).setLevel(10);
//			village.getEntity(EntityType.BARRACK, 3).setLevel(10);
//			village.getEntity(EntityType.BARRACK, 4).setLevel(10);
//			village.getEntity(EntityType.DARK_BARRACK, 1).setLevel(6);
//			village.getEntity(EntityType.DARK_BARRACK, 2).setLevel(6);
//			village.getEntity(EntityType.BARBARIAN, 1).setLevel(7);
//			village.getEntity(EntityType.ARCHER, 1).setLevel(7);
//			village.getEntity(EntityType.GIANT, 1).setLevel(7);
//			village.getEntity(EntityType.GOBLIN, 1).setLevel(6);
//			village.getEntity(EntityType.WALL_BREAKER, 1).setLevel(6);
//			village.getEntity(EntityType.BALLOON, 1).setLevel(6);
//			village.getEntity(EntityType.WIZARD, 1).setLevel(6);
//			village.getEntity(EntityType.HEALER, 1).setLevel(4);
//			village.getEntity(EntityType.DRAGON, 1).setLevel(5);
//			village.getEntity(EntityType.PEKKA, 1).setLevel(5);
//			village.getEntity(EntityType.MINION, 1).setLevel(5);
//			village.getEntity(EntityType.HOG_RIDER, 1).setLevel(5);
//			village.getEntity(EntityType.VALKYRIE, 1).setLevel(1);
//			village.getEntity(EntityType.GOLEM, 1).setLevel(5);
//			village.getEntity(EntityType.WITCH, 1).setLevel(2);
//			village.getEntity(EntityType.LAVA_HOUND, 1).setLevel(3);
//			village.getEntity(EntityType.LIGHTNING_SPELL, 1).setLevel(6);
//			village.getEntity(EntityType.HEALING_SPELL, 1).setLevel(6);
//			village.getEntity(EntityType.RAGE_SPELL, 1).setLevel(5);
//			village.getEntity(EntityType.JUMP_SPELL, 1).setLevel(3);
//			village.getEntity(EntityType.FREEZE_SPELL, 1).setLevel(5);
//			village.getEntity(EntityType.POISON_SPELL, 1).setLevel(3);
//			village.getEntity(EntityType.EARTHQUAKE_SPELL, 1).setLevel(2);
//			village.getEntity(EntityType.HASTE_SPELL, 1).setLevel(2);
//
//			village.getEntity(EntityType.BARBARIAN_KING, 1).setLevel(30);
//			village.getEntity(EntityType.ARCHER_QUEEN, 1).setLevel(40);
//			
//			village.getEntity(EntityType.BOMB, 1).setLevel(6);
//			village.getEntity(EntityType.BOMB, 2).setLevel(6);
//			village.getEntity(EntityType.BOMB, 3).setLevel(6);
//			village.getEntity(EntityType.BOMB, 4).setLevel(6);
//			village.getEntity(EntityType.BOMB, 5).setLevel(6);
//			village.getEntity(EntityType.BOMB, 6).setLevel(6);
//			village.getEntity(EntityType.SPRING_TRAP, 1).setLevel(1);
//			village.getEntity(EntityType.SPRING_TRAP, 2).setLevel(1);
//			village.getEntity(EntityType.SPRING_TRAP, 3).setLevel(1);
//			village.getEntity(EntityType.SPRING_TRAP, 4).setLevel(1);
//			village.getEntity(EntityType.SPRING_TRAP, 5).setLevel(1);
//			village.getEntity(EntityType.SPRING_TRAP, 6).setLevel(1);
//			village.getEntity(EntityType.GIANT_BOMB, 1).setLevel(4);
//			village.getEntity(EntityType.GIANT_BOMB, 2).setLevel(4);
//			village.getEntity(EntityType.GIANT_BOMB, 3).setLevel(4);
//			village.getEntity(EntityType.GIANT_BOMB, 4).setLevel(4);
//			village.getEntity(EntityType.GIANT_BOMB, 5).setLevel(4);
//			village.getEntity(EntityType.AIR_BOMB, 1).setLevel(4);
//			village.getEntity(EntityType.AIR_BOMB, 2).setLevel(4);
//			village.getEntity(EntityType.AIR_BOMB, 3).setLevel(4);
//			village.getEntity(EntityType.AIR_BOMB, 4).setLevel(4);
//			village.getEntity(EntityType.AIR_BOMB, 5).setLevel(4);
//			village.getEntity(EntityType.SEEKING_AIR_BOMB, 1).setLevel(3);
//			village.getEntity(EntityType.SEEKING_AIR_BOMB, 2).setLevel(3);
//			village.getEntity(EntityType.SEEKING_AIR_BOMB, 3).setLevel(3);
//			village.getEntity(EntityType.SEEKING_AIR_BOMB, 4).setLevel(3);
//			village.getEntity(EntityType.SEEKING_AIR_BOMB, 5).setLevel(3);
//			village.getEntity(EntityType.SKELETON_TRAP, 1).setLevel(3);
//			village.getEntity(EntityType.SKELETON_TRAP, 2).setLevel(3);
//			village.getEntity(EntityType.SKELETON_TRAP, 3).setLevel(3);
		}
		return village;
	}
	
	public static void loadVillage(Context c) {
		try {
			File f = c.getFileStreamPath(ARQUIVO);
			if (f.exists()) {
				FileInputStream in = c.openFileInput(ARQUIVO);
				int tamanho = in.available();
				byte bytes[] = new byte[tamanho];
				in.read(bytes);
				String s = new String(bytes);
				
				getVillage().convertFromString(s);
			}
		} catch (FileNotFoundException e) {
		} catch (Exception e) {
		}		
	}
	public static void saveVillage(Context c) {
		try {
			FileOutputStream out = c.openFileOutput(ARQUIVO, Context.MODE_PRIVATE);
			String msg = village.convertToString();
			out.write(msg.getBytes());
			out.close();
		} catch (Exception e) {
		}		
	}

	public static String getEntityName(Context c, EntityType type) {
		Map<String, EntityType> map = getEntityTypeMap(c);
		for (Entry<String, EntityType> entry : map.entrySet()) {
	        if (type.equals(entry.getValue())) {
	            return entry.getKey();
	        }
	    }
		return "";
	}
	public static Map<String, EntityType> getEntityTypeMap(Context c) {
		Map<String, EntityType> map = new HashMap<String, EntityType>();

		map.put(c.getString(R.string.barbarian), EntityType.BARBARIAN);
		map.put(c.getString(R.string.archer), EntityType.ARCHER);
		map.put(c.getString(R.string.giant), EntityType.GIANT);
		map.put(c.getString(R.string.goblin), EntityType.GOBLIN);
		map.put(c.getString(R.string.wall_breaker), EntityType.WALL_BREAKER);
		map.put(c.getString(R.string.balloon), EntityType.BALLOON);
		map.put(c.getString(R.string.wizard), EntityType.WIZARD);
		map.put(c.getString(R.string.healer), EntityType.HEALER);
		map.put(c.getString(R.string.dragon), EntityType.DRAGON);
		map.put(c.getString(R.string.pekka), EntityType.PEKKA);
		map.put(c.getString(R.string.baby_dragon), EntityType.BABY_DRAGON);
		map.put(c.getString(R.string.miner), EntityType.MINER);
		map.put(c.getString(R.string.minion), EntityType.MINION);
		map.put(c.getString(R.string.hog_rider), EntityType.HOG_RIDER);
		map.put(c.getString(R.string.valkyrie), EntityType.VALKYRIE);
		map.put(c.getString(R.string.golem), EntityType.GOLEM);
		map.put(c.getString(R.string.witch), EntityType.WITCH);
		map.put(c.getString(R.string.lava_hound), EntityType.LAVA_HOUND);
		map.put(c.getString(R.string.bowler), EntityType.BOWLER);
		map.put(c.getString(R.string.lightning_spell), EntityType.LIGHTNING_SPELL);
		map.put(c.getString(R.string.healing_spell), EntityType.HEALING_SPELL);
		map.put(c.getString(R.string.rage_spell), EntityType.RAGE_SPELL);
		map.put(c.getString(R.string.jump_spell), EntityType.JUMP_SPELL);
		map.put(c.getString(R.string.freeze_spell), EntityType.FREEZE_SPELL);
		map.put(c.getString(R.string.clone_spell), EntityType.CLONE_SPELL);
		map.put(c.getString(R.string.poison_spell), EntityType.POISON_SPELL);
		map.put(c.getString(R.string.earthquake_spell), EntityType.EARTHQUAKE_SPELL);
		map.put(c.getString(R.string.haste_spell), EntityType.HASTE_SPELL);
		map.put(c.getString(R.string.skeleton_spell), EntityType.SKELETON_SPELL);
		map.put(c.getString(R.string.army_camp), EntityType.ARMY_CAMP);
		map.put(c.getString(R.string.town_hall), EntityType.TOWN_HALL);
		map.put(c.getString(R.string.elixir_colector), EntityType.ELIXIR_COLLECTOR);
		map.put(c.getString(R.string.elixir_storage), EntityType.ELIXIR_STORAGE);
		map.put(c.getString(R.string.gold_mine), EntityType.GOLD_MINE);
		map.put(c.getString(R.string.gold_storage), EntityType.GOLD_STORAGE);
		map.put(c.getString(R.string.barrack), EntityType.BARRACK);
		map.put(c.getString(R.string.dark_barrack), EntityType.DARK_BARRACK);
		map.put(c.getString(R.string.laboratory), EntityType.LABORATORY);
		map.put(c.getString(R.string.cannon), EntityType.CANNON);
		map.put(c.getString(R.string.archer_tower), EntityType.ARCHER_TOWER);
		map.put(c.getString(R.string.wall), EntityType.WALL);
		map.put(c.getString(R.string.wizard_tower), EntityType.WIZARD_TOWER);
		map.put(c.getString(R.string.air_defense), EntityType.AIR_DEFENSE);
		map.put(c.getString(R.string.mortar), EntityType.MORTAR);
		map.put(c.getString(R.string.clan_castle), EntityType.CLAN_CASTLE);
		map.put(c.getString(R.string.builder), EntityType.BUILDER);
		map.put(c.getString(R.string.hidden_tesla), EntityType.HIDDEN_TESLA);
		map.put(c.getString(R.string.spell_factory), EntityType.SPELL_FACTORY);
		map.put(c.getString(R.string.xbow), EntityType.XBOW);
		map.put(c.getString(R.string.barbarian_king), EntityType.BARBARIAN_KING);
		map.put(c.getString(R.string.archer_queen), EntityType.ARCHER_QUEEN);
		map.put(c.getString(R.string.grand_warden), EntityType.GRAND_WARDEN);
		map.put(c.getString(R.string.dark_elixir_drill), EntityType.DARK_ELIXIR_DRILL);
		map.put(c.getString(R.string.dark_elixir_storage), EntityType.DARK_ELIXIR_STORAGE);
		map.put(c.getString(R.string.inferno_tower), EntityType.INFERNO_TOWER);
		map.put(c.getString(R.string.air_sweeper), EntityType.AIR_SWEEPER);
		map.put(c.getString(R.string.eagle_artillery), EntityType.EAGLE_ARTILLERY);
		map.put(c.getString(R.string.dark_spell_factory), EntityType.DARK_SPELL_FACTORY);
		map.put(c.getString(R.string.bomb), EntityType.BOMB);
		map.put(c.getString(R.string.spring_trap), EntityType.SPRING_TRAP);
		map.put(c.getString(R.string.giant_bomb), EntityType.GIANT_BOMB);
		map.put(c.getString(R.string.air_bomb), EntityType.AIR_BOMB);
		map.put(c.getString(R.string.seeking_air_bomb), EntityType.SEEKING_AIR_BOMB);
		map.put(c.getString(R.string.skeleton_trap), EntityType.SKELETON_TRAP);
		
		return map;
	}


	public static Map<EntityType, String> getEntityTypeStringMap(Context c) {
		Map<EntityType, String> mapResult = new HashMap<EntityType, String>();

		Map<String, EntityType> map = getEntityTypeMap(c);
		for (Entry<String, EntityType> entry : map.entrySet()) {
			mapResult.put(entry.getValue(), entry.getKey());
	    }
		return mapResult;
	}

	public static List<String> getEntityTypesNames(EntityType[] types, Context c) {
		Map<EntityType, String> map = getEntityTypeStringMap(c);
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < types.length; i++)
			result.add(map.get(types[i]));
		
		return result;
	}
}
