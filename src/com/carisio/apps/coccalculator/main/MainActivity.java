package com.carisio.apps.coccalculator.main;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.fragments.AboutFragment;
import com.carisio.apps.coccalculator.main.fragments.BuilderFragment;
import com.carisio.apps.coccalculator.main.fragments.EffortsSpentWithMyVillage;
import com.carisio.apps.coccalculator.main.fragments.EffortsToMaximizeVilllageFragment;
import com.carisio.apps.coccalculator.main.fragments.EntitiesOfDifferentTypeFragment;
import com.carisio.apps.coccalculator.main.fragments.EntitiesOfSameTypeFragment;
import com.carisio.apps.coccalculator.main.fragments.ResourceProductionFragment;
import com.carisio.apps.coccalculator.main.fragments.RetainedFragment;
import com.carisio.apps.coccalculator.main.fragments.ShowEvolutionTableFragment;
import com.carisio.apps.coccalculator.main.fragments.TownHallFragment;
import com.carisio.apps.coccalculator.main.fragments.TrainingCostTimeFragment;
import com.carisio.apps.coccalculator.main.fragments.TrainingTableFragment;
import com.carisio.apps.coccalculator.main.fragments.WallsFragment;
import com.carisio.apps.coccalculator.utils.ViewUtils;

public class MainActivity extends Activity {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ExpandableListView drawerList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Global.loadVillage(this);
		setContentView(R.layout.main_activity);
		setTitle(getString(R.string.app_name));
		
		prepareNavigationDrawer();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
            this,                  /* host Activity */
            drawerLayout,         /* DrawerLayout object */
            R.string.drawer_open,  /* "open drawer" description for accessibility */
            R.string.drawer_close  /* "close drawer" description for accessibility */
            ) {
            public void onDrawerClosed(View view) {
//                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
//                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.gold);
        drawerLayout.setDrawerListener(mDrawerToggle);
        
        if (savedInstanceState == null) {
//            selectItem(0);
        }

        Fragment fragment = getFragmentManager().findFragmentById(R.id.content_frame);
        if (fragment == null) {
        	showFragment(getString(R.string.efforts_to_maximize_village));
        } else {
        	showFragment(fragment);
        }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.action_bar_menu, menu);
	    return true;
//		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        
//         Handle others action buttons
        switch (item.getItemId()) {
        	case R.id.action_button_calculate:
        		showCalculateOption();
        		break;
        	case R.id.action_button_table:
        		showTableOptions();
        		break;
        }
        // Default
        return super.onOptionsItemSelected(item);
    }
    private void showTableOptions() {
    	showActionButtonOptions(getString(R.string.table_section), 
				getResources().getStringArray(R.array.nav_drawer_table_options));
    }

	private void showCalculateOption() {
		showActionButtonOptions(getString(R.string.calculate_section), 
				getResources().getStringArray(R.array.nav_drawer_simulate_options));
	}
	private void showActionButtonOptions(String title, String[] options) {
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
//		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle(title);

		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
				MainActivity.this,
		        android.R.layout.select_dialog_singlechoice) {
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = super.getView(position, convertView, parent);
				((TextView)v).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
				ViewUtils.configureTextView(MainActivity.this, (TextView) v);
				return v;
			};
		};
		arrayAdapter.addAll(options);
		builderSingle.setAdapter(
		        arrayAdapter,
		        new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		                String selectedOption = arrayAdapter.getItem(which);
		                showFragment(selectedOption);
		            }
		        });
		builderSingle.setCancelable(true);
		builderSingle.show();
	}

	private void showAboutDialog() {
		Fragment f = new AboutFragment();
		showFragment(f);
	}

	private void showImportTHFull() {
    	// Close current fragment to save actual data. If it is not done before show dialog
    	// if the user import a th full, in some cases the new th may be subscribed by the actual config when 
    	// the user leaves the fragment
    	Fragment f = new RetainedFragment();
    	setTitle(getString(R.string.import_th_title));
    	showFragment(f);
    	
    	// Show dialog box
    	LayoutInflater li = LayoutInflater.from(this);
    	View view = li.inflate(R.layout.import_th_full_layout, null);
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    	alertDialogBuilder.setView(view);
    	
    	alertDialogBuilder.setTitle(getString(R.string.import_th_title));
        alertDialogBuilder.setIcon(R.drawable.ic_file_download_black_24dp);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        TextView info = (TextView) view.findViewById(R.id.import_max_th);
        ViewUtils.configureTextView(this, info);
        final Spinner spnTHLevel = (Spinner) view.findViewById(R.id.th_level);
        ViewUtils.configureIntegerSpinner(spnTHLevel, this, 1, Global.getVillage().getMaxTownHallLevel());
        spnTHLevel.setSelection(Global.getVillage().getTownHallLevel()-1);
        
        ImageView cancel = (ImageView) view.findViewById(R.id.cancel_button);
        ImageView confirm = (ImageView) view.findViewById(R.id.confirm_button);

        cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		});
        confirm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int thLevel = spnTHLevel.getSelectedItemPosition() + 1;
				Global.getVillage().buildFullVillage(thLevel);
				Global.saveVillage(MainActivity.this);
				MainActivity.this.showFragment(getString(R.string.town_hall));
				alertDialog.dismiss();
			}
		});
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
	}

	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
    	super.onPostCreate(savedInstanceState);
    	// Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
	private void showEntitiesOfDifferentTypeFragment(String entityCategory) {
		Fragment fragment = new EntitiesOfDifferentTypeFragment();
        Bundle args = new Bundle();
        args.putString(EntitiesOfDifferentTypeFragment.ENTITY_CATEGORY, entityCategory);
        fragment.setArguments(args);
        showFragment(fragment);
	}
	private void showEntitiesOfSameTypeFragment(String entityName, String categoryName) {
		Fragment f = new EntitiesOfSameTypeFragment();
        Bundle args = new Bundle();
        args.putString(EntitiesOfSameTypeFragment.ENTITY_NAME, entityName);
        args.putString(EntitiesOfSameTypeFragment.CATEGORY_NAME, categoryName);
        f.setArguments(args);
        showFragment(f);
	}
	private void showFragment(String optionMenu) {
		Fragment fragment = null;
		if (optionMenu.equals(getString(R.string.town_hall))) {
			fragment = new TownHallFragment();
		} else if (optionMenu.equals(getString(R.string.wall))) {
			fragment = new WallsFragment();
		} else if (optionMenu.equals(getString(R.string.builder))) {
			fragment = new BuilderFragment();
		} else if (optionMenu.equals(getString(R.string.efforts_spent_so_far))) {
			fragment = new EffortsSpentWithMyVillage();
		} else if (optionMenu.equals(getString(R.string.efforts_to_maximize_village))) {
			fragment = new EffortsToMaximizeVilllageFragment();
		} else if (optionMenu.equals(getString(R.string.resource_production))) {
			fragment = new ResourceProductionFragment();
		} else if (optionMenu.equals(getString(R.string.training_cost))) {
			fragment = new TrainingCostTimeFragment();
		} else if (optionMenu.equals(getString(R.string.show_evolution_table))) {
			fragment = new ShowEvolutionTableFragment();
		} else if (optionMenu.equals(getString(R.string.show_training_cost))) {
			fragment = new TrainingTableFragment();
		}
		showFragment(fragment);
	}
	public void showFragment(Fragment f) {
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, f);
		transaction.addToBackStack(null);
		transaction.commit();
		
        drawerLayout.closeDrawer(drawerList);
	}
    private void prepareNavigationDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        
        drawerList = (ExpandableListView) findViewById(R.id.left_drawer);
        drawerList.setGroupIndicator(null);
        drawerList.setAdapter(new NavigationDrawerAdapter());
        drawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
            	int groupCount = drawerList.getExpandableListAdapter().getGroupCount();
                for (int i = 0; i < groupCount; i++) {
                	if (groupPosition != i)
                		drawerList.collapseGroup(i);
                }
            }
        });
        drawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				NavigationDrawerAdapter adapter = (NavigationDrawerAdapter) drawerList.getExpandableListAdapter();
				String groupName = adapter.getGroupName(groupPosition);
				
				if (groupName.equals(getString(R.string.about_section))) {
        			showAboutDialog();
        			return true;
        		} 
				return false;
			}});
        
        drawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
        	@Override
        	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        		NavigationDrawerAdapter adapter = (NavigationDrawerAdapter) drawerList.getExpandableListAdapter();
        		String childName = adapter.getChildName(groupPosition, childPosition);
        		String groupName = adapter.getGroupName(groupPosition);
        		
        		if (groupName.equals(getString(R.string.town_hall_section)) && childName.equals(getString(R.string.import_th_option))) {
        			showImportTHFull();
        		} else if (childName.equals(getString(R.string.builder))) {
        			showFragment(new BuilderFragment());
        		} else if (childName.equals(getString(R.string.wall))){
        			showFragment(childName);
        		} else if (groupName.equals(getString(R.string.resource_section))
        				|| groupName.equals(getString(R.string.traps_section))
        				|| groupName.equals(getString(R.string.defensive_buildings_section))
        				|| groupName.equals(getString(R.string.other_buildings_section))) {
        			showEntitiesOfSameTypeFragment(childName, groupName);	
        		} else if (groupName.equals(getString(R.string.troops_section)) ||
        				groupName.equals(getString(R.string.spells_section))) {
        			showEntitiesOfDifferentTypeFragment(childName);
        		} else {
        			showFragment(childName);
        		}
        		return true;
        	}}
        );
	}

	/* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        getActionBar().setTitle(title);
    }
    
    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
        
        if (getFragmentManager().getBackStackEntryCount() == 1)
        	setTitle(getString(R.string.app_name));
        
        drawerLayout.openDrawer(GravityCompat.START);
    }
    class NavigationDrawerAdapter extends BaseExpandableListAdapter {
    	Typeface typeFace;
    	private String[] navDrawerMainOptions;
    	private String[] navDrawerTownHallSection;
    	private String[] navDrawerResourceSection;
    	private String[] navDrawerDefensiveBuildingsSection;
    	private String[] navDrawerSpellsSection;
    	private String[] navDrawerTroopsSection;
    	private String[] navDrawerOtherBuildingsSection;
    	private String[] navDrawerTrapsSection;
    	private String[] navDrawerSimulateSection;
    	private String[] navDrawerTableSection;
    	
    	private Map<Integer, String[]> groupPositionOptions;
    	
    	public NavigationDrawerAdapter() {
    		typeFace = Typeface.createFromAsset(getAssets(),"fonts/Supercell-magic-webfont.ttf");
    		
    		
    		navDrawerMainOptions = getResources().getStringArray(R.array.nav_drawer_options);
    		navDrawerTownHallSection = getResources().getStringArray(R.array.nav_drawer_town_hall_options);
    		navDrawerResourceSection = getResources().getStringArray(R.array.nav_drawer_resource_options);
    		navDrawerDefensiveBuildingsSection = getResources().getStringArray(R.array.nav_drawer_defensive_buildings_options);
        	navDrawerSpellsSection = getResources().getStringArray(R.array.nav_drawer_spell_options);
        	navDrawerTroopsSection = getResources().getStringArray(R.array.nav_drawer_troops_options);
        	navDrawerOtherBuildingsSection = getResources().getStringArray(R.array.nav_drawer_other_building_options);
        	navDrawerTrapsSection = getResources().getStringArray(R.array.nav_drawer_traps_options);
        	navDrawerSimulateSection = getResources().getStringArray(R.array.nav_drawer_simulate_options);
        	navDrawerTableSection = getResources().getStringArray(R.array.nav_drawer_table_options);
        	
        	groupPositionOptions = new HashMap<Integer, String[]>();
        	
    		for (int i = 0; i < navDrawerMainOptions.length; i++) {
    			String s = navDrawerMainOptions[i];
    			if (s.equals(getResources().getString(R.string.town_hall_section)))
    				groupPositionOptions.put(i, navDrawerTownHallSection);
    			else if (s.equals(getResources().getString(R.string.resource_section)))
    				groupPositionOptions.put(i, navDrawerResourceSection);
    			else if (s.equals(getResources().getString(R.string.defensive_buildings_section)))
    				groupPositionOptions.put(i, navDrawerDefensiveBuildingsSection);
    			else if (s.equals(getResources().getString(R.string.spells_section)))
    				groupPositionOptions.put(i, navDrawerSpellsSection);
    			else if (s.equals(getResources().getString(R.string.troops_section)))
    				groupPositionOptions.put(i, navDrawerTroopsSection);
    			else if (s.equals(getResources().getString(R.string.other_buildings_section)))
    				groupPositionOptions.put(i, navDrawerOtherBuildingsSection);
    			else if (s.equals(getResources().getString(R.string.traps_section)))
    				groupPositionOptions.put(i, navDrawerTrapsSection);
    			else if (s.equals(getResources().getString(R.string.calculate_section)))
    				groupPositionOptions.put(i, navDrawerSimulateSection);
    			else if (s.equals(getResources().getString(R.string.table_section)))
    				groupPositionOptions.put(i, navDrawerTableSection);
    		}
		}
    	
		@Override
		public int getGroupCount() {
			return navDrawerMainOptions.length;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			String[] options = groupPositionOptions.get(groupPosition);
			if (options == null)
				return 0;
			else
				return options.length;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return navDrawerMainOptions[groupPosition];
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			String[] options = groupPositionOptions.get(groupPosition);
			if (options == null)
				return null;
			else
				return options[childPosition];
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			TextView text = (TextView) getLayoutInflater().inflate(R.layout.nav_drawer_sections, null);
			text.setText(navDrawerMainOptions[groupPosition]);
			text.setTypeface(typeFace);
			return text;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			TextView view = (TextView) getLayoutInflater().inflate(R.layout.nav_drawer_final_options, null);
			view.setText(getChild(groupPosition, childPosition).toString());
			view.setTypeface(typeFace);
			return view;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
		public String getGroupName(int groupPosition) {
			return navDrawerMainOptions[groupPosition];
		}
		
		public String getChildName(int groupPosition, int childPosition) {
			return groupPositionOptions.get(groupPosition)[childPosition];
		}

    }
}
