package com.carisio.apps.coccalculator.main.fragments;

import com.carisio.apps.coccalculator.utils.ViewUtils;

import com.carisio.apps.calculatorforclash.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutFragment extends RetainedFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.about_layout, container, false);
    	getActivity().setTitle(getActivity().getString(R.string.about_section));

    	TextView developedBy = (TextView) view.findViewById(R.id.developed_by);
        ViewUtils.configureTextView(getActivity(), developedBy);
        TextView homePage = (TextView) view.findViewById(R.id.home_page);
        ViewUtils.configureTextView(getActivity(), homePage);
        TextView disclaimer = (TextView) view.findViewById(R.id.disclaimer);
        ViewUtils.configureTextView(getActivity(), disclaimer);

    	return view;
	}
}
