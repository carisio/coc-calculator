package com.carisio.apps.coccalculator.main.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.main.MainActivity;
import com.carisio.apps.coccalculator.utils.Formatters;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.CoCTime;
import com.carisio.apps.coccalculatorengine.Resource;

public class EffortsSpentWithMyVillage extends RetainedFragment {
	private TextView txtGoldValueConstructions;
	private TextView txtElixirValueConstructions;
	private TextView txtDarkElixirValueConstructions;
	private TextView txtGoldValueWalls;
	private TextView txtGoldElixirValueWalls;
	private TextView txtTimeUpgConValue;
	private TextView txtTimeUpgTroopsValue;

	
	@Override
	public void onResume() {
		super.onResume();
	
        updateSimulationInfo();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.efforts_spent_with_village_layout, container, false);
//    	costOfEachEntityContainer = (LinearLayout)view.findViewById(R.id.container_table);
    	
    	getActivity().setTitle(getActivity().getString(R.string.efforts_spent_with_village));
    	
    	TextView txtEffortsSpentWithMyVillage = (TextView) view.findViewById(R.id.efforts_spent_with_village);
    	TextView txtCostUpgradeConstructions = (TextView) view.findViewById(R.id.resources_upgrading_village);
    	txtGoldValueConstructions = (TextView) view.findViewById(R.id.gold_value_constructions);
    	txtElixirValueConstructions = (TextView) view.findViewById(R.id.elixir_value_constructions);
    	txtDarkElixirValueConstructions = (TextView) view.findViewById(R.id.dark_elixir_value_constructions);
    	TextView txtCostUpgradeWalls = (TextView) view.findViewById(R.id.resources_upgrading_walls);
    	txtGoldValueWalls = (TextView) view.findViewById(R.id.gold_value_walls);
    	txtGoldElixirValueWalls = (TextView) view.findViewById(R.id.gold_elixir_value_walls);
    	TextView txtTime = (TextView) view.findViewById(R.id.time);
    	TextView txtTimeUpgConLabel = (TextView) view.findViewById(R.id.time_upgrading_buildings_label);
    	txtTimeUpgConValue = (TextView) view.findViewById(R.id.time_upgrading_buildings_value);
    	TextView txtTimeUpgTroopsLabel = (TextView) view.findViewById(R.id.time_upgrading_troops_label);
    	txtTimeUpgTroopsValue = (TextView) view.findViewById(R.id.time_upgrading_troops_and_spells_value);

    	ViewUtils.configureTextView(getActivity(), txtEffortsSpentWithMyVillage);
    	ViewUtils.configureTextView(getActivity(), txtCostUpgradeConstructions);
    	ViewUtils.configureTextView(getActivity(), txtGoldValueConstructions);
    	ViewUtils.configureTextView(getActivity(), txtElixirValueConstructions);
    	ViewUtils.configureTextView(getActivity(), txtDarkElixirValueConstructions);
    	ViewUtils.configureTextView(getActivity(), txtCostUpgradeWalls);
    	ViewUtils.configureTextView(getActivity(), txtGoldValueWalls);
    	ViewUtils.configureTextView(getActivity(), txtGoldElixirValueWalls);
    	ViewUtils.configureTextView(getActivity(), txtTime);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgConLabel);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgConValue);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgTroopsLabel);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgTroopsValue);
    	
        ImageView openCostDetails = (ImageView) view.findViewById(R.id.open_cost_upgrade_village_details);
        openCostDetails.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment f = new DetailedInformationFragment();
		        Bundle args = new Bundle();
		        args.putBoolean(DetailedInformationFragment.SHOW_COST_DETAILS, true);
		        args.putBoolean(DetailedInformationFragment.SHOW_EFFORTS_SPENT_SO_FAR, true);
		        f.setArguments(args);
				((MainActivity)getActivity()).showFragment(f);
			}
		});
        
        
        ImageView openTimeDetails = (ImageView) view.findViewById(R.id.open_time_upgrade_village_details);
        openTimeDetails.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment f = new DetailedInformationFragment();
		        Bundle args = new Bundle();
		        args.putBoolean(DetailedInformationFragment.SHOW_TIME_DETAILS, true);
		        args.putBoolean(DetailedInformationFragment.SHOW_EFFORTS_SPENT_SO_FAR, true);
		        f.setArguments(args);
				((MainActivity)getActivity()).showFragment(f);
			}
		});

    	return view;
    }
	
	private void updateSimulationInfo() {
		String dSymbol = getActivity().getString(R.string.day_letter); 
		String hSymbol = getActivity().getString(R.string.hour_letter);
		String mSymbol = getActivity().getString(R.string.minute_letter);
		String sSymbol = getActivity().getString(R.string.second_letter);
		String emptyString = getActivity().getString(R.string.zero);
		
		CoCTime timeToConstructions = Global.getVillage().getTimeSpentWithBuildingsUntilHere();
		CoCTime timeToTroops = Global.getVillage().getTimeSpentWithTroopsAndSpellsUntilHere();
		Resource resource = Global.getVillage().getCostSpentUntilHere();
		Resource resourceWalls = Global.getVillage().getCostSpentWithWallsUntilHere();
		
		txtGoldValueConstructions.setText(Formatters.format(resource.getGold()));
		txtElixirValueConstructions.setText(Formatters.format(resource.getElixir()));
		txtDarkElixirValueConstructions.setText(Formatters.format(resource.getDarkElixir()));
		
		txtGoldValueWalls.setText(Formatters.format(resourceWalls.getGold()));
		txtGoldElixirValueWalls.setText(Formatters.format(resourceWalls.getElixir()));
		
		txtTimeUpgTroopsValue.setText(Formatters.format(timeToTroops, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
		
		txtTimeUpgConValue.setText(Formatters.format(timeToConstructions, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
		
		txtGoldValueConstructions.setGravity(Gravity.END);
		txtElixirValueConstructions.setGravity(Gravity.END);
		txtDarkElixirValueConstructions.setGravity(Gravity.END);
		
		txtGoldValueWalls.setGravity(Gravity.END);
		txtGoldElixirValueWalls.setGravity(Gravity.END);
		
		txtTimeUpgTroopsValue.setGravity(Gravity.END);
		txtTimeUpgConValue.setGravity(Gravity.END);
	}	
}
