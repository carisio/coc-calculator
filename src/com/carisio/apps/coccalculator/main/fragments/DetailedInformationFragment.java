
package com.carisio.apps.coccalculator.main.fragments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;
import java.util.Map;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.carisio.apps.calculatorforclash.R;

import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.Formatters;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.CoCTime;
import com.carisio.apps.coccalculatorengine.Entity;
import com.carisio.apps.coccalculatorengine.EntityType;
import com.carisio.apps.coccalculatorengine.Resource;

public class DetailedInformationFragment extends RetainedFragment {
	public static final String DEFAULT_TH_LEVEL = "DEFAULT_TH_LEVEL";
	
	public static final String SHOW_COST_DETAILS = "SHOW_COST_DETAILS";
	public static final String SHOW_TIME_DETAILS = "SHOW_TIME_DETAILS";
	
	public static final String SHOW_MAXIMIZE_VILLAGE_FROM_TH_LEVEL = "SHOW_MAXIMIZE_VILLAGE_FROM_TH_LEVEL";
	public static final String SHOW_EFFORTS_SPENT_SO_FAR = "SHOW_EFFORTS_SPENT_SO_FAR";

	private boolean showCostVillage;
	private boolean showTimeVillage;
		
	private boolean showMaximizeVillage;
	private boolean showEffortsSpentSoFar;
	
	private static int savedTHLevel = -1;
	
	private Spinner spnTHLevel;
	private ExpandableListView table;
	
//	private List<View> rowsToClean;

	@Override
	public void onResume() {
		super.onResume();
		if (showMaximizeVillage) {
			if (savedTHLevel < 0) {
				savedTHLevel = Global.getVillage().getTownHallLevel();
			}
	        int position = ((ArrayAdapter<String>)spnTHLevel.getAdapter()).getPosition("" + savedTHLevel);
	        spnTHLevel.setSelection(position);
		}
		
        updateListTree();
//		getView().requestLayout();
	}
	@Override
	public void onPause() {
		super.onPause();
		if (showMaximizeVillage) {
			savedTHLevel = getSelectedTHLevel();
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getActivity().setTitle(getActivity().getString(R.string.detailed_information));
		
		loadParameters();
		
    	View view = loadMaxVillageOrEffortsSpentSoFarLayout(inflater, container);
    	configureDetailedInfoTextView(view);
    	configureBasicPropertiesOfExpandableListView(view);

    	return view;
	}
	private void loadParameters() {
		showCostVillage = getArguments().getBoolean(SHOW_COST_DETAILS);
		showTimeVillage = getArguments().getBoolean(SHOW_TIME_DETAILS);
		
		showMaximizeVillage = getArguments().getBoolean(SHOW_MAXIMIZE_VILLAGE_FROM_TH_LEVEL);
		showEffortsSpentSoFar = getArguments().getBoolean(SHOW_EFFORTS_SPENT_SO_FAR);
		
		savedTHLevel = getArguments().getInt(DEFAULT_TH_LEVEL);
	}
	private View loadMaxVillageOrEffortsSpentSoFarLayout(
			LayoutInflater inflater, ViewGroup container) {
		View view;
		if (showMaximizeVillage) {
			view = inflater.inflate(R.layout.detailed_information_maximized_village_layout, container, false);
			configureHeader(view);
		} else {
			view = inflater.inflate(R.layout.detailed_information_efforts_spent_so_far_layout, container, false);
			// There is no header
		}
		return view;
	}
	private void configureHeader(View view) {
    	TextView txtMaximizeToTH = (TextView) view.findViewById(R.id.maximize_to_th);
    	spnTHLevel = (Spinner) view.findViewById(R.id.th_level);
    	
    	ViewUtils.configureIntegerSpinner(spnTHLevel, getActivity(), 1, Global.getVillage().getMaxTownHallLevel());
    	ViewUtils.configureTextView(getActivity(), txtMaximizeToTH);
    	
        spnTHLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		updateListTree();
        	}
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
	}
	private void configureDetailedInfoTextView(View view) {
		TextView detailedInfo = (TextView)view.findViewById(R.id.detailed_info);
    	ViewUtils.configureTextView(getActivity(), detailedInfo);
    	
    	if (showCostVillage && showMaximizeVillage)
    		detailedInfo.setText(getString(R.string.cost_to_upgrade_village));
    	else if (showTimeVillage && showMaximizeVillage) 
    		detailedInfo.setText(getString(R.string.time_to_upgrade));
    	else if (showCostVillage && showEffortsSpentSoFar)
    		detailedInfo.setText(getString(R.string.resource_spent_so_far));
    	else if (showTimeVillage && showEffortsSpentSoFar) 
    		detailedInfo.setText(getString(R.string.time_spent_so_far));
	}
	private void configureBasicPropertiesOfExpandableListView(View view) {
		table = (ExpandableListView) view.findViewById(R.id.table_individual_cost);
        table.setGroupIndicator(null);
        table.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
            	int groupCount = table.getExpandableListAdapter().getGroupCount();
                for (int i = 0; i < groupCount; i++) {
                	if (groupPosition != i)
                		table.collapseGroup(i);
                }
            }
        });
	}

	private int getSelectedTHLevel() {
		return spnTHLevel.getSelectedItemPosition() + 1;
	}

	private void updateListTree() {
		if (showMaximizeVillage && showCostVillage) {
			updateListTreeWithCostToUpgradeVillage();
		} else if (showMaximizeVillage && showTimeVillage) {
			updateListTreeWithTimeToUpgradeVillage();
		} else if (showEffortsSpentSoFar && showCostVillage) {
			updateListTreeWithCostSpentSoFar();
		} else if (showEffortsSpentSoFar && showTimeVillage) {
			updateListTreeWithTimeSpentSoFar();
		}
	}

	private void updateListTreeWithTimeSpentSoFar() {
		String dSymbol = getActivity().getString(R.string.day_letter); 
		String hSymbol = getActivity().getString(R.string.hour_letter);
		String mSymbol = getActivity().getString(R.string.minute_letter);
		String sSymbol = getActivity().getString(R.string.second_letter);
		String emptyString = getActivity().getString(R.string.zero);
		
		List<EntityType> allTypes = EntityType.getAllExcept(new EntityType[]{EntityType.BUILDER, EntityType.WALL});
		
		List<String> groupLabel = new ArrayList<String>();
		List<String> groupInfo = new ArrayList<String>();
		List<Integer> groupPercentage = new ArrayList<Integer>();
		List<List<String>> childInfo = new ArrayList<List<String>>();
		List<List<Integer>> childPercentage = new ArrayList<List<Integer>>();
		
		Map<EntityType, String> mapName = Global.getEntityTypeStringMap(getActivity());
		int maxTHLevel = Global.getVillage().getMaxTownHallLevel();
		for (int i = 0; i < allTypes.size(); i++) {
			EntityType type = allTypes.get(i);
			
			int thLevel = Global.getVillage().getTownHallLevel();
			int maxNumberEntitiesAtThisLevel = type.getMaximumNumberOfEntityByTH(thLevel);
			int maxNumberEntitiesAtMaxTHLevel = type.getMaximumNumberOfEntityByTH(maxTHLevel);
			List<String> individualTimeList = new ArrayList<String>();
			List<Integer> tempIndividualPercentageList = new ArrayList<Integer>();
			List<Entity> entitiesOfGivenType = Global.getVillage().getNEntities(type, maxNumberEntitiesAtThisLevel);
			CoCTime accTime = new CoCTime(0, 0, 0, 0);
			int accPercentage = 0;
			int maxEntityLvlAtMaxTH = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(Global.getVillage().getMaxTownHallLevel());
			
			for (int j = 0; j < maxNumberEntitiesAtThisLevel; j++) {
				Entity entity = entitiesOfGivenType.get(j);
				CoCTime individualTime = entity.getAccumulatedTimeToUpgradeTo(0, entity.getLevel());

				String formattedIndTime = Formatters.format(individualTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString);
				individualTimeList.add(formattedIndTime);
				accTime = accTime.sum(individualTime);
				accPercentage += entity.getPercentageTimeCompleted(maxEntityLvlAtMaxTH);
				tempIndividualPercentageList.add(entity.getPercentageTimeCompleted(maxEntityLvlAtMaxTH));
			}
			
			if (accTime.isZero()) {
				continue;
			}
			
			String formattedAccTime = Formatters.format(accTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString);
			groupLabel.add(mapName.get(type));
			groupInfo.add(formattedAccTime);
			groupPercentage.add(accPercentage/maxNumberEntitiesAtMaxTHLevel);
			childInfo.add(individualTimeList);
			childPercentage.add(tempIndividualPercentageList);
		}

		table.setAdapter(new CostAndTimeAdapter(groupLabel, groupInfo, null, groupPercentage, childInfo, childPercentage));
		
		table.invalidate();
	}
	private void updateListTreeWithCostSpentSoFar() {
		List<EntityType> allTypes = EntityType.getAllExcept(new EntityType[]{EntityType.BUILDER, EntityType.WALL});
		
		List<String> groupLabel = new ArrayList<String>();
		List<String> groupInfo = new ArrayList<String>();
		List<Integer> groupPercentage = new ArrayList<Integer>();
		List<Integer> imgId = new ArrayList<Integer>();
		int[] imgTemp = new int[1];
		List<List<String>> childInfo = new ArrayList<List<String>>();
		List<List<Integer>> childPercentage = new ArrayList<List<Integer>>();
		
		Map<EntityType, String> mapName = Global.getEntityTypeStringMap(getActivity());
		
		int maxTHLevel = Global.getVillage().getMaxTownHallLevel();
		for (int i = 0; i < allTypes.size(); i++) {
			EntityType type = allTypes.get(i);
			
			int maxNumberEntitiesAtCurrentTHLevel = type.getMaximumNumberOfEntityByTH(Global.getVillage().getTownHallLevel());
			int maxNumberEntitiesAtMaxTHLevel = type.getMaximumNumberOfEntityByTH(Global.getVillage().getMaxTownHallLevel());
			List<String> individualCostList = new ArrayList<String>();
			List<Integer> tempIndividualPercentageList = new ArrayList<Integer>();
			List<Entity> entitiesOfGivenType = Global.getVillage().getNEntities(type, maxNumberEntitiesAtCurrentTHLevel);
			Resource accCost = new Resource(0, 0, 0);
			int accPercentage = 0;
			int maxEntityLvlAtMaxTH = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(Global.getVillage().getMaxTownHallLevel());
			
			for (int j = 0; j < maxNumberEntitiesAtCurrentTHLevel; j++) {
				Entity entity = entitiesOfGivenType.get(j);
				int entityLvl = entity.getLevel();
				Resource individualCost = entity.getAccumulatedResourceToUpgradeTo(0, entityLvl);

				accCost = accCost.sum(individualCost);
				accPercentage += entity.getPercentageResourcesCompleted(maxEntityLvlAtMaxTH);
				String formattedIndCost = getFormattedResourceAndFormatImage(imgTemp, individualCost);
				individualCostList.add(formattedIndCost);
				tempIndividualPercentageList.add(entity.getPercentageResourcesCompleted(maxEntityLvlAtMaxTH));
			}
			if (accCost.isZero()) {
				continue;
			}
			String formattedAccCost = getFormattedResourceAndFormatImage(imgTemp, accCost);
			groupLabel.add(mapName.get(type));
			groupInfo.add(formattedAccCost);
			groupPercentage.add(accPercentage/maxNumberEntitiesAtMaxTHLevel);
			imgId.add(imgTemp[0]);
			childInfo.add(individualCostList);
			childPercentage.add(tempIndividualPercentageList);
		}

		table.setAdapter(new CostAndTimeAdapter(groupLabel, groupInfo, imgId, groupPercentage, childInfo, childPercentage));
		
		table.invalidate();
	}

	
	private void updateListTreeWithCostToUpgradeVillage() {
		int selectedTHLevel = getSelectedTHLevel();
		
		List<EntityType> allTypes = EntityType.getAllExcept(new EntityType[]{EntityType.BUILDER, EntityType.WALL});
		
		List<String> groupLabel = new ArrayList<String>();
		List<String> groupValue = new ArrayList<String>();
		List<Integer> groupPercentage = new ArrayList<Integer>();
		List<Integer> groupImgId = new ArrayList<Integer>();
		int[] imgTemp = new int[1];
		List<List<String>> childValue = new ArrayList<List<String>>();
		List<List<Integer>> childPercentage = new ArrayList<List<Integer>>();
		
		Map<EntityType, String> mapName = Global.getEntityTypeStringMap(getActivity());
		for (int i = 0; i < allTypes.size(); i++) {
			EntityType type = allTypes.get(i);
			
			int maxNumberEntitiesAtSelectedTHLevel = type.getMaximumNumberOfEntityByTH(selectedTHLevel);
			List<String> tempIndividualCostList = new ArrayList<String>();
			List<Integer> tempIndividualPercentageList = new ArrayList<Integer>();
			List<Entity> entitiesOfGivenType = Global.getVillage().getNEntities(type, maxNumberEntitiesAtSelectedTHLevel);
			Resource accCost = new Resource(0, 0, 0);
			int accPercentage = 0;
			int maxEntityLvlAtSelectedTH = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(selectedTHLevel);
			
			for (int j = 0; j < maxNumberEntitiesAtSelectedTHLevel; j++) {
				Entity entity = entitiesOfGivenType.get(j);
				Resource individualCost = entity.getAccumulatedResourceToUpgradeTo(maxEntityLvlAtSelectedTH);
				
				accCost = accCost.sum(individualCost);
				accPercentage += entity.getPercentageResourcesCompleted(maxEntityLvlAtSelectedTH);
				String formattedIndCost = getFormattedResourceAndFormatImage(imgTemp, individualCost);
				tempIndividualCostList.add(formattedIndCost);
				tempIndividualPercentageList.add(entity.getPercentageResourcesCompleted(maxEntityLvlAtSelectedTH));
			}
			
			if (accCost.isZero()) {
				continue;
			}
			
			String formattedAccCost = getFormattedResourceAndFormatImage(imgTemp, accCost);
			groupLabel.add(mapName.get(type));
			groupValue.add(formattedAccCost);
			groupImgId.add(imgTemp[0]);
			groupPercentage.add(accPercentage/maxNumberEntitiesAtSelectedTHLevel);
			childValue.add(tempIndividualCostList);
			childPercentage.add(tempIndividualPercentageList);
		}

		table.setAdapter(new CostAndTimeAdapter(groupLabel, groupValue, groupImgId, groupPercentage, childValue, childPercentage));
		
		table.invalidate();
	}

	private void updateListTreeWithTimeToUpgradeVillage() {
		int selectedTHLevel = getSelectedTHLevel();
		
		String dSymbol = getActivity().getString(R.string.day_letter); 
		String hSymbol = getActivity().getString(R.string.hour_letter);
		String mSymbol = getActivity().getString(R.string.minute_letter);
		String sSymbol = getActivity().getString(R.string.second_letter);
		String emptyString = getActivity().getString(R.string.all_updated);
		
		List<EntityType> allTypes = EntityType.getAllExcept(new EntityType[]{EntityType.BUILDER, EntityType.WALL});
		
		List<String> groupLabel = new ArrayList<String>();
		List<String> groupInfo = new ArrayList<String>();
		List<Integer> groupPercentage = new ArrayList<Integer>();
		List<List<String>> childInfo = new ArrayList<List<String>>();
		List<List<Integer>> childPercentage = new ArrayList<List<Integer>>();
		
		Map<EntityType, String> mapName = Global.getEntityTypeStringMap(getActivity());
		for (int i = 0; i < allTypes.size(); i++) {
			EntityType type = allTypes.get(i);

			int maxNumberEntitiesAtSelectedTHLevel = type.getMaximumNumberOfEntityByTH(selectedTHLevel);
			List<String> individualTimeList = new ArrayList<String>();
			List<Integer> tempIndividualPercentageList = new ArrayList<Integer>();
			List<Entity> entitiesOfGivenType = Global.getVillage().getNEntities(type, maxNumberEntitiesAtSelectedTHLevel);
			CoCTime accTime = new CoCTime(0, 0, 0, 0);
			int accPercentage = 0;
			int maxEntityLvlAtSelectedTH = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(selectedTHLevel);
			
			for (int j = 0; j < maxNumberEntitiesAtSelectedTHLevel; j++) {
				Entity entity = entitiesOfGivenType.get(j);
				int maxLvlAtSelectedTH = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(selectedTHLevel);
				CoCTime individualTime = entity.getAccumulatedTimeToUpgradeTo(maxLvlAtSelectedTH);

				accTime = accTime.sum(individualTime);
				accPercentage += entity.getPercentageTimeCompleted(maxEntityLvlAtSelectedTH);
				String formattedIndTime = Formatters.format(individualTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString);
				individualTimeList.add(formattedIndTime);
				tempIndividualPercentageList.add(entity.getPercentageTimeCompleted(maxEntityLvlAtSelectedTH));
			}
			
			if (accTime.isZero()) {
				continue;
			}
			
			String formattedAccTime = Formatters.format(accTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString);
			groupLabel.add(mapName.get(type));
			groupInfo.add(formattedAccTime);
			groupPercentage.add(accPercentage/maxNumberEntitiesAtSelectedTHLevel);
			childInfo.add(individualTimeList);
			childPercentage.add(tempIndividualPercentageList);
		}

		table.setAdapter(new CostAndTimeAdapter(groupLabel, groupInfo, null, groupPercentage, childInfo, childPercentage));
		
		table.invalidate();
	}
	
	private String getFormattedResourceAndFormatImage(int[] img,
			Resource resource) {
		String formatted_cost = getActivity().getString(R.string.zero);
		if (resource.getGold() > 0) {
			formatted_cost = Formatters.format(resource.getGold());
			img[0] = R.drawable.gold;
		}
		if (resource.getElixir() > 0) {
			formatted_cost = Formatters.format(resource.getElixir());
			img[0] = R.drawable.elixir;
		}
		if (resource.getDarkElixir() > 0) {
			formatted_cost = Formatters.format(resource.getDarkElixir());
			img[0] = R.drawable.dark_elixir;
		}
		return formatted_cost;
	}	

	class ArrayIdxComparator implements Comparator<Integer> {
		private String[] array;

		public ArrayIdxComparator(List<String> l) {
			array = new String[l.size()];
			for (int i = 0; i < l.size(); i++)
				array[i] = l.get(i);
		}
		public Integer[] createIndexArray() {
	        Integer[] indexes = new Integer[array.length];
	        for (int i = 0; i < array.length; i++) {
	            indexes[i] = i; 
	        }
	        return indexes;
	    }
		@Override
		public int compare(Integer idx1, Integer idx2) {
			return array[idx1].compareTo(array[idx2]);
		}
		
	}
    class CostAndTimeAdapter extends BaseExpandableListAdapter {
    	
    	/* Group info */
    	private String[] groupLabel;
    	private String[] groupValue;
    	private int[] groupImgID;
    	private int[] groupPercentage;
    	
    	/* Child info */
    	private String[][] childValue;
    	private int[][] childPercentage;
    	
    	public CostAndTimeAdapter(List<String> groupLabel, List<String> groupValue, List<Integer> groupImgID, List<Integer> groupPercentage, List<List<String>> childValue, List<List<Integer>> childPercentage) {
    		this.groupLabel = new String[groupLabel.size()];
    		this.groupValue = new String[groupValue.size()];
    		if (groupImgID != null)
    			this.groupImgID = new int[groupImgID.size()];
    		this.groupPercentage = new int[groupPercentage.size()];
    		this.childValue = new String[childValue.size()][];
    		this.childPercentage = new int[childPercentage.size()][];
    		
    		/* Sort */
    		ArrayIdxComparator comparator = new ArrayIdxComparator(groupLabel);
    		Integer[] idx = comparator.createIndexArray();
    		Arrays.sort(idx, comparator);
    		
    		for (int i = 0; i < idx.length; i++) {
    			this.groupLabel[i] = groupLabel.get(idx[i]);
    			this.groupValue[i] = groupValue.get(idx[i]);
    			if (groupImgID != null)
    				this.groupImgID[i] = groupImgID.get(idx[i]);
    			this.groupPercentage[i] = groupPercentage.get(idx[i]);
    			
    			this.childValue[i] = new String[childValue.get(idx[i]).size()];
    			for (int j = 0; j < childValue.get(idx[i]).size(); j++)
    				this.childValue[i][j] = childValue.get(idx[i]).get(j);
    			this.childPercentage[i] = new int[childPercentage.get(idx[i]).size()];
    			for (int j = 0; j < childPercentage.get(idx[i]).size(); j++)
    				this.childPercentage[i][j] = childPercentage.get(idx[i]).get(j);
    		}
		}
		@Override
		public int getGroupCount() {
			return groupLabel.length;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			if (childValue[groupPosition].length == 1)
				return 0;
			return childValue[groupPosition].length;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return groupLabel[groupPosition];
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return childValue[groupPosition][childPosition];
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {

			LinearLayout linearLayout;
			if (groupImgID == null) {
				linearLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_detailed_information_text_text_dark_background, null);
			} else {
				linearLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_detailed_information_text_text_icon_dark_background, null);
				ImageView img = (ImageView) linearLayout.findViewById(R.id.img_for_entity_info);
				img.setImageResource(groupImgID[groupPosition]);
				
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)linearLayout.findViewById(R.id.entity_info).getLayoutParams();
				params.addRule(RelativeLayout.LEFT_OF, R.id.img_for_entity_info);
				linearLayout.findViewById(R.id.entity_info).setLayoutParams(params); //causes layout update
			}
			
			ImageView groupIndicator = (ImageView) linearLayout.findViewById(R.id.img_group_indicator);
			if (getChildrenCount(groupPosition) > 0) {
				if (isExpanded)
					groupIndicator.setImageResource(R.drawable.ic_keyboard_arrow_up_black_18dp);
				else
					groupIndicator.setImageResource(R.drawable.ic_keyboard_arrow_down_black_18dp);
			}
				
			TextView entity = (TextView)linearLayout.findViewById(R.id.entity_label);
			entity.setText(groupLabel[groupPosition]);
			ViewUtils.configureTextView(getActivity(), entity);
			
			TextView entityInfo = (TextView) linearLayout.findViewById(R.id.entity_info);
			entityInfo.setText(groupValue[groupPosition]);
			ViewUtils.configureTextView(getActivity(), entityInfo);

			ProgressBar progress = (ProgressBar)linearLayout.findViewById(R.id.progressBar);
			progress.setProgress(groupPercentage[groupPosition]);
			return linearLayout;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			LinearLayout linearLayout;
			
			if (groupImgID == null) {
				linearLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_detailed_information_text_text_light_background, null);
			} else {
				linearLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_detailed_information_text_text_icon_light_background, null);
				ImageView img = (ImageView) linearLayout.findViewById(R.id.img_for_entity_info);
				img.setImageResource(groupImgID[groupPosition]);
				
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)linearLayout.findViewById(R.id.entity_info).getLayoutParams();
				params.addRule(RelativeLayout.LEFT_OF, R.id.img_for_entity_info);
				linearLayout.findViewById(R.id.entity_info).setLayoutParams(params); //causes layout update
			}
			TextView entity = (TextView)linearLayout.findViewById(R.id.entity_label);
			entity.setText("#" + (childPosition + 1));
			ViewUtils.configureTextView(getActivity(), entity);
			
			TextView entityInfo = (TextView) linearLayout.findViewById(R.id.entity_info);
			entityInfo.setText(childValue[groupPosition][childPosition]);
			ViewUtils.configureTextView(getActivity(), entityInfo);
			
			ProgressBar progress = (ProgressBar)linearLayout.findViewById(R.id.progressBar);
			progress.setProgress(childPercentage[groupPosition][childPosition]);
			
			return linearLayout;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
    }
}