package com.carisio.apps.coccalculator.main.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.Formatters;
import com.carisio.apps.coccalculator.utils.ImageUtils;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.CoCTime;
import com.carisio.apps.coccalculatorengine.Entity;
import com.carisio.apps.coccalculatorengine.EntityType;
import com.carisio.apps.coccalculatorengine.Resource;
import com.carisio.apps.coccalculatorengine.TroopsAndSpellsTrainingTable;

import de.hdodenhof.circleimageview.CircleImageView;

public class TrainingCostTimeFragment extends RetainedFragment {
	private Map<EntityType, TroopsAndSpellsTrainingTable> troopsTrainingMap;
	private List<Spinner> elixirTroopSpinners;
	private List<Entity> elixirTroops;
	private List<Spinner> darkElixirTroopSpinners;
	private List<Entity> darkElixirTroops;

	private List<Spinner> elixirSpellsSpinners;
	private List<Entity> elixirSpells;
	private List<Spinner> darkElixirSpellsSpinners;
	private List<Entity> darkElixirSpells;

	private static boolean saved = false; 
	private static List<Integer> savedDataElixirTroopSpinners;
	private static List<Integer> savedDataDarkElixirTroopSpinners;
	private static List<Integer> savedDataElixirSpellsSpinners;
	private static List<Integer> savedDataDarkElixirSpellsSpinners;
	
	TextView elixirCost;
	TextView darkElixirCost;
	TextView totalTimeTrainTroops;
	TextView elixirCostToBrew;
	TextView darkElixirCostToBrew;
	TextView totalTimeBrewSpells;
	
	private TextView armyCampOccupation;
	private TextView spellsOccupation;
	
	private int availableSpace;
	private int availableSpellsSpace;
	
	@Override
	public void onPause() {
		super.onPause();
		
		saved = true;
		savedDataElixirTroopSpinners = new ArrayList<Integer>();
		savedDataDarkElixirTroopSpinners = new ArrayList<Integer>();
		savedDataElixirSpellsSpinners = new ArrayList<Integer>();
		savedDataDarkElixirSpellsSpinners = new ArrayList<Integer>();
		for (int i = 0; i < elixirTroopSpinners.size(); i++)
			savedDataElixirTroopSpinners.add(elixirTroopSpinners.get(i).getSelectedItemPosition());
		for (int i = 0; i < darkElixirTroopSpinners.size(); i++)
			savedDataDarkElixirTroopSpinners.add(darkElixirTroopSpinners.get(i).getSelectedItemPosition());
		for (int i = 0; i < elixirSpellsSpinners.size(); i++)
			savedDataElixirSpellsSpinners.add(elixirSpellsSpinners.get(i).getSelectedItemPosition());
		for (int i = 0; i < darkElixirSpellsSpinners.size(); i++)
			savedDataDarkElixirSpellsSpinners.add(darkElixirSpellsSpinners.get(i).getSelectedItemPosition());
	}
	@Override
	public void onResume() {
		super.onResume();
//		for (Spinner s : elixirTroopSpinners)
//			s.setSelection(0);
//		
//		for (Spinner s : darkElixirTroopSpinners)
//			s.setSelection(0);
		if (saved) {
			for (int i = 0; i < savedDataElixirTroopSpinners.size(); i++)
				elixirTroopSpinners.get(i).setSelection(savedDataElixirTroopSpinners.get(i));
			for (int i = 0; i < savedDataDarkElixirTroopSpinners.size(); i++)
				darkElixirTroopSpinners.get(i).setSelection(savedDataDarkElixirTroopSpinners.get(i));
			for (int i = 0; i < savedDataElixirSpellsSpinners.size(); i++)
				elixirSpellsSpinners.get(i).setSelection(savedDataElixirSpellsSpinners.get(i));
			for (int i = 0; i < savedDataDarkElixirSpellsSpinners.size(); i++)
				darkElixirSpellsSpinners.get(i).setSelection(savedDataDarkElixirSpellsSpinners.get(i));
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		initiateVariables();
		
    	View view = inflater.inflate(R.layout.training_cost_time_layout, container, false);
    	getActivity().setTitle(getActivity().getString(R.string.training_cost));
    	
    	// Army and spells camp ocupation
    	armyCampOccupation = (TextView) view.findViewById(R.id.army_camp_occupation);
    	ViewUtils.configureTextView(getActivity(), armyCampOccupation);
    	spellsOccupation = (TextView) view.findViewById(R.id.spells_occupation);
    	ViewUtils.configureTextView(getActivity(), spellsOccupation);
    	
    	// Configure table title
    	TextView elixirTroopsLabel = (TextView)view.findViewById(R.id.textview_elixir_troops_title);
    	TextView spinnerElixirTroopsLabel = (TextView)view.findViewById(R.id.spinner_elixir_troops_title);
    	elixirTroopsLabel.setText(getActivity().getText(R.string.elixir_troops));
    	spinnerElixirTroopsLabel.setText(getActivity().getText(R.string.quantity_short));
    	ViewUtils.configureTextView(getActivity(), elixirTroopsLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerElixirTroopsLabel);
    	
    	TextView darkElixirTroopsLabel = (TextView)view.findViewById(R.id.textview_dark_elixir_troops_title);
    	TextView spinnerDarkElixirTroopsLabel = (TextView)view.findViewById(R.id.spinner_dark_elixir_troops_title);
    	darkElixirTroopsLabel.setText(getActivity().getText(R.string.dark_elixir_troops));
    	spinnerDarkElixirTroopsLabel.setText(getActivity().getText(R.string.quantity_short));
    	ViewUtils.configureTextView(getActivity(), darkElixirTroopsLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerDarkElixirTroopsLabel);

    	TextView elixirSpellsLabel = (TextView)view.findViewById(R.id.textview_elixir_spells_title);
    	TextView spinnerElixirSpellsLabel = (TextView)view.findViewById(R.id.spinner_elixir_spells_title);
    	elixirSpellsLabel.setText(getActivity().getText(R.string.elixir_spells));
    	spinnerElixirSpellsLabel.setText(getActivity().getText(R.string.quantity_short));
    	ViewUtils.configureTextView(getActivity(), elixirSpellsLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerElixirSpellsLabel);

    	TextView darkElixirSpellsLabel = (TextView)view.findViewById(R.id.textview_dark_elixir_spells_title);
    	TextView spinnerDarkElixirSpellsLabel = (TextView)view.findViewById(R.id.spinner_dark_elixir_spells_title);
    	darkElixirSpellsLabel.setText(getActivity().getText(R.string.dark_elixir_spells));
    	spinnerDarkElixirSpellsLabel.setText(getActivity().getText(R.string.quantity_short));
    	ViewUtils.configureTextView(getActivity(), darkElixirSpellsLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerDarkElixirSpellsLabel);
    	
    	// Train information
    	TextView trainInfoTitle = (TextView) view.findViewById(R.id.train_info_title);
    	elixirCost = (TextView) view.findViewById(R.id.elixir_cost);
    	darkElixirCost = (TextView) view.findViewById(R.id.dark_elixir_cost);
    	totalTimeTrainTroops = (TextView) view.findViewById(R.id.total_time_to_train_troops);
    	ViewUtils.configureTextView(getActivity(), trainInfoTitle);
    	ViewUtils.configureTextView(getActivity(), elixirCost);
    	ViewUtils.configureTextView(getActivity(), darkElixirCost);
    	ViewUtils.configureTextView(getActivity(), totalTimeTrainTroops);
    	
    	// Brew information
    	TextView brewInfoTitle = (TextView) view.findViewById(R.id.brew_info_title);
    	elixirCostToBrew = (TextView) view.findViewById(R.id.elixir_cost_to_brew);
    	darkElixirCostToBrew = (TextView) view.findViewById(R.id.dark_elixir_cost_to_brew);
    	totalTimeBrewSpells = (TextView) view.findViewById(R.id.total_time_to_brew_spells);
    	ViewUtils.configureTextView(getActivity(), brewInfoTitle);
    	ViewUtils.configureTextView(getActivity(), elixirCostToBrew);
    	ViewUtils.configureTextView(getActivity(), darkElixirCostToBrew);
    	ViewUtils.configureTextView(getActivity(), totalTimeBrewSpells);

    	// Create rows 
    	createElixirTroopsRows((LinearLayout) view.findViewById(R.id.table_elixir_troops_title), inflater, container);
    	createDarkElixirTroopsRows((LinearLayout) view.findViewById(R.id.table_dark_elixir_troops_title), inflater, container);
    	createSpellsRow((LinearLayout) view.findViewById(R.id.table_elixir_spells_title), inflater, container);
    	createDarkSpellsRow((LinearLayout) view.findViewById(R.id.table_dark_elixir_spells_title), inflater, container);
    	
    	return view;
	}

	private void createTroopsRow(List<EntityType> troopsTypes, List<Entity> troops, List<Spinner> troopsSpinner,   
			LinearLayout table, LayoutInflater inflater, ViewGroup container) {
		Map<EntityType, String> mapEntityType = Global.getEntityTypeStringMap(getActivity());
		
		for (int i = 0; i < troopsTypes.size(); i++) {
			EntityType type = troopsTypes.get(i);
			Entity troop = Global.getVillage().getEntity(type, 1);
			if (troop.getLevel() > 0)
				troops.add(troop);
		}
		
		for (Entity e : troops) {
			String screenEntityType = mapEntityType.get(e.getType());
			
			View row = inflater.inflate(R.layout.row_image_text_spinner, container, false);
    		final CircleImageView circleIV = (CircleImageView)row.findViewById(R.id.entity_profile_image);
    		circleIV.setFillColorResource(android.R.color.white);
    		
    		TextView entityLabel = (TextView) row.findViewById(R.id.entity_label);
    		ViewUtils.configureTextView(getActivity(), entityLabel);
    		Spinner troopQuantity = (Spinner) row.findViewById(R.id.level_picker);
    		circleIV.setImageResource(ImageUtils.getImages(getActivity(), screenEntityType)[e.getLevel()]);
    		entityLabel.setText(screenEntityType);
    		
    		int troopSpace = troopsTrainingMap.get(e.getType()).getSpace();
    		int maxTroopType = availableSpace/troopSpace;
    		ViewUtils.configureIntegerSpinner(troopQuantity, getActivity(), 0, maxTroopType);

        	troopQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            	@Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            		updateInformation();
                }
            	@Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });
        	troopsSpinner.add(troopQuantity);
    		table.addView(row);
    		
    		View row2 = inflater.inflate(R.layout.horizontal_line, container, false);
    		table.addView(row2);
    	}
		
		if (troops.size() == 0)
			table.setVisibility(View.GONE);
	}
	private void createElixirTroopsRows(LinearLayout table, LayoutInflater inflater, ViewGroup container) {
		createTroopsRow(EntityType.getElixirTroops(), elixirTroops, elixirTroopSpinners, table, inflater, container);
	}
	private void createDarkElixirTroopsRows(LinearLayout table, LayoutInflater inflater, ViewGroup container) {
		createTroopsRow(EntityType.getDarkElixirTroops(), darkElixirTroops, darkElixirTroopSpinners, table, inflater, container);
	}
	private void createSpellsRow(LinearLayout table, LayoutInflater inflater, ViewGroup container) {
		createTroopsRow(EntityType.getSpells(), elixirSpells, elixirSpellsSpinners, table, inflater, container);
	}
	private void createDarkSpellsRow(LinearLayout table, LayoutInflater inflater, ViewGroup container) {
		createTroopsRow(EntityType.getDarkSpells(), darkElixirSpells, darkElixirSpellsSpinners, table, inflater, container);
	}

	private void initiateVariables() {
		troopsTrainingMap = TroopsAndSpellsTrainingTable.getAllTroopsAndSpellsTrainingTable();
		availableSpace = Global.getVillage().getAvailabeSpaceInArmyCamps();
		availableSpellsSpace = Global.getVillage().getAvailableSpaceInSpellFactorys();
		elixirTroops = new ArrayList<Entity>();
		elixirTroopSpinners = new ArrayList<Spinner>();
		darkElixirTroops = new ArrayList<Entity>();
		darkElixirTroopSpinners = new ArrayList<Spinner>();
		elixirSpells = new ArrayList<Entity>();
		elixirSpellsSpinners = new ArrayList<Spinner>();
		darkElixirSpells = new ArrayList<Entity>();
		darkElixirSpellsSpinners = new ArrayList<Spinner>();
	}

	private void updateInformation() {
		String dSymbol = getActivity().getString(R.string.day_letter); 
		String hSymbol = getActivity().getString(R.string.hour_letter);
		String mSymbol = getActivity().getString(R.string.minute_letter);
		String sSymbol = getActivity().getString(R.string.second_letter);
		String emptyString = getActivity().getString(R.string.zero);

		/* Army information */
		int occupation = 0;
		Resource cost = new Resource(0,0,0);
		CoCTime elixirTroopsTime = new CoCTime(0, 0, 0, 0);
		CoCTime darkElixirTroopsTime = new CoCTime(0, 0, 0, 0);
		
		for (int i = 0; i < elixirTroops.size(); i++) {
			Entity e = elixirTroops.get(i);
			Spinner s = elixirTroopSpinners.get(i);
			TroopsAndSpellsTrainingTable trainingMap = troopsTrainingMap.get(e.getType());
			
			int troopSpace = trainingMap.getSpace();
			int troopQty = Integer.valueOf(s.getSelectedItem().toString());
			cost = cost.sum(trainingMap.getCostToTrain(e.getLevel()).multiply(troopQty));
			elixirTroopsTime = elixirTroopsTime.sum(trainingMap.getTimeToTrain(e.getLevel()).multiply(troopQty));
			occupation += (troopQty * troopSpace);
		}
		
		for (int i = 0; i < darkElixirTroops.size(); i++) {
			Entity e = darkElixirTroops.get(i);
			Spinner s = darkElixirTroopSpinners.get(i);
			TroopsAndSpellsTrainingTable trainingMap = troopsTrainingMap.get(e.getType());

			int troopSpace = trainingMap.getSpace();
			int troopQty = Integer.valueOf(s.getSelectedItem().toString());
			cost = cost.sum(trainingMap.getCostToTrain(e.getLevel()).multiply(troopQty));
			darkElixirTroopsTime = darkElixirTroopsTime.sum(trainingMap.getTimeToTrain(e.getLevel()).multiply(troopQty));
			occupation += (troopQty * troopSpace);
		}
		
		armyCampOccupation.setText(getActivity().getString(R.string.army) + " " + occupation + "/" + availableSpace);

		if (occupation > availableSpace) {
			armyCampOccupation.setTextColor(ContextCompat.getColor(getActivity(), R.color.app_alert_foreground_color));
			Toast.makeText(getActivity(), getActivity().getString(R.string.reduce_army_message), Toast.LENGTH_SHORT).show();
		} else {
			armyCampOccupation.setTextColor(ContextCompat.getColor(getActivity(), R.color.app_foreground_color));
		}
		
		/* Spells information */
		int spellsOccupation = 0;
		Resource spellsCost = new Resource(0,0,0);
		CoCTime elixirSpellsTime = new CoCTime(0, 0, 0, 0);
		CoCTime darkElixirSpellsTime = new CoCTime(0, 0, 0, 0);
		
		for (int i = 0; i < elixirSpells.size(); i++) {
			Entity e = elixirSpells.get(i);
			Spinner s = elixirSpellsSpinners.get(i);
			TroopsAndSpellsTrainingTable trainingMap = troopsTrainingMap.get(e.getType());
			
			int spellSpace = trainingMap.getSpace();
			int spellQty = Integer.valueOf(s.getSelectedItem().toString());
			spellsCost = spellsCost.sum(trainingMap.getCostToTrain(e.getLevel()).multiply(spellQty));
			elixirSpellsTime = elixirSpellsTime.sum(trainingMap.getTimeToTrain(e.getLevel()).multiply(spellQty));
			spellsOccupation += (spellQty * spellSpace);
		}
		
		for (int i = 0; i < darkElixirSpells.size(); i++) {
			Entity e = darkElixirSpells.get(i);
			Spinner s = darkElixirSpellsSpinners.get(i);
			TroopsAndSpellsTrainingTable trainingMap = troopsTrainingMap.get(e.getType());

			int darkSpellsSpace = trainingMap.getSpace();
			int darkSpellsQty = Integer.valueOf(s.getSelectedItem().toString());
			spellsCost = spellsCost.sum(trainingMap.getCostToTrain(e.getLevel()).multiply(darkSpellsQty));
			darkElixirSpellsTime = darkElixirSpellsTime.sum(trainingMap.getTimeToTrain(e.getLevel()).multiply(darkSpellsQty));
			spellsOccupation += (darkSpellsQty * darkSpellsSpace);
		}
		
		this.spellsOccupation.setText(getActivity().getString(R.string.spells) + " " + spellsOccupation + "/" + availableSpellsSpace);

		if (spellsOccupation > availableSpellsSpace) {
			this.spellsOccupation.setTextColor(ContextCompat.getColor(getActivity(), R.color.app_alert_foreground_color));
			Toast.makeText(getActivity(), getActivity().getString(R.string.reduce_spells_message), Toast.LENGTH_SHORT).show();
		} else {
			this.spellsOccupation.setTextColor(ContextCompat.getColor(getActivity(), R.color.app_foreground_color));
		}

		/* Set final text */
		int nBarracks = Global.getVillage().countItem(EntityType.BARRACK);
		int nDarkBarracks = Global.getVillage().countItem(EntityType.DARK_BARRACK);
		elixirTroopsTime = elixirTroopsTime.multiply(1.0/nBarracks);
		darkElixirTroopsTime = darkElixirTroopsTime.multiply(1.0/nDarkBarracks);
		
		elixirCost.setText(Formatters.format(cost.getElixir()));
		darkElixirCost.setText(Formatters.format(cost.getDarkElixir()));
		if (elixirTroopsTime.compareTo(darkElixirTroopsTime) > 0)
			totalTimeTrainTroops.setText(Formatters.format(elixirTroopsTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
		else
			totalTimeTrainTroops.setText(Formatters.format(darkElixirTroopsTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
		
		// NOTE: TODAY THERE IS ONLY ONE SPELL FACTORY AND DARK SPELL FACTORY.
		// BUT LEAVE IT HERE IN CASE OF NEW SPELL FACTORIES
		int nSpellFactory = Global.getVillage().countItem(EntityType.SPELL_FACTORY);
		int nDarkSpellFactory = Global.getVillage().countItem(EntityType.DARK_SPELL_FACTORY);
		elixirSpellsTime = elixirSpellsTime.multiply(1.0/nSpellFactory);
		darkElixirSpellsTime = darkElixirSpellsTime.multiply(1.0/nDarkSpellFactory);
		
		elixirCostToBrew.setText(Formatters.format(spellsCost.getElixir()));
		darkElixirCostToBrew.setText(Formatters.format(spellsCost.getDarkElixir()));
		if (elixirSpellsTime.compareTo(darkElixirSpellsTime) > 0)
			totalTimeBrewSpells.setText(Formatters.format(elixirSpellsTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
		else
			totalTimeBrewSpells.setText(Formatters.format(darkElixirSpellsTime, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
	}
}