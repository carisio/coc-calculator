package com.carisio.apps.coccalculator.main.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.ImageUtils;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.EntityType;

import de.hdodenhof.circleimageview.CircleImageView;

public class BuilderFragment extends RetainedFragment {
	private Spinner qtdPicker;
	private int maxQtdAllowed;
	
	@Override
	public void onResume() {
		super.onResume();
        qtdPicker.setSelection(Global.getVillage().getNumBuilder()-1);
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	initializeVariables();
    	
    	String builderLabel = getActivity().getString(R.string.builder);
    	getActivity().setTitle(builderLabel);

    	View view = inflater.inflate(R.layout.image_text_spinner_container, container, false);
    	LinearLayout table = (LinearLayout)view.findViewById(R.id.table_text_spinner);
    	
    	// Configure table title
    	View title = view.findViewById(R.id.table_title);
    	TextView mainLabel = (TextView)title.findViewById(R.id.textview_column_label);
    	TextView spinnerLabel = (TextView)title.findViewById(R.id.spinner_table_label);
    	mainLabel.setText(getActivity().getString(R.string.buildings_label));
    	spinnerLabel.setText(getActivity().getText(R.string.quantity_short));
    	ViewUtils.configureTextView(getActivity(), mainLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerLabel);
    	
    
		View row = (View)inflater.inflate(R.layout.row_image_text_spinner, null);
		CircleImageView profileImage = (CircleImageView)row.findViewById(R.id.entity_profile_image);
		profileImage.setFillColorResource(android.R.color.white);
		profileImage.setImageResource(ImageUtils.getBuilderHutImage());
		
		TextView entityLabel = (TextView) row.findViewById(R.id.entity_label);
		ViewUtils.configureTextView(getActivity(), entityLabel);
		
		qtdPicker = (Spinner) row.findViewById(R.id.level_picker);
		
		entityLabel.setText(builderLabel);

		ViewUtils.configureIntegerSpinner(qtdPicker, getActivity(), 1, maxQtdAllowed);
		
        qtdPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		int numBuilderSelected = position + 1;
				Global.getVillage().setNumBuilder(numBuilderSelected);
        		Global.saveVillage(getActivity());
            }
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
		table.addView(row);

		View row2 = inflater.inflate(R.layout.horizontal_line, container, false);
		table.addView(row2);
		
		return view;
    }

    private void initializeVariables() {
    	maxQtdAllowed = EntityType.BUILDER.getMaximumNumberOfEntityByTH(1);
    }
}
