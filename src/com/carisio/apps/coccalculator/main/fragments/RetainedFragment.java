package com.carisio.apps.coccalculator.main.fragments;

import android.app.Fragment;
import android.os.Bundle;

public class RetainedFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
}
