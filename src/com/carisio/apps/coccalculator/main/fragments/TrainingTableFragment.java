package com.carisio.apps.coccalculator.main.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.Formatters;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.CoCTime;
import com.carisio.apps.coccalculatorengine.EntityType;
import com.carisio.apps.coccalculatorengine.Resource;
import com.carisio.apps.coccalculatorengine.TroopsAndSpellsTrainingTable;

public class TrainingTableFragment extends RetainedFragment {
	private static Spinner spnEntityType;
	private ListView listView;
	private Map<EntityType, TroopsAndSpellsTrainingTable> mapTrainingInfo;
			
	private int posEntityType = -1;
	
	@Override
	public void onResume() {
		super.onResume();
		spnEntityType.setSelection(posEntityType);
	}
	@Override
	public void onPause() {
		super.onPause();
		posEntityType = spnEntityType.getSelectedItemPosition();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mapTrainingInfo = TroopsAndSpellsTrainingTable.getAllTroopsAndSpellsTrainingTable();
		
    	getActivity().setTitle(getActivity().getString(R.string.show_training_cost));
    	
    	View view = inflater.inflate(R.layout.training_table_for_troops_and_spells_layout, container, false);
    	
    	// Configure view
    	TextView type = (TextView)view.findViewById(R.id.type);
    	spnEntityType = (Spinner)view.findViewById(R.id.type_options);
    	
    	List<EntityType> entityTypes = EntityType.getTroopsAndSpells();
    	Map<EntityType, String> map = Global.getEntityTypeStringMap(getActivity());
    	List<String> troopsSpellsNames = new ArrayList<String>();
    	for (EntityType t : entityTypes)
    		troopsSpellsNames.add(map.get(t));
    	
    	
    	ViewUtils.configureTextView(getActivity(), type);
    	ViewUtils.configureSpinner(spnEntityType, getActivity(), troopsSpellsNames);
    	
    	spnEntityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		updateList();
            }
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
    	
    	listView = (ListView) view.findViewById(R.id.training_table);
    	
		return view;
	}
	
	private void updateList() {
		String selectedEntityName = (String) spnEntityType.getSelectedItem();
		
		Map<String, EntityType> map = Global.getEntityTypeMap(getActivity());
		EntityType entityType = map.get(selectedEntityName);
		
		List<Integer> levels = new ArrayList<Integer>();
		int maxLevel = entityType.getEvolutionTable().getMaxLevel();
		for (int i = 0; i <= maxLevel; i++) { // Row 0 is used as a header
			levels.add(i);
		}
		listView.setAdapter(new ShowTrainingTableAdapter(getActivity(), R.layout.row_training_table_troops_spells, levels, selectedEntityName, entityType));
	}
	
	class ShowTrainingTableAdapter extends ArrayAdapter<Integer> {
		private EntityType selectedEntityType;
		private String dSymbol; 
		private String hSymbol;
		private String mSymbol;
		private String sSymbol;
		private String emptyString;

		
		public ShowTrainingTableAdapter(Context context, int resource, List<Integer> levels, String selectedEntityType, EntityType entityType) {
			super(context, resource, levels);
			this.selectedEntityType = entityType;
			
			dSymbol = getActivity().getString(R.string.day_letter); 
			hSymbol = getActivity().getString(R.string.hour_letter);
			mSymbol = getActivity().getString(R.string.minute_letter);
			sSymbol = getActivity().getString(R.string.second_letter);
			emptyString = getActivity().getString(R.string.zero);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            View view = vi.inflate(R.layout.row_training_table_troops_spells, null);

            TextView txtLvl = (TextView) view.findViewById(R.id.entity_level);
            TextView txtTime = (TextView) view.findViewById(R.id.entity_time);
            TextView txtCost = (TextView) view.findViewById(R.id.entity_cost);
            ImageView imgForCost = (ImageView) view.findViewById(R.id.img_for_entity_cost);
            
            ViewUtils.configureTextView(getActivity(), txtLvl);
            ViewUtils.configureTextView(getActivity(), txtTime);
            ViewUtils.configureTextView(getActivity(), txtCost);
            
            Integer lvlReq = getItem(position);
            
            if (lvlReq == 0) {
            	txtLvl.setText(getContext().getString(R.string.level_short));
            	txtTime.setText(getContext().getString(R.string.time_to_train));
            	txtCost.setText(getContext().getString(R.string.cost_to_train));
            	
            	return view;
            }
            
            TroopsAndSpellsTrainingTable trainingTable = mapTrainingInfo.get(selectedEntityType);
            Resource costToTrain = trainingTable.getCostToTrain(lvlReq);
            CoCTime timeToTrain = trainingTable.getTimeToTrain(lvlReq);

            int[] image = new int[1];
            txtLvl.setText("" + lvlReq);
            txtTime.setText(Formatters.format(timeToTrain, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
            txtCost.setText(getFormattedResourceAndFormatImage(image, costToTrain));
            imgForCost.setImageResource(image[0]);
            
            return view;
		}
		
		private String getFormattedResourceAndFormatImage(int[] img, Resource resource) {
			String formatted_cost = "0";
			if (resource.getGold() > 0) {
				formatted_cost = Formatters.format(resource.getGold());
				img[0] = R.drawable.gold;
			}
			if (resource.getElixir() > 0) {
				formatted_cost = Formatters.format(resource.getElixir());
				img[0] = R.drawable.elixir;
			}
			if (resource.getDarkElixir() > 0) {
				formatted_cost = Formatters.format(resource.getDarkElixir());
				img[0] = R.drawable.dark_elixir;
			}
			return formatted_cost;
		}	
	}
}
