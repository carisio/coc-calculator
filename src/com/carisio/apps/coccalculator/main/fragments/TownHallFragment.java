package com.carisio.apps.coccalculator.main.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.ImageUtils;
import com.carisio.apps.coccalculator.utils.ViewUtils;

import de.hdodenhof.circleimageview.CircleImageView;

public class TownHallFragment extends RetainedFragment {
	private String entityName;
	private int maxLevelAllowed;
	private Spinner levelPicker;
	private int initialTownHallLevel = -1;

	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	initializeVariables();
    	
    	getActivity().setTitle(entityName);

    	
    	View view = inflater.inflate(R.layout.image_text_spinner_container, container, false);
    	LinearLayout table = (LinearLayout)view.findViewById(R.id.table_text_spinner);

    	
    	// Configure table title
    	View title = view.findViewById(R.id.table_title);
    	TextView mainLabel = (TextView)title.findViewById(R.id.textview_column_label);
    	TextView spinnerLabel = (TextView)title.findViewById(R.id.spinner_table_label);
    	mainLabel.setText(getActivity().getText(R.string.buildings_label));
    	spinnerLabel.setText(getActivity().getText(R.string.level_short));
    	ViewUtils.configureTextView(getActivity(), mainLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerLabel);
    	
    	
		View row = (View)inflater.inflate(R.layout.row_image_text_spinner, null);
		final CircleImageView profileImage = (CircleImageView)row.findViewById(R.id.entity_profile_image);
		profileImage.setFillColorResource(android.R.color.white);
		profileImage.setImageResource(ImageUtils.getBuilderHutImage());
		
		TextView entityLabel = (TextView) row.findViewById(R.id.entity_label);
		ViewUtils.configureTextView(getActivity(), entityLabel);

		levelPicker = (Spinner) row.findViewById(R.id.level_picker);
		
		entityLabel.setText(entityName);

		ViewUtils.configureIntegerSpinner(levelPicker, getActivity(), 1, maxLevelAllowed);
		
        levelPicker.setSelection(initialTownHallLevel-1);
        levelPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		int newThLevel = position + 1;
				if (initialTownHallLevel > newThLevel) {
					showConfirmMessage(newThLevel);
				} else {
					Global.getVillage().setTownHallLevel(newThLevel);
				}
				profileImage.setImageResource(ImageUtils.getTownHownImages()[newThLevel]);
        		Global.saveVillage(getActivity());
            }
        	private void showConfirmMessage(final int newTHLevel) {
        		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle(getActivity().getString(R.string.lost_th_progress_title));
				builder.setMessage(getActivity().getString(R.string.lost_th_message_question));
				builder.setPositiveButton(getActivity().getString(R.string.lost_th_positive_button), new DialogInterface.OnClickListener() { 
					public void onClick(DialogInterface arg0, int arg1) {
						Global.getVillage().setTownHallLevel(newTHLevel);
						initialTownHallLevel = newTHLevel;
					}
				}); 
				builder.setNegativeButton(getActivity().getString(R.string.lost_th_cancel_button), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						levelPicker.setSelection(initialTownHallLevel-1);
					}
				});
				builder.create();
				builder.show();
        	}
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
		table.addView(row);
		
		View row2 = inflater.inflate(R.layout.horizontal_line, container, false);
		table.addView(row2);
		
        return view;
    }

    private void initializeVariables() {
    	entityName = getActivity().getString(R.string.town_hall);
    	initialTownHallLevel = Global.getVillage().getTownHallLevel();
    	maxLevelAllowed = Global.getVillage().getMaxTownHallLevel();
    }
}