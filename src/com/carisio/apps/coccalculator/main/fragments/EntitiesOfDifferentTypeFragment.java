package com.carisio.apps.coccalculator.main.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.ImageUtils;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.EntityType;

import de.hdodenhof.circleimageview.CircleImageView;

public class EntitiesOfDifferentTypeFragment extends RetainedFragment {
	public static final String ENTITY_CATEGORY = "ENTITY_CATEGORY";
	private List<Spinner> spinners;
	private List<String> entityNames;
	private List<CircleImageView> profileImages;
	
    public EntitiesOfDifferentTypeFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPause() {
    	super.onPause();
		for (int i = 0; i < spinners.size(); i++) {
			EntityType type = Global.getEntityTypeMap(getActivity()).get(entityNames.get(i));
			Spinner spn = spinners.get(i);
			Global.getVillage().getEntity(type, 1).setLevel(spn.getSelectedItemPosition());
		}
		Global.saveVillage(getActivity());    	
    }
    @Override
    public void onResume() {
    	super.onResume();
    	int thLevel = Global.getVillage().getTownHallLevel();
    	for (int i = 0; i < spinners.size(); i++) {
    		EntityType type = Global.getEntityTypeMap(getActivity()).get(entityNames.get(i));
    		short maxLevel = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(thLevel);
    		Spinner levelPicker = spinners.get(i);
    		
	    	int level = Global.getVillage().getEntity(type, 1).getLevel();
	        if (level > maxLevel)
	        	level = maxLevel;

        	levelPicker.setSelection(level);
            profileImages.get(i).setImageResource(ImageUtils.getImages(getActivity(), entityNames.get(i))[level]);
    	}
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	initializeVariables();

    	View view = inflater.inflate(R.layout.image_text_spinner_container, container, false);
    	LinearLayout table = (LinearLayout)view.findViewById(R.id.table_text_spinner);
    	
    	getActivity().setTitle(getArguments().getString(ENTITY_CATEGORY));
    	
    	spinners = new ArrayList<Spinner>();

    	// Configure table title
    	View title = view.findViewById(R.id.table_title);
    	TextView mainLabel = (TextView)title.findViewById(R.id.textview_column_label);
    	TextView spinnerLabel = (TextView)title.findViewById(R.id.spinner_table_label);
    	mainLabel.setText(getArguments().getString(ENTITY_CATEGORY));
    	spinnerLabel.setText(getActivity().getText(R.string.level_short));
    	ViewUtils.configureTextView(getActivity(), mainLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerLabel);
    	
    	int thLevel = Global.getVillage().getTownHallLevel();
    	for (int i = 0; i < entityNames.size(); i++) {
    		final String entityName = entityNames.get(i);
    		EntityType type = Global.getEntityTypeMap(getActivity()).get(entityNames.get(i));
    		
    		short maxLevel = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(thLevel);

    		View row = (View)inflater.inflate(R.layout.row_image_text_spinner, null);
    		final CircleImageView circleImageView = (CircleImageView)row.findViewById(R.id.entity_profile_image);
    		circleImageView.setFillColorResource(android.R.color.white);
    		profileImages.add(circleImageView);
    		TextView entityLabel = (TextView) row.findViewById(R.id.entity_label);
    		ViewUtils.configureTextView(getActivity(), entityLabel);
    		Spinner levelPicker = (Spinner) row.findViewById(R.id.level_picker);
    		spinners.add(levelPicker);
    		
    		entityLabel.setText(entityNames.get(i));
             
    		ViewUtils.configureIntegerSpinner(levelPicker, getActivity(), 0, maxLevel);

    		levelPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					circleImageView.setImageResource(ImageUtils.getImages(getActivity(), entityName)[position]);
				}
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
    		});

    		table.addView(row);
    		
    		View row2 = inflater.inflate(R.layout.horizontal_line, container, false);
    		table.addView(row2);
    	}
        return view;
    }

    private void initializeVariables() {
    	String entityCategory = getArguments().getString(ENTITY_CATEGORY);
    	entityNames = new ArrayList<String>();
    	profileImages = new ArrayList<CircleImageView>();
    	
    	int thLevel = Global.getVillage().getTownHallLevel();
    	
    	if (entityCategory.equals(getString(R.string.elixir_troops))) {
    		entityNames.add(getString(R.string.barbarian));
    		entityNames.add(getString(R.string.archer));
    		entityNames.add(getString(R.string.giant));
    		entityNames.add(getString(R.string.goblin));
    		entityNames.add(getString(R.string.wall_breaker));
    		entityNames.add(getString(R.string.balloon));
    		entityNames.add(getString(R.string.wizard));
    		entityNames.add(getString(R.string.healer));
    		entityNames.add(getString(R.string.dragon));
    		entityNames.add(getString(R.string.pekka));
    		entityNames.add(getString(R.string.baby_dragon));
    		entityNames.add(getString(R.string.miner));
    	} else if (entityCategory.equals(getString(R.string.dark_elixir_troops))) {
    		entityNames.add(getString(R.string.minion));
    		entityNames.add(getString(R.string.hog_rider));
    		entityNames.add(getString(R.string.valkyrie));
    		entityNames.add(getString(R.string.golem));
    		entityNames.add(getString(R.string.witch));
    		entityNames.add(getString(R.string.lava_hound));
    		entityNames.add(getString(R.string.bowler));
    	} else if (entityCategory.equals(getString(R.string.heroes_troops))) {
    		entityNames.add(getString(R.string.barbarian_king));
    		entityNames.add(getString(R.string.archer_queen));
    		entityNames.add(getString(R.string.grand_warden));
    	} else if (entityCategory.equals(getString(R.string.elixir_spells))) {
    		entityNames.add(getString(R.string.lightning_spell));
    		entityNames.add(getString(R.string.healing_spell));
    		entityNames.add(getString(R.string.rage_spell));
    		entityNames.add(getString(R.string.jump_spell));
    		entityNames.add(getString(R.string.freeze_spell));
    		entityNames.add(getString(R.string.clone_spell));
    	} else if (entityCategory.equals(getString(R.string.dark_elixir_spells))) {
    		entityNames.add(getString(R.string.poison_spell));
    		entityNames.add(getString(R.string.earthquake_spell));
    		entityNames.add(getString(R.string.haste_spell));
    		entityNames.add(getString(R.string.skeleton_spell));
    	}
    	for (int i = entityNames.size()-1; i >= 0; i--) {
    		EntityType type = Global.getEntityTypeMap(getActivity()).get(entityNames.get(i));
    		if (type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(thLevel) == (short) 0) {
    			entityNames.remove(i);
    		}
    	}
    }
}
