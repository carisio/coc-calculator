package com.carisio.apps.coccalculator.main.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.ImageUtils;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.EntityType;

import de.hdodenhof.circleimageview.CircleImageView;

public class EntitiesOfSameTypeFragment extends RetainedFragment {
	public static final String ENTITY_NAME = "ENTITY_NAME";
	public static final String CATEGORY_NAME = "CATEGORY_NAME";

	private String entityName;
	private String categoryName;
	
	private EntityType type;
	private int maxQuantityForTheVillage;
	private int maxLevelAllowed;
	private List<Spinner> spinners;
	private List<CircleImageView> images;
	
	private int[] imagesResources;
	
    public EntitiesOfSameTypeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	initializeVariables();
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
    	super.onResume();
    	for (int i = 0; i < spinners.size(); i++) {
			Spinner spn = spinners.get(i);
            int level = Global.getVillage().getEntity(type, i+1).getLevel();
            if (level > maxLevelAllowed)
            	level = maxLevelAllowed;
            
            spn.setSelection(level);
            images.get(i).setImageResource(imagesResources[level]);
		}
    }
    @Override
    public void onPause() {
		super.onPause();    	
		for (int i = 0; i < spinners.size(); i++) {
			Spinner spn = spinners.get(i);
			Global.getVillage().getEntity(type, i+1).setLevel(spn.getSelectedItemPosition());
		}
		Global.saveVillage(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	initializeVariables();
    	
    	getActivity().setTitle(entityName);

    	View view = inflater.inflate(R.layout.image_text_spinner_container, container, false);
    	LinearLayout table = (LinearLayout)view.findViewById(R.id.table_text_spinner);

    	createTableTitle(view);
    	
    	for (int i = 0; i < maxQuantityForTheVillage; i++) {
    		View row = (View)inflater.inflate(R.layout.row_image_text_spinner, null);
    		final CircleImageView circleImageView = (CircleImageView)row.findViewById(R.id.entity_profile_image);
    		circleImageView.setFillColorResource(android.R.color.white);
    		TextView entityLabel = (TextView) row.findViewById(R.id.entity_label);
    		ViewUtils.configureTextView(getActivity(), entityLabel);
    		Spinner levelPicker = (Spinner) row.findViewById(R.id.level_picker);
    		images.add(circleImageView);
    		spinners.add(levelPicker);

    		if (maxQuantityForTheVillage == 1) {
    			entityLabel.setText(entityName);
    		} else {
    			entityLabel.setText(entityName + " #" + (i+1));
    		}
    		
    		ViewUtils.configureIntegerSpinner(levelPicker, getActivity(), 0, maxLevelAllowed);
    		levelPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					circleImageView.setImageResource(imagesResources[position]);
				}
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
    		});

    		table.addView(row);
    		
    		View row2 = inflater.inflate(R.layout.horizontal_line, container, false);
    		table.addView(row2);
    	}
    	
        return view;
    }

	private void createTableTitle(View container) {
		// Configure table title
    	View title = container.findViewById(R.id.table_title);
    	TextView mainLabel = (TextView)title.findViewById(R.id.textview_column_label);
    	TextView spinnerLabel = (TextView)title.findViewById(R.id.spinner_table_label);
    	ViewUtils.configureTextView(getActivity(), mainLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerLabel);
    	spinnerLabel.setText(getActivity().getText(R.string.level_short));
    	
    	if (categoryName.equals(getActivity().getText(R.string.traps_section))) {
    		mainLabel.setText(categoryName);
    	} else {
    		mainLabel.setText(getActivity().getText(R.string.buildings_label));
    	}
	}

    private void initializeVariables() {
    	spinners = new ArrayList<Spinner>();
    	images = new ArrayList<CircleImageView>();
    	
    	entityName = getArguments().getString(ENTITY_NAME);
    	categoryName = getArguments().getString(CATEGORY_NAME);
    	int thLevel = Global.getVillage().getEntity(EntityType.TOWN_HALL, 1).getLevel();

    	type = Global.getEntityTypeMap(getActivity()).get(entityName);
    	maxLevelAllowed = type.getEvolutionTable().getMaxLevelAllowedAtTHLevel(thLevel);
        maxQuantityForTheVillage = type.getMaximumNumberOfEntityByTH(thLevel);
        
        imagesResources = ImageUtils.getImages(getActivity(), entityName);
    }
}
