package com.carisio.apps.coccalculator.main.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.Formatters;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.Entity;
import com.carisio.apps.coccalculatorengine.EntityType;
import com.carisio.apps.coccalculatorengine.ResourcesTable;

public class ResourceProductionFragment extends RetainedFragment {
	private int boostMultiplier = 1;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.resource_production_layout, container, false);

    	getActivity().setTitle(getActivity().getString(R.string.resource_production));
    	configureControls(view);
    	
    	return view;
    }
	
	private void configureControls(View view) {
		TextView title = (TextView) view.findViewById(R.id.resource_production_rate_title);
		TextView boostText = (TextView) view.findViewById(R.id.boost_text);
    	final Spinner boostResources = (Spinner) view.findViewById(R.id.boost_resources);
    	TextView hourly = (TextView) view.findViewById(R.id.hourly);
    	TextView goldHourlyValue = (TextView) view.findViewById(R.id.gold_hourly_value);
    	TextView elixirHourlyValue = (TextView) view.findViewById(R.id.elixir_hourly_value);
    	TextView darkElixirHourlyValue = (TextView) view.findViewById(R.id.dark_elixir_hourly_value);
    	TextView daily = (TextView) view.findViewById(R.id.daily);
    	TextView goldDailyValue = (TextView) view.findViewById(R.id.gold_daily_value);
    	TextView elixirDailyValue = (TextView) view.findViewById(R.id.elixir_daily_value);
    	TextView darkElixirDailyValue = (TextView) view.findViewById(R.id.dark_elixir_daily_value);
    	TextView weekly = (TextView) view.findViewById(R.id.weekly);
    	TextView goldWeeklyValue = (TextView) view.findViewById(R.id.gold_weekly_value);
    	TextView elixirWeeklyValue = (TextView) view.findViewById(R.id.elixir_weekly_value);
    	TextView darkElixirWeeklyValue = (TextView) view.findViewById(R.id.dark_elixir_weekly_value);
    	TextView monthly = (TextView) view.findViewById(R.id.monthly);
    	TextView goldMonthlyValue = (TextView) view.findViewById(R.id.gold_monthly_value);
    	TextView elixirMonthlyValue = (TextView) view.findViewById(R.id.elixir_monthly_value);
    	TextView darkElixirMonthlyValue = (TextView) view.findViewById(R.id.dark_elixir_monthly_value);
    	
    	ViewUtils.configureTextView(getActivity(), title);
    	ViewUtils.configureTextView(getActivity(), boostText);
    	ViewUtils.configureSpinner(boostResources, getActivity(), getBoostOptions());
    	ViewUtils.configureTextView(getActivity(), hourly);
    	ViewUtils.configureTextView(getActivity(), goldHourlyValue);
    	ViewUtils.configureTextView(getActivity(), elixirHourlyValue);
    	ViewUtils.configureTextView(getActivity(), darkElixirHourlyValue);
    	ViewUtils.configureTextView(getActivity(), daily);
    	ViewUtils.configureTextView(getActivity(), goldDailyValue);
    	ViewUtils.configureTextView(getActivity(), elixirDailyValue);
    	ViewUtils.configureTextView(getActivity(), darkElixirDailyValue);
    	ViewUtils.configureTextView(getActivity(), weekly);
    	ViewUtils.configureTextView(getActivity(), goldWeeklyValue);
    	ViewUtils.configureTextView(getActivity(), elixirWeeklyValue);
    	ViewUtils.configureTextView(getActivity(), darkElixirWeeklyValue);
    	ViewUtils.configureTextView(getActivity(), monthly);
    	ViewUtils.configureTextView(getActivity(), goldMonthlyValue);
    	ViewUtils.configureTextView(getActivity(), elixirMonthlyValue);
    	ViewUtils.configureTextView(getActivity(), darkElixirMonthlyValue);

		boostResources.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		if (boostResources.getItemAtPosition(position).equals(getString(R.string.yes)))
        			boostMultiplier = 2;
        		else
        			boostMultiplier = 1;
        		
        		updateInfo();
            }
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
	}

	@Override
	public void onResume() {
		super.onResume();
		updateInfo();
	}

	private void updateInfo() {
		View view = getView();
		
    	TextView goldHourlyValue = (TextView) view.findViewById(R.id.gold_hourly_value);
    	TextView elixirHourlyValue = (TextView) view.findViewById(R.id.elixir_hourly_value);
    	TextView darkElixirHourlyValue = (TextView) view.findViewById(R.id.dark_elixir_hourly_value);
    	TextView goldDailyValue = (TextView) view.findViewById(R.id.gold_daily_value);
    	TextView elixirDailyValue = (TextView) view.findViewById(R.id.elixir_daily_value);
    	TextView darkElixirDailyValue = (TextView) view.findViewById(R.id.dark_elixir_daily_value);
    	TextView goldWeeklyValue = (TextView) view.findViewById(R.id.gold_weekly_value);
    	TextView elixirWeeklyValue = (TextView) view.findViewById(R.id.elixir_weekly_value);
    	TextView darkElixirWeeklyValue = (TextView) view.findViewById(R.id.dark_elixir_weekly_value);
    	TextView goldMonthlyValue = (TextView) view.findViewById(R.id.gold_monthly_value);
    	TextView elixirMonthlyValue = (TextView) view.findViewById(R.id.elixir_monthly_value);
    	TextView darkElixirMonthlyValue = (TextView) view.findViewById(R.id.dark_elixir_monthly_value);
    	
    	List<Entity> gold_mines = Global.getVillage().getAllEntities(EntityType.GOLD_MINE);
    	List<Entity> elixir_collectors = Global.getVillage().getAllEntities(EntityType.ELIXIR_COLLECTOR);
    	List<Entity> dark_elixir_drills = Global.getVillage().getAllEntities(EntityType.DARK_ELIXIR_DRILL);
    	ResourcesTable goldTable = ResourcesTable.getGoldMineResourceTable();
    	ResourcesTable elixirTable = ResourcesTable.getElixirCollectorResourceTable();
    	ResourcesTable darkElixirTable = ResourcesTable.getDarkElixirDrillResourceTable();
    	
    	long goldPerHour = 0;
    	long elixirPerHour = 0;
    	long darkElixirPerHour = 0;
    	
    	for (Entity e : gold_mines)
    		goldPerHour += goldTable.getProductionPerHour(e.getLevel());
    	for (Entity e : elixir_collectors)
    		elixirPerHour += elixirTable.getProductionPerHour(e.getLevel());
    	for (Entity e : dark_elixir_drills)
    		darkElixirPerHour += darkElixirTable.getProductionPerHour(e.getLevel());

    	goldPerHour *= boostMultiplier;
    	elixirPerHour *= boostMultiplier;
    	darkElixirPerHour *= boostMultiplier;
    	
    	goldHourlyValue.setText(Formatters.format(goldPerHour));
    	elixirHourlyValue.setText(Formatters.format(elixirPerHour));
    	darkElixirHourlyValue.setText(Formatters.format(darkElixirPerHour));

    	goldDailyValue.setText(Formatters.format(goldPerHour*24));
    	elixirDailyValue.setText(Formatters.format(elixirPerHour*24));
    	darkElixirDailyValue.setText(Formatters.format(darkElixirPerHour*24));
    	
    	goldWeeklyValue.setText(Formatters.format(goldPerHour*24*7));
    	elixirWeeklyValue.setText(Formatters.format(elixirPerHour*24*7));
    	darkElixirWeeklyValue.setText(Formatters.format(darkElixirPerHour*24*7));

    	goldMonthlyValue.setText(Formatters.format(goldPerHour*24*30));
    	elixirMonthlyValue.setText(Formatters.format(elixirPerHour*24*30));
    	darkElixirMonthlyValue.setText(Formatters.format(darkElixirPerHour*24*30));		
	}

	private List<String> getBoostOptions() {
		List<String> optionsList = new ArrayList<String>();
    	optionsList.add(getString(R.string.no));
    	optionsList.add(getString(R.string.yes));
		return optionsList;
	}
}
