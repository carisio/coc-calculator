
package com.carisio.apps.coccalculator.main.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.main.MainActivity;
import com.carisio.apps.coccalculator.utils.Formatters;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.CoCTime;
import com.carisio.apps.coccalculatorengine.EntityType;
import com.carisio.apps.coccalculatorengine.Resource;

public class EffortsToMaximizeVilllageFragment extends RetainedFragment {
	private static int savedTHLevel = -1;
	private static int savedBuilderNum = -1;
	private static int savedBuilderEff = -1;
	
	private Spinner spnTHLevel;
	private Spinner spnBuildersEff;
	private Spinner spnBuildersQtd;
	
	private TextView txtGoldValueConstructions;
	private TextView txtElixirValueConstructions;
	private TextView txtDarkElixirValueConstructions;
	private TextView txtGoldValueWalls;
	private TextView txtGoldElixirValueWalls;
	private TextView txtTimeUpgConValue;
	private TextView txtTimeUpgTroopsValue;
	
//	LinearLayout costOfEachEntityContainer;	
//	private List<View> rowEachEntityCost;
	
	@Override
	public void onResume() {
		super.onResume();
	
		if (savedTHLevel < 0) {
			savedTHLevel = Global.getVillage().getTownHallLevel();
			savedBuilderNum = EntityType.BUILDER.getMaximumNumberOfEntityByTH(savedTHLevel);
			savedBuilderEff = 100;
		}
        int position = ((ArrayAdapter<String>)spnTHLevel.getAdapter()).getPosition("" + savedTHLevel);
        spnTHLevel.setSelection(position);
        
        position = ((ArrayAdapter<String>)spnBuildersQtd.getAdapter()).getPosition("" + savedBuilderNum);
        spnBuildersQtd.setSelection(position);

        position = ((ArrayAdapter<String>)spnBuildersEff.getAdapter()).getPosition("" + savedBuilderEff);
        spnBuildersEff.setSelection(position);

        updateSimulationInfo();
//        updateCostOfEachEntity();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		savedTHLevel = getSelectedTHLevel();
		savedBuilderNum = getSelectedBuilderNum();
		savedBuilderEff = getSelectedBuilderEff();
	}
	private int getSelectedTHLevel() {
		return spnTHLevel.getSelectedItemPosition() + 1;
	}
	private int getSelectedBuilderNum() {
		return spnBuildersQtd.getSelectedItemPosition() + 1;
	}
	private int getSelectedBuilderEff() {
		return (spnBuildersEff.getSelectedItemPosition() + 1)*10;
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.efforts_maximize_village_layout, container, false);
    	
    	getActivity().setTitle(getActivity().getString(R.string.efforts_to_maximize_village));
    	
    	TextView txtMaximizeToTH = (TextView) view.findViewById(R.id.maximize_to_th);
    	spnTHLevel = (Spinner) view.findViewById(R.id.th_level);
    	TextView txtBuildersQtd = (TextView) view.findViewById(R.id.builders_label);
    	spnBuildersQtd = (Spinner) view.findViewById(R.id.builders_quantity);
    	TextView txtBuildersEff = (TextView) view.findViewById(R.id.builders_efficiency_label);
    	spnBuildersEff = (Spinner) view.findViewById(R.id.builders_efficiency_value);
    	TextView txtCostUpgradeConstructions = (TextView) view.findViewById(R.id.cost_to_upgrade_village);
    	txtGoldValueConstructions = (TextView) view.findViewById(R.id.gold_value_constructions);
    	txtElixirValueConstructions = (TextView) view.findViewById(R.id.elixir_value_constructions);
    	txtDarkElixirValueConstructions = (TextView) view.findViewById(R.id.dark_elixir_value_constructions);
    	TextView txtCostUpgradeWalls = (TextView) view.findViewById(R.id.cost_to_upgrade_walls);
    	txtGoldValueWalls = (TextView) view.findViewById(R.id.gold_value_walls);
    	txtGoldElixirValueWalls = (TextView) view.findViewById(R.id.gold_elixir_value_walls);
    	TextView txtTime = (TextView) view.findViewById(R.id.time);
    	TextView txtTimeUpgConLabel = (TextView) view.findViewById(R.id.time_to_upgrade_constructions_label);
    	txtTimeUpgConValue = (TextView) view.findViewById(R.id.time_to_upgrade_constructions_value);
    	TextView txtTimeUpgTroopsLabel = (TextView) view.findViewById(R.id.time_to_upgrade_troops_label);
    	txtTimeUpgTroopsValue = (TextView) view.findViewById(R.id.time_to_upgrade_troops_label_value);

    	ViewUtils.configureTextView(getActivity(), txtMaximizeToTH);
    	ViewUtils.configureTextView(getActivity(), txtBuildersQtd);
    	ViewUtils.configureTextView(getActivity(), txtBuildersEff);
    	ViewUtils.configureTextView(getActivity(), txtCostUpgradeConstructions);
    	ViewUtils.configureTextView(getActivity(), txtGoldValueConstructions);
    	ViewUtils.configureTextView(getActivity(), txtElixirValueConstructions);
    	ViewUtils.configureTextView(getActivity(), txtDarkElixirValueConstructions);
    	ViewUtils.configureTextView(getActivity(), txtCostUpgradeWalls);
    	ViewUtils.configureTextView(getActivity(), txtGoldValueWalls);
    	ViewUtils.configureTextView(getActivity(), txtGoldElixirValueWalls);
    	ViewUtils.configureTextView(getActivity(), txtTime);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgConLabel);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgConValue);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgTroopsLabel);
    	ViewUtils.configureTextView(getActivity(), txtTimeUpgTroopsValue);
    	
    	int maxTHLevel = Global.getVillage().getMaxTownHallLevel();

    	ViewUtils.configureIntegerSpinner(spnTHLevel, getActivity(), 1, maxTHLevel);
		ViewUtils.configureIntegerSpinner(spnBuildersQtd, getActivity(), 1, EntityType.BUILDER.getMaximumNumberOfEntityByTH(maxTHLevel));
		ViewUtils.configureIntegerSpinner(spnBuildersEff, getActivity(), 10, 100, 10);
		
        
        spnTHLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		updateSimulationInfo();
        	}
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
        
        spnBuildersQtd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		updateSimulationInfo();
        	}
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
        
        spnBuildersEff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		updateSimulationInfo();
        	}
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
    	

        ImageView openCostDetails = (ImageView) view.findViewById(R.id.open_cost_upgrade_village_details);
        openCostDetails.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment f = new DetailedInformationFragment();
		        Bundle args = new Bundle();
		        args.putBoolean(DetailedInformationFragment.SHOW_COST_DETAILS, true);
		        args.putBoolean(DetailedInformationFragment.SHOW_MAXIMIZE_VILLAGE_FROM_TH_LEVEL, true);
		        args.putInt(DetailedInformationFragment.DEFAULT_TH_LEVEL, getSelectedTHLevel());
		        f.setArguments(args);
				((MainActivity)getActivity()).showFragment(f);
			}
		});
        
        
        ImageView openTimeDetails = (ImageView) view.findViewById(R.id.open_time_upgrade_village_details);
        openTimeDetails.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment f = new DetailedInformationFragment();
		        Bundle args = new Bundle();
		        args.putBoolean(DetailedInformationFragment.SHOW_TIME_DETAILS, true);
		        args.putBoolean(DetailedInformationFragment.SHOW_MAXIMIZE_VILLAGE_FROM_TH_LEVEL, true);
		        args.putInt(DetailedInformationFragment.DEFAULT_TH_LEVEL, getSelectedTHLevel());
		        f.setArguments(args);
				((MainActivity)getActivity()).showFragment(f);
			}
		});

    	return view;
    }
	
	private void updateSimulationInfo() {
		String dSymbol = getActivity().getString(R.string.day_letter); 
		String hSymbol = getActivity().getString(R.string.hour_letter);
		String mSymbol = getActivity().getString(R.string.minute_letter);
		String sSymbol = getActivity().getString(R.string.second_letter);
		String emptyString = getActivity().getString(R.string.all_updated);

   		int simulatedTHLevel = getSelectedTHLevel();
   		int simulatedBuilderQtd = getSelectedBuilderNum();
   		int simulatedBuilderEff = getSelectedBuilderEff();
   		
		CoCTime timeToConstructions = Global.getVillage().getTimeToMaximizeBuildingsUntilTHLevel(simulatedTHLevel);
		CoCTime timeToTroops = Global.getVillage().getTimeToMaximizeTroopsAndSpellsUntilTHLevel(simulatedTHLevel);
		Resource resource = Global.getVillage().getCostToMaximizeUntilTHLevel(simulatedTHLevel);
		Resource resourceWalls = Global.getVillage().getCostToMaximizeWallsUnitlTHLevel(simulatedTHLevel);
		
		txtGoldValueConstructions.setText(Formatters.format(resource.getGold()));
		txtElixirValueConstructions.setText(Formatters.format(resource.getElixir()));
		txtDarkElixirValueConstructions.setText(Formatters.format(resource.getDarkElixir()));
		
		txtGoldValueWalls.setText(Formatters.format(resourceWalls.getGold()));
		txtGoldElixirValueWalls.setText(Formatters.format(resourceWalls.getElixir()));
		
		txtTimeUpgTroopsValue.setText(Formatters.format(timeToTroops, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
		
		timeToConstructions = timeToConstructions.multiply((1.0 / simulatedBuilderQtd) * (100.0 / simulatedBuilderEff));
		txtTimeUpgConValue.setText(Formatters.format(timeToConstructions, dSymbol, hSymbol, mSymbol, sSymbol, emptyString));
		
		txtGoldValueConstructions.setGravity(Gravity.END);
		txtElixirValueConstructions.setGravity(Gravity.END);
		txtDarkElixirValueConstructions.setGravity(Gravity.END);
		
		txtGoldValueWalls.setGravity(Gravity.END);
		txtGoldElixirValueWalls.setGravity(Gravity.END);
		
		txtTimeUpgTroopsValue.setGravity(Gravity.END);
		txtTimeUpgConValue.setGravity(Gravity.END);
	}	
}