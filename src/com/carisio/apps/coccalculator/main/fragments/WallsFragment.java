package com.carisio.apps.coccalculator.main.fragments;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.ImageUtils;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.EntityType;

import de.hdodenhof.circleimageview.CircleImageView;

public class WallsFragment extends RetainedFragment {
	private int thLevel;
	private int maxQuantity;
	private int maxWallLevel;
	private Map<Integer, Spinner> spinners;
	private TextView wallsCount;
	
	@Override
	public void onPause() {
		super.onPause();
		saveVillage();
	}
	@Override
	public void onResume() {
		super.onResume();
		for (int i = 1; i <= maxWallLevel; i++) {
			int nWalls = Global.getVillage().getWall(i).getNumberOfWalls();
			if (nWalls > maxQuantity)
				nWalls = maxQuantity;
			
			Spinner wallQuantity = spinners.get(i);
			wallQuantity.setSelection(nWalls);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		initiateVariables();
		
    	View view = inflater.inflate(R.layout.image_text_spinner_container, container, false);
    	getActivity().setTitle(getActivity().getString(R.string.wall));
    	
    	// Configure table title
    	View title = view.findViewById(R.id.table_title);
    	TextView mainLabel = (TextView)title.findViewById(R.id.textview_column_label);
    	TextView spinnerLabel = (TextView)title.findViewById(R.id.spinner_table_label);
    	mainLabel.setText(getActivity().getText(R.string.walls_level));
    	spinnerLabel.setText(getActivity().getText(R.string.quantity_short));
    	ViewUtils.configureTextView(getActivity(), mainLabel);
    	ViewUtils.configureTextView(getActivity(), spinnerLabel);
    	
    	createRows((LinearLayout) view.findViewById(R.id.table_text_spinner), inflater, container);
    	return view;
	}

	private void createRows(LinearLayout table, LayoutInflater inflater, ViewGroup container) {
    	for (int i = maxWallLevel; i > 0; i--) {
    		View row = inflater.inflate(R.layout.row_image_text_spinner, container, false);
    		final CircleImageView circleIV = (CircleImageView)row.findViewById(R.id.entity_profile_image);
    		circleIV.setFillColorResource(android.R.color.white);
    		TextView entityLabel = (TextView) row.findViewById(R.id.entity_label);
    		ViewUtils.configureTextView(getActivity(), entityLabel);
    		Spinner wallQuantity = (Spinner) row.findViewById(R.id.level_picker);
    		circleIV.setImageResource(ImageUtils.getWallsImages()[i]);
    		entityLabel.setText(getActivity().getString(R.string.level) + " " + i);
    		
    		ViewUtils.configureIntegerSpinner(wallQuantity, getActivity(), 0, maxQuantity);
        	wallQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            	@Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            		updateNumberOfWalls();
                }
            	@Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });
        	spinners.put(i, wallQuantity);
    		table.addView(row);
    		
    		View row2 = inflater.inflate(R.layout.horizontal_line, container, false);
    		table.addView(row2);
    	}
    	
    	View row = inflater.inflate(R.layout.row_walls_count, container, false);
    	wallsCount = (TextView) row.findViewById(R.id.number_of_walls);
    	ViewUtils.configureTextView(getActivity(), wallsCount);

    	table.addView(row);
	}

	private void saveVillage() {
		int numberSelectedWalls = getNumberOfSelectedWalls();
		Set<Integer> keys = spinners.keySet();
		for (Integer key : keys) {
			int nWalls = Integer.valueOf(spinners.get(key).getSelectedItem().toString());
			Global.getVillage().getWall(key).setNumberOfWalls(nWalls);
		}
		Global.saveVillage(getActivity());
		
		if (numberSelectedWalls > maxQuantity) 
			Global.getVillage().adaptWallsToTHLevel(Global.getVillage().getTownHallLevel());
	}
	private int getNumberOfSelectedWalls() {
		Set<Integer> keys = spinners.keySet();
		int numberSelectedWalls = 0;
		for (Integer key : keys) {
			if (key != 0) // Don�t count lvl 0 walls
				numberSelectedWalls += Integer.valueOf(spinners.get(key).getSelectedItem().toString());
		}
		return numberSelectedWalls;
	}
	private void updateNumberOfWalls() {
		int numberSelectedWalls = getNumberOfSelectedWalls();
		String text = numberSelectedWalls + "/" + maxQuantity;
		if (numberSelectedWalls > maxQuantity) {
			wallsCount.setTextColor(ContextCompat.getColor(getActivity(), R.color.app_alert_foreground_color));
			Toast.makeText(getActivity(), getActivity().getString(R.string.reduce_walls_message), Toast.LENGTH_LONG).show();
		} else {
			wallsCount.setTextColor(ContextCompat.getColor(getActivity(), R.color.app_foreground_color));
		}
		wallsCount.setText(text);
	}
	private void initiateVariables() {
    	thLevel = Global.getVillage().getTownHallLevel();
    	maxQuantity = EntityType.WALL.getMaximumNumberOfEntityByTH(thLevel);
    	maxWallLevel = EntityType.WALL.getEvolutionTable().getMaxLevelAllowedAtTHLevel(thLevel);
    	spinners = new HashMap<Integer, Spinner>();
	}
}
