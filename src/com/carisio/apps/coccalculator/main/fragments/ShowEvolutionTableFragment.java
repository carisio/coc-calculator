package com.carisio.apps.coccalculator.main.fragments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.carisio.apps.calculatorforclash.R;
import com.carisio.apps.coccalculator.main.Global;
import com.carisio.apps.coccalculator.utils.Formatters;
import com.carisio.apps.coccalculator.utils.ViewUtils;
import com.carisio.apps.coccalculatorengine.CoCTime;
import com.carisio.apps.coccalculatorengine.EntityType;
import com.carisio.apps.coccalculatorengine.EvolutionTable;
import com.carisio.apps.coccalculatorengine.Resource;
import com.carisio.apps.coccalculatorengine.WallEntity;

public class ShowEvolutionTableFragment extends RetainedFragment {
	private static Spinner spnEntityType;
	private static Spinner spnShowOptions;
	private ListView listView;
	
	private int posEntityType = -1;
	private int posShowOptions = -1;
	
	@Override
	public void onResume() {
		super.onResume();
		spnEntityType.setSelection(posEntityType);
		spnShowOptions.setSelection(posShowOptions);
	}
	@Override
	public void onPause() {
		super.onPause();
		posEntityType = spnEntityType.getSelectedItemPosition();
		posShowOptions = spnShowOptions.getSelectedItemPosition();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
    	getActivity().setTitle(getActivity().getString(R.string.show_evolution_table));
    	
    	View view = inflater.inflate(R.layout.show_evolution_table_layout, container, false);
    	
    	// Configure view
    	TextView type = (TextView)view.findViewById(R.id.type);
    	spnEntityType = (Spinner)view.findViewById(R.id.type_options);
    	TextView show = (TextView)view.findViewById(R.id.show);
    	spnShowOptions= (Spinner)view.findViewById(R.id.show_options);
    	
    	List<String> entityTypes = Global.getEntityTypesNames(EntityType.getAllTypes(), getActivity());
    	entityTypes.remove(getActivity().getString(R.string.builder));
    	Collections.sort(entityTypes);
    	
    	ViewUtils.configureTextView(getActivity(), type);
    	ViewUtils.configureTextView(getActivity(), show);
    	ViewUtils.configureSpinner(spnEntityType, getActivity(), entityTypes);
    	ViewUtils.configureSpinner(spnShowOptions, getActivity(), Arrays.asList(getResources().getStringArray(R.array.show_options)));
    	
    	spnEntityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		updateList();
            }
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });

    	spnShowOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		updateList();
            }
        	@Override
            public void onNothingSelected(AdapterView<?> parentView) {
        			
            }
        });
    	
    	listView = (ListView) view.findViewById(R.id.evolution_table);
    	
		return view;
	}
	
	private void updateList() {
		String selectedEntityType = (String) spnEntityType.getSelectedItem();
		String selectedShowOptions = (String) spnShowOptions.getSelectedItem();
		
		Map<String, EntityType> map = Global.getEntityTypeMap(getActivity());
		EntityType entityType = map.get(selectedEntityType);
		
		List<Integer> levels = new ArrayList<Integer>();
		int maxLevel = entityType.getEvolutionTable().getMaxLevel();
		for (int i = 0; i <= maxLevel; i++) { // Row 0 is used as a header
			levels.add(i);
		}
		listView.setAdapter(new ShowEvolutionTableAdapter(getActivity(), R.layout.row_evolution_table, levels, selectedEntityType, selectedShowOptions));
	}
	
	class ShowEvolutionTableAdapter extends ArrayAdapter<Integer> {
		private String selectedShowOption;
		private EntityType selectedEntityType;
		private String dSymbol; 
		private String hSymbol;
		private String mSymbol;
		private String sSymbol;
		private String emptyString;

		
		public ShowEvolutionTableAdapter(Context context, int resource, List<Integer> levels, String selectedEntityType, String selectedShowOption) {
			super(context, resource, levels);

			Map<String, EntityType> map = Global.getEntityTypeMap(getActivity());
			this.selectedEntityType= map.get(selectedEntityType);
			this.selectedShowOption = selectedShowOption;
			
			dSymbol = getActivity().getString(R.string.day_letter); 
			hSymbol = getActivity().getString(R.string.hour_letter);
			mSymbol = getActivity().getString(R.string.minute_letter);
			sSymbol = getActivity().getString(R.string.second_letter);
			emptyString = getActivity().getString(R.string.zero);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            View view = vi.inflate(R.layout.row_evolution_table, null);

            TextView txtLvl = (TextView) view.findViewById(R.id.entity_level);
            TextView txtInfo = (TextView) view.findViewById(R.id.entity_info);
            ImageView imgForInfo = (ImageView) view.findViewById(R.id.img_for_entity_info);
            
            ViewUtils.configureTextView(getActivity(), txtLvl);
            ViewUtils.configureTextView(getActivity(), txtInfo);
            
            
            Integer lvlReq = getItem(position);
            
            if (lvlReq == 0) {
            	txtLvl.setText(getContext().getString(R.string.level_short));
            	txtInfo.setText(selectedShowOption);
            	return view;
            }
            
            EvolutionTable et = selectedEntityType.getEvolutionTable();
            
            int imgTemp[] = new int[1];
            String formattedInfo = null;
            if (selectedShowOption.equals(getActivity().getString(R.string.show_cost))) {
            	Resource r = et.getLevelRequirements(position).getCostToUpgrade();
            	formattedInfo = getFormattedResourceAndFormatImage(imgTemp, r);
            } else if (selectedShowOption.equals(getActivity().getString(R.string.show_cumulative_cost))) {
            	Resource r = et.getAccumulatedResourceToUpgrade(0, position);
            	formattedInfo = getFormattedResourceAndFormatImage(imgTemp, r);
            } else if (selectedShowOption.equals(getActivity().getString(R.string.show_time))) {
            	CoCTime t = et.getLevelRequirements(position).getTimeToUpgrade();
            	formattedInfo= Formatters.format(t, dSymbol, hSymbol, mSymbol, sSymbol, emptyString);
            	imgTemp[0] = R.drawable.ic_schedule_black_24dp;
            } else if (selectedShowOption.equals(getActivity().getString(R.string.show_cumulative_time))) {
            	CoCTime t = et.getAccumulatedTimeToUpgrade(0, position);
            	formattedInfo= Formatters.format(t, dSymbol, hSymbol, mSymbol, sSymbol, emptyString);
            	imgTemp[0] = R.drawable.ic_schedule_black_24dp;
            }
            
            if (selectedEntityType.equals(EntityType.WALL)) {
            	
            	if (position >= WallEntity.getInitialLevelToUpgradeWithGoldAndElixir() && 
            			selectedShowOption.equals(getActivity().getString(R.string.show_cost))) {
            		imgTemp[0] = R.drawable.gold_or_elixir;
            	}
            	if (position >= WallEntity.getInitialLevelToUpgradeWithGoldAndElixir() && 
            			selectedShowOption.equals(getActivity().getString(R.string.show_cumulative_cost))) {
            		Resource r = et.getAccumulatedResourceToUpgrade(0, position);
            		r.setGold(r.getGold() + r.getElixir());
            		r.setElixir(0);
            		formattedInfo = getFormattedResourceAndFormatImage(imgTemp, r);
            	}
            }
            
            txtLvl.setText("" + position);
            txtInfo.setText(formattedInfo);
            imgForInfo.setImageResource(imgTemp[0]);
            return view;
		}
		
		private String getFormattedResourceAndFormatImage(int[] img, Resource resource) {
			String formatted_cost = "0";
			if (resource.getGold() > 0) {
				formatted_cost = Formatters.format(resource.getGold());
				img[0] = R.drawable.gold;
			}
			if (resource.getElixir() > 0) {
				formatted_cost = Formatters.format(resource.getElixir());
				img[0] = R.drawable.elixir;
			}
			if (resource.getDarkElixir() > 0) {
				formatted_cost = Formatters.format(resource.getDarkElixir());
				img[0] = R.drawable.dark_elixir;
			}
			return formatted_cost;
		}	
	}
}
