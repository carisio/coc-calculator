=> Adicionar novo level de constru��o:
- Alterar o arquivo EvolutionTable (M�todo get____EvolutionTable())
- Adicionar arquivo de recursos com a figura da constru��o
- Fazer refer�ncia a imagem da nova tropa no ImageUtil (M�todo get____Images())
- Se tiver alterado a quantidade de constru��es por TH level, alterar o arquivo EntityType (M�todo getMaximumNumberOfEntityByTH()) e o arquivo Village (M�todo initializeEntities())

=> Adicionar novo level de tropa/feiti�o:
- Alterar o arquivo EvolutionTable (M�todo get____EvolutionTable())
- Alterar o arquivo TroopsAndSpellsTrainingTable (M�todo get____TrainingTable())
- Adicionar arquivo de recursos com a figura da tropa
- Fazer refer�ncia a imagem da nova tropa no ImageUtil (M�todo get____Images())

=> Adicionar nova tropa
- No arquivo strings.xml
-- Adicionar o nome da tropa
- No arquivo EntityType:
-- Definir a nova tropa no enum
-- Alterar o m�todo getEvolutionTable() para pegar a tabela de evolu��o dessa nova tropa
-- Alterar o m�todo getMaximumNumberOfEntityByTH() para considerar a nova tropa
-- Inserir o novo tipo no m�todo getAllTypes()
-- Inserir a nova tropa em getElixirTroops() ou em getDarkElixirTroops()
- No arquivo EvolutionTable:
-- Criar o m�todo get____EvolutionTable(), chamado pelo m�todo EntityType::getEvolutionTable()
- No arquivo TroopsAndSpellsTrainingTable
-- Criar o m�todo get____TrainingTable()
-- Adicionar a chamada de get____TrainingTable() dentro do m�todo getAllTroopsAndSpellsTrainingTable()
- Adicionar arquivos de imagens
-- No arquivo ImageUtil:
- Criar o m�todo get____Images
- Alterar o m�todo createMapEntityNameImage() para chamar o m�todo get____Images rec�m criado
-- No arquivo Village, alterar o m�todo initializeEntities() para considerar a nova tropa
-- No arquivo EntitiesOfDifferentTypeFragments, alterar o m�todo initializeVariables()
-- No arquivo Global, alterar o m�todo getEntityTypeMap()


Refactoring => � poss�vel fazer o refacatoring pra n�o ter que definir mais essas coisas no m�todo initializeVariables() dentro de EntitiesOfDifferentTypeFragments (j� existe a separa��o de tropas/feiti�os negros e normais dentro de EntityType)